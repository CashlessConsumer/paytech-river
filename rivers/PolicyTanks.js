onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "digital payments – Dvara Research Blog",
                "feedUrl": "https://www.dvara.com/blog/tag/digital-payments/feed/",
                "websiteUrl": "https://www.dvara.com/blog",
                "feedDescription": "Doorway to Financial Access",
                "whenLastUpdate": "Thu, 08 Nov 2018 13:11:42 GMT",
                "item": [
                    {
                        "title": "Comments on the Report of Watal Committee on Digital Payments",
                        "link": "https://www.dvara.com/blog/2017/01/12/comments-on-the-report-of-watal-committee-on-digital-payments/",
                        "body": "By Malavika Raghavan, IFMR Finance Foundation\nShortly after Christmas last month, a press release from the Ministry of Finance on 28th December announced that the Committee on Digital Payments (chaired by Ratan P. Watal) had submitted its Report. IFMR Finance Foundation’s Future ...",
                        "pubDate": "Thu, 12 Jan 2017 10:39:59 GMT",
                        "permaLink": "",
                        "comments": "https://www.dvara.com/blog/2017/01/12/comments-on-the-report-of-watal-committee-on-digital-payments/#respond",
                        "id": "0000488"
                    },
                    {
                        "title": "Insights from the “Digital Payments Roundtable” hosted by the Future of Finance Initiative",
                        "link": "https://www.dvara.com/blog/2017/05/30/insights-from-the-digital-payments-roundtable-hosted-by-the-future-of-finance-initiative/",
                        "body": "(This post is authored by the Future of Finance Team at the IFMR Finance Foundation).\nIn April, the Future of Finance Initiative (FFI) hosted a series of closed door workshops with a small set of digital financial service providers focusing on payments, credit and investments. ...",
                        "pubDate": "Tue, 30 May 2017 07:11:25 GMT",
                        "permaLink": "",
                        "comments": "https://www.dvara.com/blog/2017/05/30/insights-from-the-digital-payments-roundtable-hosted-by-the-future-of-finance-initiative/#respond",
                        "id": "0000487"
                    },
                    {
                        "title": "Reviewing Payments Regulation",
                        "link": "https://www.dvara.com/blog/2018/03/05/reviewing-payments-regulation/",
                        "body": "India has a rich and diverse payments infrastructure. The eco-system of Payment Service Providers (PSPs) has undergone a rapid evolution in India over the recent past with the advances in technology and yet, there still exists hurdles to enabling ubiquitous access to payment ...",
                        "pubDate": "Mon, 05 Mar 2018 03:22:55 GMT",
                        "permaLink": "",
                        "comments": "https://www.dvara.com/blog/2018/03/05/reviewing-payments-regulation/#respond",
                        "id": "0000486"
                    },
                    {
                        "title": "Looking at Retail Digital Payments Through a Data Lens",
                        "link": "https://www.dvara.com/blog/2018/07/03/looking-at-retail-digital-payments-through-a-data-lens/",
                        "body": "By Sansiddha Pani, Dvara Research\nIn the recent years, the Government of India has focussed on promoting a cashless economy as part of its Digital India programme. The government has taken a number of steps to promote digital payments for availing government services[i] and also ...",
                        "pubDate": "Tue, 03 Jul 2018 08:32:27 GMT",
                        "permaLink": "",
                        "comments": "https://www.dvara.com/blog/2018/07/03/looking-at-retail-digital-payments-through-a-data-lens/#comments",
                        "id": "0000485"
                    }
                ]
            },
            {
                "feedTitle": "Indicus list Archive Feed",
                "feedUrl": "https://us11.campaign-archive.com/feed?u=b1dc50f1953ee62e477bfb335&id=52e93611b9",
                "websiteUrl": "https://us11.campaign-archive.com/feed?u=b1dc50f1953ee62e477bfb335&id=52e93611b9",
                "feedDescription": "",
                "whenLastUpdate": "Fri, 02 Nov 2018 12:32:24 GMT",
                "item": [
                    {
                        "title": "RBI goes public with dissent",
                        "link": "https://mailchi.mp/874d53534b7e/rbi-goes-public-with-dissent-1592661",
                        "body": "RBI goes public with dissent\n  \n  \n    \n      \n        \n          \n          \n        \n        \n           \n        \n        \n          \n            \n              \n                \n                  November 2018\n                \n              \n            \n          \n        \n ...",
                        "pubDate": "Fri, 02 Nov 2018 12:23:12 GMT",
                        "permaLink": "https://mailchi.mp/874d53534b7e/rbi-goes-public-with-dissent-1592661",
                        "id": "0000437"
                    },
                    {
                        "title": "Reports of data and consent breaches need industry and regulatory attention",
                        "link": "https://mailchi.mp/24c0a57ca814/reports-of-data-and-consent-breaches-need-industry-and-regulatory-attention",
                        "body": "Reports of data and consent breaches need industry and regulatory attention\n  \n  \n    \n      \n        \n          \n          \n        \n        \n           \n        \n        \n          \n            \n              \n                \n                  April 2018\n                \n     ...",
                        "pubDate": "Tue, 03 Apr 2018 12:30:00 GMT",
                        "permaLink": "https://mailchi.mp/24c0a57ca814/reports-of-data-and-consent-breaches-need-industry-and-regulatory-attention",
                        "id": "0000326"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "PolicyTanks.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.001,
        "ctBuilds": 4,
        "ctDuplicatesSkipped": 4,
        "whenGMT": "Thu, 08 Nov 2018 13:12:00 GMT",
        "whenLocal": "2018-11-8 13:12:00",
        "aggregator": "River5 v0.4.14"
    }
})