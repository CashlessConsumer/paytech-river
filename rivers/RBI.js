onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Wed, 28 Nov 2018 11:13:44 GMT",
                "item": [
                    {
                        "title": "Liquidity Adjustment Facility: Fixed Rate Repo Operations",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45591",
                        "body": "The result of the RBI Fixed Rate Repo Operations held on November 28, 2018 is as under:                        Amount (face value in &#8377; Billion)                          Items          Overnight Repo Operations            6.50% Fixed Rate                          1. Bids ...",
                        "pubDate": "Wed, 28 Nov 2018 16:15:00 GMT",
                        "permaLink": "",
                        "id": "0000718"
                    },
                    {
                        "title": "182-Days Treasury Bills: Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45589",
                        "body": "Date of Auction: November 28, 2018                    I.        Notified Amount        &#8377; 4,000.00 crore                    II.        Underwriting Notified Amount        &#8377; 0.00                    III.        Competitive Bids Received        &nbsp;                    ...",
                        "pubDate": "Wed, 28 Nov 2018 14:15:00 GMT",
                        "permaLink": "",
                        "id": "0000717"
                    },
                    {
                        "title": "364-Days Treasury Bills: Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45590",
                        "body": "Date of Auction: November 28, 2018                    I.        Notified Amount        &#8377; 4,000.00 crore                    II.        Underwriting Notified Amount        &#8377; 0.00                    III.        Competitive Bids Received        &nbsp;                    ...",
                        "pubDate": "Wed, 28 Nov 2018 14:15:00 GMT",
                        "permaLink": "",
                        "id": "0000716"
                    },
                    {
                        "title": "91-Days Treasury Bills: Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45588",
                        "body": "",
                        "pubDate": "Wed, 28 Nov 2018 14:15:00 GMT",
                        "permaLink": "",
                        "id": "0000715"
                    },
                    {
                        "title": "Underwriting Auction for sale of Government Securities for &#8377; 12,000 cr on November 29, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45583",
                        "body": "The Government of India has announced the sale (issue/re-issue) of Government Stock detailed below through auctions to be held on November 30, 2018.    As per revised scheme of underwriting notified on November 14, 2007, the amounts of Minimum Underwriting Commitment (MUC) and ...",
                        "pubDate": "Wed, 28 Nov 2018 13:15:00 GMT",
                        "permaLink": "",
                        "id": "0000714"
                    },
                    {
                        "title": "RBI releases data on ECB/FCCB/RDB for October 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45584",
                        "body": "The Reserve Bank of India has today released the data on External Commercial Borrowings (ECB), Foreign Currency Convertible Bonds (FCCB) and Rupee Denominated Bonds (RDB) both, through Automatic Route and Approval Route, for the month of October 2018.    Ajit Prasad    Assistant ...",
                        "pubDate": "Wed, 28 Nov 2018 13:25:00 GMT",
                        "permaLink": "",
                        "id": "0000713"
                    },
                    {
                        "title": "91-Days Treasury Bills - Auction Result: Cut off",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45585",
                        "body": "Date of Auction: November 28, 2018                    I.        Total Face Value Notified        &#8377; 7,000 Crore                    II.        Cut-off Price and          Implicit Yield at Cut-Off Price        &#8377; 98.34          (YTM: 6.7706%)                    III.      ...",
                        "pubDate": "Wed, 28 Nov 2018 13:30:00 GMT",
                        "permaLink": "",
                        "id": "0000712"
                    },
                    {
                        "title": "182-Days Treasury Bills - Auction Result: Cut off",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45586",
                        "body": "Date of Auction: November 28, 2018                    I.        Total Face Value Notified        &#8377; 4,000 Crore                    II.        Cut-off Price and          Implicit Yield at Cut-Off Price        &#8377; 96.60          (YTM: 7.0587%)                    III.      ...",
                        "pubDate": "Wed, 28 Nov 2018 13:30:00 GMT",
                        "permaLink": "",
                        "id": "0000711"
                    },
                    {
                        "title": "364-Days Treasury Bills - Auction Result: Cut off",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45587",
                        "body": "Date of Auction: November 28, 2018                    I.        Total Face Value Notified        &#8377; 4,000 Crore                    II.        Cut-off Price and          Implicit Yield at Cut-Off Price        &#8377; 93.28          (YTM: 7.2239%)                    III.      ...",
                        "pubDate": "Wed, 28 Nov 2018 13:30:00 GMT",
                        "permaLink": "",
                        "id": "0000710"
                    },
                    {
                        "title": "Liquidity Adjustment Facility: Fixed Rate Reverse Repo Operations",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45581",
                        "body": "The result of the RBI Fixed Rate Reverse Repo Operations held on November 27, 2018 is as under:                        Amount (face value in &#8377; Billion)                          Items          Overnight Reverse Repo Auction          6.25% Fixed Rate                          ...",
                        "pubDate": "Wed, 28 Nov 2018 08:45:00 GMT",
                        "permaLink": "",
                        "id": "0000709"
                    },
                    {
                        "title": "Money Market Operations as on November 27, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45582",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Wed, 28 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000708"
                    },
                    {
                        "title": "Conversion of Government of India (GoI)’s Securities",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45580",
                        "body": "The Reserve Bank of India, in consultation with the Government of India, has converted a security maturing in 2018-19, having a total face value of &#8377; 2,779.986 crore, to a longer tenor security maturing in 2023-24.    The transaction has been conducted on November 26, 2018 ...",
                        "pubDate": "Tue, 27 Nov 2018 18:30:00 GMT",
                        "permaLink": "",
                        "id": "0000707"
                    },
                    {
                        "title": "RBI to inject durable liquidity through OMO purchase auctions  in December 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45579",
                        "body": "Based on an assessment of the durable liquidity needs going forward, RBI has decided to conduct purchase of Government securities under Open Market Operations (OMOs) for an aggregate amount of &#8377; 400 billion in the month of December 2018.      The auction dates and the ...",
                        "pubDate": "Tue, 27 Nov 2018 18:15:00 GMT",
                        "permaLink": "",
                        "id": "0000706"
                    },
                    {
                        "title": "RBI Announces OMO Purchase of Government of India Dated Securities",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45577",
                        "body": "",
                        "pubDate": "Tue, 27 Nov 2018 18:05:00 GMT",
                        "permaLink": "",
                        "id": "0000705"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 28 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45578",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                        Sl. No.          Name of the Company          ...",
                        "pubDate": "Tue, 27 Nov 2018 18:05:00 GMT",
                        "permaLink": "",
                        "id": "0000704"
                    },
                    {
                        "title": "Results of Auctions of 3 to 25 years State Development Loans of Fourteen State Governments - Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45576",
                        "body": "",
                        "pubDate": "Tue, 27 Nov 2018 16:35:00 GMT",
                        "permaLink": "",
                        "id": "0000703"
                    },
                    {
                        "title": "Result of the 7-day Variable Rate Reverse Repo auction held on November 27, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45575",
                        "body": "Tenor        7-day                    Notified Amount (in &#8377; billion)        300.00                    Total amount of offers received (in &#8377; billion)        118.62                    Amount accepted (in &#8377; billion)        118.62                    Cut off Rate ...",
                        "pubDate": "Tue, 27 Nov 2018 16:20:00 GMT",
                        "permaLink": "",
                        "id": "0000702"
                    },
                    {
                        "title": "The Uravakonda Co-operative Town Bank Ltd., Uravakonda, Andhra Pradesh – Penalised",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45573",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377; 2.00 lakh (Rupees Two lakh only) on The Uravakonda Co-operative Town Bank Ltd., Uravakonda, Andhra Pradesh, in exercise of the powers vested in it under the provisions of Section 47A (1) (c) read with Section 46 ...",
                        "pubDate": "Tue, 27 Nov 2018 15:20:00 GMT",
                        "permaLink": "",
                        "id": "0000700"
                    },
                    {
                        "title": "The Nellore Co-operative Urban Bank Ltd., Nellore, Andhra Pradesh – Penalised",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45572",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377; 2.00 lakh (Rupees Two lakh only) on The Nellore Co-operative Urban Bank Ltd., Nellore, Andhra Pradesh, in exercise of the powers vested in it under the provisions of Section 47A (1) (c) read with Section 46 (4) ...",
                        "pubDate": "Tue, 27 Nov 2018 15:15:00 GMT",
                        "permaLink": "",
                        "id": "0000699"
                    },
                    {
                        "title": "Sri Bharathi Co-operative Urban Bank Ltd., Hyderabad, Telangana – Penalised",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45571",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377; 2.00 lakh (Rupees Two lakh only) on Sri Bharathi Co-operative Urban Bank Ltd., Hyderabad, Telangana, in exercise of the powers vested in it under the provisions of Section 47A (1) (c) read with Section 46 (4) of ...",
                        "pubDate": "Tue, 27 Nov 2018 15:10:00 GMT",
                        "permaLink": "",
                        "id": "0000698"
                    },
                    {
                        "title": "Survey on Indian Startup Sector",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45568",
                        "body": "The Reserve Bank has launched a survey on India&rsquo;s startup sector (SISS). The survey is intended to create a profile of the startup sector in India and provide dimensions relating to turnover, profitability and workforce. The SISS would also throw light on the problems ...",
                        "pubDate": "Tue, 27 Nov 2018 15:00:00 GMT",
                        "permaLink": "",
                        "id": "0000697"
                    },
                    {
                        "title": "Result of Yield/ Price Based Auctions of State Development Loans  of State Governments",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45569",
                        "body": "Sr. No.        State        Notified amount in          (&#8377; Cr)        Amount accepted          (&#8377; Cr)        Cut off Price/ yield         Tenure (Yrs)                    1.        Andhra Pradesh #        1539.8        0        NA        15                    2.       ...",
                        "pubDate": "Tue, 27 Nov 2018 15:00:00 GMT",
                        "permaLink": "",
                        "id": "0000696"
                    },
                    {
                        "title": "The Kuppam Co-operative Town Bank Ltd., Kuppam, Andhra Pradesh – Penalised",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45570",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377; 5.00 lakh (Rupees Five lakh only) on The Kuppam Co-operative Town Bank Ltd.,Kuppam, Andhra Pradesh in exercise of the powers vested in it under the provisions of Section 47A (1)(c) read with Section 46(4) of the ...",
                        "pubDate": "Tue, 27 Nov 2018 15:05:00 GMT",
                        "permaLink": "",
                        "id": "0000695"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on November 27, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45567",
                        "body": "Tenor        14-day                    Notified Amount (in &#8377; billion)        235.00                    Total amount of bids received (in &#8377; billion)        107.20                    Amount allotted (in &#8377; billion)        107.20                    Cut off Rate (%) ...",
                        "pubDate": "Tue, 27 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000694"
                    },
                    {
                        "title": "Money Market Operations as on November 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45566",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Tue, 27 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000692"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Mon, 26 Nov 2018 14:14:18 GMT",
                "item": [
                    {
                        "title": "Auction for Sale (Re-issue) of Government Stock (GS)",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11419&Mode=0",
                        "body": "Government of India        Ministry of Finance    Department of Economic Affairs    Budget Division    New Delhi, dated November 26, 2018    NOTIFICATION    Auction for Sale (Re-issue) of Government Stock (GS)    F.No.4(6)W&amp;M/2018: Government of India(GoI) hereby notifies ...",
                        "pubDate": "Mon, 26 Nov 2018 19:20:00 GMT",
                        "permaLink": "",
                        "id": "0000687"
                    },
                    {
                        "title": "Auction of Government of India Dated Securities",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11420&Mode=0",
                        "body": "RBI/2018-19/80    Ref.No.IDMD/1293/08.02.032/2018-19    November 26, 2018    All Scheduled Commercial BanksAll State Co-operative Banks/All Scheduled Primary(Urban) Co-operative Banks /All Financial Institutions/All Primary Dealers.    Dear Sir/Madam,    Auction of Government of ...",
                        "pubDate": "Mon, 26 Nov 2018 19:25:00 GMT",
                        "permaLink": "",
                        "id": "0000686"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Mon, 26 Nov 2018 13:18:03 GMT",
                "item": [
                    {
                        "title": "Government of India announces the sale of five dated securities for &#8377; 12,000 crore",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45564",
                        "body": "",
                        "pubDate": "Mon, 26 Nov 2018 18:40:00 GMT",
                        "permaLink": "",
                        "id": "0000685"
                    },
                    {
                        "title": "RBI to conduct 7-day Variable Rate Reverse Repo Auction  under LAF on November 27, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45563",
                        "body": "The Reserve Bank of India will conduct the following Variable rate Reverse Repo auction on November 27, 2018, Tuesday, as per the revised guidelines on Term Repo Auctions issued on February 13, 2014.                        Sl. No.          Notified Amount            (&#8377; ...",
                        "pubDate": "Mon, 26 Nov 2018 18:30:00 GMT",
                        "permaLink": "",
                        "id": "0000684"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Mon, 26 Nov 2018 12:20:48 GMT",
                "item": [
                    {
                        "title": "External Commercial Borrowings (ECB) Policy – Review of Hedging Provision",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11418&Mode=0",
                        "body": "",
                        "pubDate": "Mon, 26 Nov 2018 17:40:00 GMT",
                        "permaLink": "",
                        "id": "0000683"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Mon, 26 Nov 2018 10:49:50 GMT",
                "item": [
                    {
                        "title": "Result of the 7-day Variable Rate Reverse Repo Auction held on November 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45561",
                        "body": "",
                        "pubDate": "Mon, 26 Nov 2018 16:10:00 GMT",
                        "permaLink": "",
                        "id": "0000682"
                    },
                    {
                        "title": "Money Market Operations as on November 23, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45560",
                        "body": "",
                        "pubDate": "Mon, 26 Nov 2018 11:10:00 GMT",
                        "permaLink": "",
                        "id": "0000678"
                    },
                    {
                        "title": "RBI to conduct 7-day Variable rate Reverse Repo auction under LAF on November 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45559",
                        "body": "",
                        "pubDate": "Mon, 26 Nov 2018 10:30:00 GMT",
                        "permaLink": "",
                        "id": "0000676"
                    },
                    {
                        "title": "Money Market Operations as on November 22, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45557",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Mon, 26 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000675"
                    },
                    {
                        "title": "Reserve Bank of India – Bulletin Weekly Statistical Supplement – Extract",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45555",
                        "body": "1. Reserve Bank of India - Liabilities and Assets*                    (&#8377; Billion)                    Item        2017        2018        Variation                    Nov. 17        Nov. 9        Nov. 16        Week        Year                    1        2        3        ...",
                        "pubDate": "Fri, 23 Nov 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000671"
                    },
                    {
                        "title": "91-day Treasury Bills auction announcement: &#8377; 7,000 crore under Regular Auction",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45552",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000664"
                    },
                    {
                        "title": "182-day Treasury Bills auction announcement: &#8377; 4,000 crore under Regular Auction",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45553",
                        "body": "The Reserve Bank of India has announced the auction of 182 day Government of India Treasury Bills for notified amount of &#8377; 4,000 crore. The sale will be subject to the terms and conditions specified in the General Notification F.No.4(2)-W&amp;M/2018 dated March 27, 2018 ...",
                        "pubDate": "Thu, 22 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000663"
                    },
                    {
                        "title": "364-day Treasury Bills auction announcement: &#8377; 4,000 crore under Regular Auction",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45554",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 17:40:00 GMT",
                        "permaLink": "",
                        "id": "0000662"
                    },
                    {
                        "title": "Auction of State Government Securities",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45551",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 17:10:00 GMT",
                        "permaLink": "",
                        "id": "0000661"
                    },
                    {
                        "title": "RBI releases ‘Quarterly BSR-1: Outstanding Credit of Scheduled Commercial Banks for June 2018'",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45550",
                        "body": "Today, the Reserve Bank released its web publication entitled &lsquo;Quarterly Basic Statistical Returns (BSR)-1: Outstanding Credit of Scheduled Commercial Banks (SCBs), June 2018&rsquo;, along with quarterly data for March 2018, on its Database on Indian Economy (DBIE) portal ...",
                        "pubDate": "Thu, 22 Nov 2018 17:05:00 GMT",
                        "permaLink": "",
                        "id": "0000660"
                    },
                    {
                        "title": "Results of OMO purchase auction held on November 22, 2018 and Settlement on November 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45549",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 16:15:00 GMT",
                        "permaLink": "",
                        "id": "0000659"
                    },
                    {
                        "title": "Scheduled Banks’ Statement of Position in India as on Friday, November 09, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45547",
                        "body": "(Amount in Billions of &#8377;)                    &nbsp;        &nbsp;        SCHEDULED COMMERCIAL BANKS          (Including RRBs)        ALL SCHEDULED BANKS                    &nbsp;        &nbsp;        10-NOV-2017        26-OCT-2018 *        09-NOV-2018 *        10-NOV-2017  ...",
                        "pubDate": "Thu, 22 Nov 2018 16:10:00 GMT",
                        "permaLink": "",
                        "id": "0000658"
                    },
                    {
                        "title": "Government Stock - Full Auction Results",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45546",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 15:35:00 GMT",
                        "permaLink": "",
                        "id": "0000656"
                    },
                    {
                        "title": "Government Stock - Auction Results: Cut-off",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45545",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 14:30:00 GMT",
                        "permaLink": "",
                        "id": "0000655"
                    },
                    {
                        "title": "Result of the 15-day Variable Rate Repo Auction held on November 22, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45544",
                        "body": "Tenor         15-day                    Notified Amount (in &#8377; billion)        235.00                    Total amount of bids received (in &#8377; billion)        295.70                    Amount allotted (in &#8377; billion)        235.01                    Cut off Rate ...",
                        "pubDate": "Thu, 22 Nov 2018 12:50:00 GMT",
                        "permaLink": "",
                        "id": "0000649"
                    },
                    {
                        "title": "Money Market Operations as on November 21, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45543",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 11:45:00 GMT",
                        "permaLink": "",
                        "id": "0000648"
                    },
                    {
                        "title": "Results of Underwriting Auctions Conducted on November 22, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45542",
                        "body": "",
                        "pubDate": "Thu, 22 Nov 2018 10:35:00 GMT",
                        "permaLink": "",
                        "id": "0000647"
                    },
                    {
                        "title": "Money Market Operations as on November 20, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45540",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Thu, 22 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000645"
                    },
                    {
                        "title": "Underwriting Auction for sale of Government Securities for &#8377; 12,000 cr on November 22, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45538",
                        "body": "Government of India has announced the sale (issue/re-issue) of Government Stock detailed below through auctions to be held on November 22, 2018.      As per revised scheme of underwriting notified on November 14, 2007, the amounts of Minimum Underwriting Commitment (MUC) and the ...",
                        "pubDate": "Tue, 20 Nov 2018 23:45:00 GMT",
                        "permaLink": "",
                        "id": "0000641"
                    },
                    {
                        "title": "Marginal Cost of Funds Based Lending Rate (MCLR) for the month October 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45537",
                        "body": "The Reserve Bank of India has today released Lending Rates of Scheduled Commercial Banks based on data received during the month of October 2018.    Ajit Prasad    Assistant Adviser    Press Release : 2018-2019/1179",
                        "pubDate": "Tue, 20 Nov 2018 20:40:00 GMT",
                        "permaLink": "",
                        "id": "0000639"
                    },
                    {
                        "title": "Reserve Money for the week ended November 16, 2018 and  Money Supply for the fortnight ended November 09, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45536",
                        "body": "The Reserve Bank has today released data on Reserve Money for the week ended November 16, 2018 and Money Supply for the fortnight ended November 09, 2018.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1178",
                        "pubDate": "Tue, 20 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000638"
                    },
                    {
                        "title": "Directions under Section 35A of the Banking Regulation Act, 1949 (AACS) – Padmashri Dr. Vitthalrao Vikhe Patil Co-operative Bank Ltd., Nashik, Maharashtra – Extension of Period",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45528",
                        "body": "Reserve Bank of India, in the public interest, had issued directions to Padmashri Dr. Vitthalrao Vikhe Patil Co-operative Bank Ltd., Nashik, Maharashtra in exercise of powers vested in it under sub-section (1) of Section 35A read with section 56 of the Banking Regulation Act, ...",
                        "pubDate": "Tue, 20 Nov 2018 13:05:00 GMT",
                        "permaLink": "",
                        "id": "0000625"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on November 20, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45527",
                        "body": "Tenor          14-day                          Notified Amount (in &#8377; billion)           230.00                          Total amount of bids received (in &#8377; billion)          276.50                          Amount allotted (in &#8377; billion)          230.01          ...",
                        "pubDate": "Tue, 20 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000624"
                    },
                    {
                        "title": "Term Repo Auctions under Liquidity Adjustment Facility",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45526",
                        "body": "",
                        "pubDate": "Tue, 20 Nov 2018 11:45:00 GMT",
                        "permaLink": "",
                        "id": "0000623"
                    },
                    {
                        "title": "Money Market Operations as on November 19, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45525",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Tue, 20 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000621"
                    },
                    {
                        "title": "RBI Central Board meets at Mumbai",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45523",
                        "body": "",
                        "pubDate": "Mon, 19 Nov 2018 20:45:00 GMT",
                        "permaLink": "",
                        "id": "0000619"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 32 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45520",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                        Sl. No.          Name of the Company          ...",
                        "pubDate": "Mon, 19 Nov 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000616"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 34 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45521",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                        Sl. No.          Name of the Company          ...",
                        "pubDate": "Mon, 19 Nov 2018 18:15:00 GMT",
                        "permaLink": "",
                        "id": "0000615"
                    },
                    {
                        "title": "Result of the Overnight Variable Rate Reverse Repo Auction held on  November 19, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45518",
                        "body": "Tenor        Overnight                    Notified Amount (in &#8377; billion)        250.00                    Total amount of offers received (in &#8377; billion)        441.68                    Amount accepted (in &#8377; billion)        250.07                    Cut off ...",
                        "pubDate": "Mon, 19 Nov 2018 16:40:00 GMT",
                        "permaLink": "",
                        "id": "0000614"
                    },
                    {
                        "title": "Results of Auctions of 2/13 years State Development Loans of Five State Governments - Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45517",
                        "body": "",
                        "pubDate": "Mon, 19 Nov 2018 15:25:00 GMT",
                        "permaLink": "",
                        "id": "0000612"
                    },
                    {
                        "title": "Result of Yield/Price Based Auctions of State Development Loans  of State Governments",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45516",
                        "body": "Sr. No.        State        Notified amount in          (&#8377; Cr)        Amount accepted          (&#8377; Cr)        Cut off Price/ yield         Tenure          (Yrs)                    1.        Gujarat *        1000        1300        8.53        10                    2.  ...",
                        "pubDate": "Mon, 19 Nov 2018 14:00:00 GMT",
                        "permaLink": "",
                        "id": "0000611"
                    },
                    {
                        "title": "Money Market Operations as on November 17, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45515",
                        "body": "",
                        "pubDate": "Mon, 19 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000607"
                    },
                    {
                        "title": "Money Market Operations as on November 16, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45512",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Mon, 19 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000605"
                    }
                ]
            },
            {
                "feedTitle": "PUBLICATION FROM RBI",
                "feedUrl": "https://www.rbi.org.in/Publication_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Publication.",
                "whenLastUpdate": "Fri, 16 Nov 2018 15:12:12 GMT",
                "item": [
                    {
                        "title": "Certificates of Authorisation issued by the Reserve Bank of India under the Payment and Settlement Systems Act, 2007 for Setting up and Operating Payment System in India",
                        "link": "http://www.rbi.org.in/scripts/PublicationsView.aspx?id=12043",
                        "body": "A. Certificates of Authorisation issued by the Reserve Bank of India under the Payment and Settlement Systems Act, 2007 for Setting up and Operating Payment System in India    The Payment and Settlement Systems Act, 2007 along with the Board for Regulation and Supervision of ...",
                        "pubDate": "Fri, 16 Nov 2018 20:20:00 GMT",
                        "permaLink": "",
                        "id": "0000598"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Fri, 16 Nov 2018 13:14:34 GMT",
                "item": [
                    {
                        "title": "Value Free Transfer (VFT) of Government Securities – Guidelines",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11417&Mode=0",
                        "body": "RBI/2018-19/78    IDMD.CDD.No.1241/11.02.001/2018-19    November 16, 2018    All SGL/CSGL Account holders    Madam/Sir,    Value Free Transfer (VFT) of Government Securities &ndash; Guidelines    A reference is invited to Notification No.183 dated September 05, 2011 on ...",
                        "pubDate": "Fri, 16 Nov 2018 18:35:00 GMT",
                        "permaLink": "",
                        "id": "0000597"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Fri, 16 Nov 2018 12:49:12 GMT",
                "item": [
                    {
                        "title": "RBI to conduct Overnight Variable Rate Reverse Repo Auction under LAF on November 19, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45510",
                        "body": "",
                        "pubDate": "Fri, 16 Nov 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000594"
                    },
                    {
                        "title": "19th Round of Quarterly Services and Infrastructure Outlook  Survey (SIOS) – Q3:2018-19",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45508",
                        "body": "The Reserve Bank has been conducting the Services and Infrastructure Outlook Survey (SIOS) since 2014-15. The survey is conducted on a quarterly basis. The 19th round of the SIOS seeks an assessment of the business situation for the current quarter (Q3:2018-19, i.e., ...",
                        "pubDate": "Fri, 16 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000593"
                    },
                    {
                        "title": "Government of India announces the sale of four dated securities for &#8377; 12,000 crore",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45509",
                        "body": "",
                        "pubDate": "Fri, 16 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000592"
                    },
                    {
                        "title": "Result of the 3-day Variable Rate Reverse Repo Auction held on November 16, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45504",
                        "body": "Tenor        3-day                    Notified Amount (in &#8377; billion)        400.00                     Total amount of offers received (in &#8377; billion)        612.62                    Amount accepted (in &#8377; billion)        400.07                    Cut off Rate ...",
                        "pubDate": "Fri, 16 Nov 2018 16:25:00 GMT",
                        "permaLink": "",
                        "id": "0000587"
                    },
                    {
                        "title": "Overseas Direct Investment for October 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45500",
                        "body": "The Reserve Bank of India has today released the data on Overseas Direct Investment, both under Automatic Route and the Approval Route, for the month of October 2018.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1141",
                        "pubDate": "Fri, 16 Nov 2018 13:05:00 GMT",
                        "permaLink": "",
                        "id": "0000584"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on October 16, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45499",
                        "body": "",
                        "pubDate": "Fri, 16 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000583"
                    },
                    {
                        "title": "Money Market Operations as on November 15, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45498",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Fri, 16 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000582"
                    },
                    {
                        "title": "RBI to conduct 3-day Variable rate Reverse Repo auction under LAF on November 16, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45496",
                        "body": "",
                        "pubDate": "Thu, 15 Nov 2018 19:05:00 GMT",
                        "permaLink": "",
                        "id": "0000580"
                    },
                    {
                        "title": "Cancellation of Certificate of Authorisation –  MMP Mobi Wallet Payment Systems Ltd",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45495",
                        "body": "The Reserve Bank of India, in exercise of the powers conferred on it under Payment and Settlement Systems Act, 2007, has cancelled the Certificate of Authorisation (COA) of the following Payment System Operator (PSO) on account of voluntary surrender of authorisation by the ...",
                        "pubDate": "Thu, 15 Nov 2018 18:20:00 GMT",
                        "permaLink": "",
                        "id": "0000579"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Thu, 15 Nov 2018 13:02:49 GMT",
                "item": [
                    {
                        "title": "Real Time Gross Settlement (RTGS) System - Implementation of Positive Confirmation",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11414&Mode=0",
                        "body": "RBI/2018-19/76    DPSS (CO) RTGS No.1049/04.04.016/2018-19    November 15, 2018    The Chairman / Managing Director / Chief Executive Officer    of member banks participating in RTGS    Madam / Dear Sir,    Real Time Gross Settlement (RTGS) System - Implementation of Positive ...",
                        "pubDate": "Thu, 15 Nov 2018 18:00:00 GMT",
                        "permaLink": "",
                        "id": "0000578"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Thu, 15 Nov 2018 12:06:24 GMT",
                "item": [
                    {
                        "title": "RBI releases Data on India’s International Trade in Services: September 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45490",
                        "body": "The Reserve Bank releases monthly data on India&rsquo;s international trade in services with a lag of around 45 days.    The value of exports and imports of services during the month of September 2018 are given in the following Table.                  Table: International Trade ...",
                        "pubDate": "Thu, 15 Nov 2018 17:30:00 GMT",
                        "permaLink": "",
                        "id": "0000573"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Thu, 15 Nov 2018 11:35:43 GMT",
                "item": [
                    {
                        "title": "Exim Bank's Government of India supported Line of Credit of USD 3.5 million to the Government of the Republic of Suriname",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11411&Mode=0",
                        "body": "RBI/2018-19/75    A.P. (DIR Series) Circular No. 14    November 15, 2018    All Category &ndash; I Authorised Dealer Banks    Madam/Sir    Exim Bank's Government of India supported Line of Credit of USD 3.5 million to the Government of the Republic of Suriname    Export-Import ...",
                        "pubDate": "Thu, 15 Nov 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000572"
                    },
                    {
                        "title": "Exim Bank's Government of India supported Line of Credit of USD 27.5 million to the Government of the Republic of Suriname",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11412&Mode=0",
                        "body": "RBI/2018-19/74    A.P. (DIR Series) Circular No.13    November 15, 2018    All Category &ndash; I Authorised Dealer Banks    Madam/Sir    Exim Bank's Government of India supported Line of Credit of USD 27.5 million to the    Government of the Republic of Suriname    ...",
                        "pubDate": "Thu, 15 Nov 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000571"
                    },
                    {
                        "title": "Exim Bank's Government of India supported Line of Credit of USD 2.5 million to the Government of Madagascar",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11413&Mode=0",
                        "body": "RBI/2018-19/73    A.P. (DIR Series) Circular No.12    November 15, 2018    All Category &ndash; I Authorised Dealer Banks    Madam/Sir    Exim Bank's Government of India supported Line of Credit of USD 2.5 million to the    Government of Madagascar    Export-Import Bank of India ...",
                        "pubDate": "Thu, 15 Nov 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000570"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Thu, 15 Nov 2018 10:33:52 GMT",
                "item": [
                    {
                        "title": "Results of OMO purchase auction held on November 15, 2018 and Settlement on November 16, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45488",
                        "body": "",
                        "pubDate": "Thu, 15 Nov 2018 15:55:00 GMT",
                        "permaLink": "",
                        "id": "0000568"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 33 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45487",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                       Sl.          No.          Name of the Company          ...",
                        "pubDate": "Thu, 15 Nov 2018 15:55:00 GMT",
                        "permaLink": "",
                        "id": "0000566"
                    },
                    {
                        "title": "Directions issued to the Shivam Sahakari Bank Ltd., Ichalkaranji, Dist. - Kolhapur, Maharashtra – Extension of Period",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45485",
                        "body": "The Reserve Bank of India (vide directive DCBS.CO.BSD-I/D-6/12.22.351/ 2017-18 dated May 18, 2018) had placed the Shivam Sahakari Bank Ltd., Ichalkaranji, Dist: Kolhapur, Maharashtra, under Directions from the close of business on May 19, 2018.    It is hereby notified for the ...",
                        "pubDate": "Thu, 15 Nov 2018 14:30:00 GMT",
                        "permaLink": "",
                        "id": "0000565"
                    },
                    {
                        "title": "Results of Underwriting Auctions Conducted on November 15, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45484",
                        "body": "",
                        "pubDate": "Thu, 15 Nov 2018 14:05:00 GMT",
                        "permaLink": "",
                        "id": "0000564"
                    },
                    {
                        "title": "Money Market Operations as on November 14, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45483",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Thu, 15 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000561"
                    },
                    {
                        "title": "RBI imposes monetary penalty on  The Jammu and Kashmir Bank Ltd",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45480",
                        "body": "The Reserve Bank of India (RBI) has imposed, by an order dated November 05, 2018, a monetary penalty of &#8377; 30 million on The Jammu and Kashmir Bank Ltd. (the bank) for non-compliance with the directions issued by RBI on Income Recognition and Asset Classification (IRAC) ...",
                        "pubDate": "Wed, 14 Nov 2018 18:05:00 GMT",
                        "permaLink": "",
                        "id": "0000558"
                    },
                    {
                        "title": "RBI imposes monetary penalty on on Deutsche Bank A.G",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45481",
                        "body": "The Reserve Bank of India (RBI) has imposed, by an order dated November 05, 2018, a monetary penalty of &#8377; 30.10 million on Deutsche Bank A.G. (the bank) for non-compliance with the directions issued by RBI on Income Recognition and Asset Classification (IRAC) norms, Know ...",
                        "pubDate": "Wed, 14 Nov 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000557"
                    },
                    {
                        "title": "Monthly Bulletin for November 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45479",
                        "body": "The Reserve Bank of India today released the November 2018 issue of its monthly Bulletin. The Bulletin includes three speeches by the top management, two articles and Current Statistics.    Jose J. Kattoor    Chief General Manager    Press Release: 2018-2019/1118",
                        "pubDate": "Wed, 14 Nov 2018 18:00:00 GMT",
                        "permaLink": "",
                        "id": "0000556"
                    },
                    {
                        "title": "Subsidiary General Ledger Account and Constituents' Subsidiary General Ledger Account: Eligibility Criteria and Operational Guidelines",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45478",
                        "body": "In exercise of powers conferred by Section 4 of Government Securities Act 2006, Reserve Bank of India had notified the conditions applicable for opening and maintaining of Subsidiary General Ledger (SGL) Accounts and Constituents&rsquo; Subsidiary General Ledger (CSGL) Accounts ...",
                        "pubDate": "Wed, 14 Nov 2018 17:40:00 GMT",
                        "permaLink": "",
                        "id": "0000555"
                    },
                    {
                        "title": "Reserve Money for the week ended November 9, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45477",
                        "body": "The Reserve Bank of India has today released data on Reserve Money for the week ended November 9, 2018.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1120",
                        "pubDate": "Wed, 14 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000554"
                    },
                    {
                        "title": "Survey on International Trade in Banking Services: 2017-18",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45476",
                        "body": "",
                        "pubDate": "Wed, 14 Nov 2018 17:20:00 GMT",
                        "permaLink": "",
                        "id": "0000553"
                    },
                    {
                        "title": "Underwriting Auction for sale of Government Securities for &#8377; 12,000 cr on November 15, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45471",
                        "body": "",
                        "pubDate": "Wed, 14 Nov 2018 14:50:00 GMT",
                        "permaLink": "",
                        "id": "0000547"
                    },
                    {
                        "title": "Money Market Operations as on November 13, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45465",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Wed, 14 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000539"
                    },
                    {
                        "title": "Results of Auctions of 4 to 25 years State Development Loans of  Ten State Governments - Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45462",
                        "body": "",
                        "pubDate": "Tue, 13 Nov 2018 17:40:00 GMT",
                        "permaLink": "",
                        "id": "0000535"
                    },
                    {
                        "title": "Result of the 56-day Variable Rate Repo Auction held on November 13, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45460",
                        "body": "Tenor        56-day                    Notified Amount (in &#8377; billion)        200.00                    Total amount of bids received (in &#8377; billion)        422.00                    Amount allotted (in &#8377; billion)        200.02                    Cut off Rate (%) ...",
                        "pubDate": "Tue, 13 Nov 2018 16:25:00 GMT",
                        "permaLink": "",
                        "id": "0000534"
                    },
                    {
                        "title": "Result of Yield/ Price Based Auctions of State Development Loans of State Governments",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45461",
                        "body": "",
                        "pubDate": "Tue, 13 Nov 2018 16:35:00 GMT",
                        "permaLink": "",
                        "id": "0000533"
                    },
                    {
                        "title": "Result of the 28-day Variable Rate Repo Auction held on November 13, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45458",
                        "body": "",
                        "pubDate": "Tue, 13 Nov 2018 13:45:00 GMT",
                        "permaLink": "",
                        "id": "0000531"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 31 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45457",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                        Sl. No.          Name of the Company          ...",
                        "pubDate": "Tue, 13 Nov 2018 12:55:00 GMT",
                        "permaLink": "",
                        "id": "0000530"
                    },
                    {
                        "title": "Result of the 14-day Variable rate Repo auction held on November 13, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45455",
                        "body": "",
                        "pubDate": "Tue, 13 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000528"
                    },
                    {
                        "title": "RBI to conduct 28-day and 56-day Variable rate Repo auction under LAF on November 13, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45454",
                        "body": "",
                        "pubDate": "Tue, 13 Nov 2018 12:05:00 GMT",
                        "permaLink": "",
                        "id": "0000527"
                    },
                    {
                        "title": "Money Market Operations as on November 12, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45453",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Tue, 13 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000526"
                    },
                    {
                        "title": "RBI cancels Certificate of Registration of 30 NBFCs",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45449",
                        "body": "The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has cancelled the Certificate of Registration of the following companies.                              Sl. No.            Name of the Company          ...",
                        "pubDate": "Mon, 12 Nov 2018 14:15:00 GMT",
                        "permaLink": "",
                        "id": "0000517"
                    },
                    {
                        "title": "Money Market Operations as on November 09, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45447",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Mon, 12 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000515"
                    },
                    {
                        "title": "RBI issues Directions to The Adoor Co-operative Urban Bank Ltd,  Adoor, Kerala",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45445",
                        "body": "The Reserve Bank of India (vide directive DCBS.CO.PCC D-4/12.26.004/2018-19 dated November 02, 2018) has placed the The Adoor Co-operative Urban Bank Ltd, Adoor, Kerala under Directions. According to the Directions, depositors will be allowed to withdraw a sum not exceeding ...",
                        "pubDate": "Fri, 09 Nov 2018 19:15:00 GMT",
                        "permaLink": "",
                        "id": "0000510"
                    },
                    {
                        "title": "Reserve Money for the week ended November 02, 2018 and Money Supply for the fortnight ended October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45443",
                        "body": "The Reserve Bank has today released data on Reserve Money for the week ended November 02, 2018 and Money Supply for the fortnight ended October 26, 2018.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1083",
                        "pubDate": "Fri, 09 Nov 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000507"
                    },
                    {
                        "title": "Directions under section 35A of the Banking Regulation Act, 1949 (AACS) - Sikar Urban Co-operative Bank Ltd; Sikar (Rajasthan)",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45442",
                        "body": "It is hereby notified for the information of the public that in exercise of powers vested in it under sub-section (1) of Section 35A of the Banking Regulation Act, 1949 (AACS) read with Section 56 of the Banking Regulation Act, 1949, the Reserve Bank of India (RBI) has issued ...",
                        "pubDate": "Fri, 09 Nov 2018 17:25:00 GMT",
                        "permaLink": "",
                        "id": "0000506"
                    },
                    {
                        "title": "Scheduled Banks’ Statement of Position in India as on Friday, October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45435",
                        "body": "(Amount in Billions of &#8377;)                      &nbsp;        &nbsp;        SCHEDULED COMMERCIAL BANKS           (Including RRBs)        ALL SCHEDULED BANKS                    &nbsp;        &nbsp;        27-Oct-17        12-OCT-2018 *        26-OCT-2018 *        27-Oct-17   ...",
                        "pubDate": "Fri, 09 Nov 2018 14:45:00 GMT",
                        "permaLink": "",
                        "id": "0000498"
                    },
                    {
                        "title": "17 NBFCs surrender their Certificate of Registration to RBI",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45434",
                        "body": "The following NBFCs have surrendered the Certificate of Registration granted to them by the Reserve Bank of India. The Reserve Bank of India, in exercise of powers conferred on it under Section 45-IA (6) of the Reserve Bank of India Act, 1934, has therefore cancelled their ...",
                        "pubDate": "Fri, 09 Nov 2018 14:10:00 GMT",
                        "permaLink": "",
                        "id": "0000497"
                    },
                    {
                        "title": "Money Market Operations as on November 08, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45433",
                        "body": "",
                        "pubDate": "Fri, 09 Nov 2018 12:50:00 GMT",
                        "permaLink": "",
                        "id": "0000496"
                    },
                    {
                        "title": "Result of the 13-day Variable Rate Repo Auction held on November 09, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45432",
                        "body": "",
                        "pubDate": "Fri, 09 Nov 2018 12:10:00 GMT",
                        "permaLink": "",
                        "id": "0000495"
                    },
                    {
                        "title": "Money Market Operations as on November 06, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45429",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Fri, 09 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000492"
                    }
                ]
            },
            {
                "feedTitle": "PUBLICATION FROM RBI",
                "feedUrl": "https://www.rbi.org.in/Publication_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Publication.",
                "whenLastUpdate": "Tue, 06 Nov 2018 13:06:47 GMT",
                "item": [
                    {
                        "title": "The Reserve Bank of India Act, 1934",
                        "link": "http://www.rbi.org.in/scripts/PublicationsView.aspx?id=6449",
                        "body": "",
                        "pubDate": "Tue, 06 Nov 2018 18:30:00 GMT",
                        "permaLink": "",
                        "id": "0000478"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Tue, 06 Nov 2018 12:05:47 GMT",
                "item": [
                    {
                        "title": "RBI announces rate of interest on Floating Rate Bonds, 2024",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45427",
                        "body": "The rate of interest on the Floating Rate Bonds, 2024 (FRB 2024) applicable for the half year November 07, 2018 to May 06, 2019 shall be 7.22 percent per annum.    It may be recalled that the rate of interest on the FRB, 2024 was set at the average rate (rounded off up to two ...",
                        "pubDate": "Tue, 06 Nov 2018 17:25:00 GMT",
                        "permaLink": "",
                        "id": "0000477"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Tue, 06 Nov 2018 12:05:47 GMT",
                "item": [
                    {
                        "title": "External Commercial Borrowings (ECB) Policy – Review of Minimum Average Maturity and Hedging Provisions",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11408&Mode=0",
                        "body": "",
                        "pubDate": "Tue, 06 Nov 2018 17:25:00 GMT",
                        "permaLink": "",
                        "id": "0000476"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Tue, 06 Nov 2018 10:59:09 GMT",
                "item": [
                    {
                        "title": "Results of OMO purchase auction held on November 6, 2018 and Settlement on November 9, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45426",
                        "body": "",
                        "pubDate": "Tue, 06 Nov 2018 16:20:00 GMT",
                        "permaLink": "",
                        "id": "0000475"
                    },
                    {
                        "title": "Result of the 14-day Variable rate Repo auction held on November 06, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45416",
                        "body": "",
                        "pubDate": "Tue, 06 Nov 2018 12:10:00 GMT",
                        "permaLink": "",
                        "id": "0000463"
                    },
                    {
                        "title": "Money Market Operations as on November 05, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45415",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Tue, 06 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000462"
                    },
                    {
                        "title": "Reserve Bank of India imposes monetary penalty on FINO Payments Bank Limited",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45413",
                        "body": "The Reserve Bank of India (RBI) has, on October 31, 2018, imposed a monetary penalty of &#8377; 10 million on FINO Payments Bank Limited (the bank) for contravention of the direction to stop opening of new accounts until further instructions on account of violation of certain ...",
                        "pubDate": "Mon, 05 Nov 2018 17:45:00 GMT",
                        "permaLink": "",
                        "id": "0000459"
                    },
                    {
                        "title": "Mint Street Memos (MSM) No. 15: Housing Services in CPI – Measurement Issues",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45412",
                        "body": "The Reserve Bank of India today placed on its website the fifteenth release under the series &lsquo;Mint Street Memos (MSM)&rsquo; titled &ldquo;Housing Services in CPI &ndash; Measurement Issues&rdquo;.    The paper authored by Dr. Praggya Das from Monetary Policy Department ...",
                        "pubDate": "Mon, 05 Nov 2018 17:30:00 GMT",
                        "permaLink": "",
                        "id": "0000458"
                    },
                    {
                        "title": "Results of Auctions of 3 to 25 years State Development Loans of  Twelve State Governments - Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45411",
                        "body": "",
                        "pubDate": "Mon, 05 Nov 2018 17:20:00 GMT",
                        "permaLink": "",
                        "id": "0000457"
                    },
                    {
                        "title": "Directions under Section 35A of the Banking Regulation Act, 1949 (AACS) – Vasantdada Nagari Sahakari Bank Ltd, Osmanabad, Maharashtra –  Extension of period",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45410",
                        "body": "Reserve Bank of India, in the public interest, had issued directions to Vasantdada Nagari Sahakari Bank Ltd, Osmanabad, Maharashtra in exercise of powers vested in it under sub-section (1) of Section 35A of the Banking Regulation Act, 1949 (AACS) from the close of business on ...",
                        "pubDate": "Mon, 05 Nov 2018 16:50:00 GMT",
                        "permaLink": "",
                        "id": "0000456"
                    },
                    {
                        "title": "Directions u/s 35A of the Banking Regulation Act, 1949 (AACS) – Gomati Nagariya Sahakari Bank Ltd., Jaunpur (Uttar Pradesh) - Extension of period",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45408",
                        "body": "The Reserve Bank of India (RBI) has extended the Directions issued to the Gomati Nagariya Sahakari Bank Ltd., Jaunpur (Uttar Pradesh) for a further period of six months from November 11, 2018 to May 10, 2019, subject to review. The bank has been under directions u/s 35A of the ...",
                        "pubDate": "Mon, 05 Nov 2018 15:45:00 GMT",
                        "permaLink": "",
                        "id": "0000453"
                    },
                    {
                        "title": "Result of the Overnight Variable Rate Reverse Repo Auction held on November 05, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45407",
                        "body": "Tenor        Overnight                    Notified Amount (in &#8377; billion)        600.00                    Total amount of offers received (in &#8377; billion)        506.67                    Amount accepted (in &#8377; billion)        506.67                    Cut off ...",
                        "pubDate": "Mon, 05 Nov 2018 15:15:00 GMT",
                        "permaLink": "",
                        "id": "0000451"
                    },
                    {
                        "title": "Money Market Operations as on November 03, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45405",
                        "body": "",
                        "pubDate": "Mon, 05 Nov 2018 11:55:00 GMT",
                        "permaLink": "",
                        "id": "0000450"
                    },
                    {
                        "title": "RBI to conduct Overnight Variable rate Reverse Repo auction under LAF on November 5, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45404",
                        "body": "",
                        "pubDate": "Mon, 05 Nov 2018 10:35:00 GMT",
                        "permaLink": "",
                        "id": "0000449"
                    },
                    {
                        "title": "Money Market Operations as on November 02, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45401",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Mon, 05 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000446"
                    }
                ]
            },
            {
                "feedTitle": "SPEECHES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/speeches_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Speeches.",
                "whenLastUpdate": "Fri, 02 Nov 2018 15:15:22 GMT",
                "item": [
                    {
                        "title": "Some Thoughts on Credit Risk and Bank Capital Regulation - Shri N.S. Vishwanathan, Deputy Governor, Reserve Bank of India - October 29, 2018 - Delivered at  XLRI, Jamshedpur",
                        "link": "http://www.rbi.org.in/scripts/BS_SpeechesView.aspx?id=1067",
                        "body": "",
                        "pubDate": "Fri, 02 Nov 2018 20:25:00 GMT",
                        "permaLink": "",
                        "id": "0000439"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Fri, 02 Nov 2018 12:47:17 GMT",
                "item": [
                    {
                        "title": "Partial Credit Enhancement to Bonds Issued by Non-Banking Financial  Companies and Housing Finance Companies",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11407&Mode=0",
                        "body": "",
                        "pubDate": "Fri, 02 Nov 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000438"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Fri, 02 Nov 2018 12:32:17 GMT",
                "item": [
                    {
                        "title": "Sovereign Gold Bond 2018-19 Series-III-Issue Price",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45399",
                        "body": "In terms of GoI notification F.No.4(22)-W&amp;M/2018 and RBI circular IDMD.CDD.No.821/14.04.050/2018-19 dated October 08, 2018, the Sovereign Gold Bond Scheme 2018-19 - Series III will be opened for subscription for the period from November 05, 2018 to November 09, 2018. The ...",
                        "pubDate": "Fri, 02 Nov 2018 17:50:00 GMT",
                        "permaLink": "",
                        "id": "0000436"
                    },
                    {
                        "title": "Result of the 3-day Variable Rate Reverse Repo Auction held on November 2, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45396",
                        "body": "Tenor          3-day                          Notified Amount (in &#8377; billion)          300.00                          Total amount of offers received (in &#8377; billion)          378.33                          Amount accepted (in &#8377; billion)          300.10          ...",
                        "pubDate": "Fri, 02 Nov 2018 16:15:00 GMT",
                        "permaLink": "",
                        "id": "0000434"
                    },
                    {
                        "title": "Directions under Section 35A read with Section 56 of the Banking Regulation Act, 1949 (AACS) – The Karad Janata Sahakari Bank Ltd., Karad, Maharashtra- Extension of period",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45397",
                        "body": "The Karad Janata Sahakari Bank Ltd., Karad was placed under directions vide directive DCBS.CO.BSD-I/D-4/12.22.126/2017-18 dated November 07, 2017 from the close of business on November 09, 2017 for a period of six months. The validity of the above directions were extended by ...",
                        "pubDate": "Fri, 02 Nov 2018 16:25:00 GMT",
                        "permaLink": "",
                        "id": "0000433"
                    },
                    {
                        "title": "RBI to conduct 3-day Variable Rate Reverse Repo Auction under LAF on November 2, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45392",
                        "body": "The Reserve Bank of India will conduct the following Variable Rate Reverse Repo Auction on November 2, 2018, Friday, as per the revised guidelines on Term Repo Auctions issued on February 13, 2014.                         Sl. No.          Notified Amount             (&#8377; ...",
                        "pubDate": "Fri, 02 Nov 2018 12:40:00 GMT",
                        "permaLink": "",
                        "id": "0000427"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on November 02, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45391",
                        "body": "",
                        "pubDate": "Fri, 02 Nov 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000426"
                    },
                    {
                        "title": "Money Market Operations as on November 01, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45390",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Fri, 02 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000424"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Thu, 01 Nov 2018 11:35:18 GMT",
                "item": [
                    {
                        "title": "Alteration in the name of \"Qatar National Bank SAQ\" to \"Qatar National Bank (Q.P.S.C.)\" in the Second Schedule to the Reserve Bank of India Act, 1934",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11406&Mode=0",
                        "body": "RBI/2018-19/69    DBR.No.Ret.BC.06/12.06.149/2018-19    November 01, 2018    All Scheduled Commercial Banks    Dear Sir/Madam    Alteration in the name of &quot;Qatar National Bank SAQ&quot; to &quot;Qatar National Bank (Q.P.S.C.)&quot; in the Second Schedule to the Reserve Bank ...",
                        "pubDate": "Thu, 01 Nov 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000412"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Thu, 01 Nov 2018 10:39:17 GMT",
                "item": [
                    {
                        "title": "Results of OMO purchase auction held on November 1, 2018 and Settlement on November 2, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45383",
                        "body": "",
                        "pubDate": "Thu, 01 Nov 2018 16:00:00 GMT",
                        "permaLink": "",
                        "id": "0000411"
                    },
                    {
                        "title": "Result of the 4-day Variable Rate Reverse Repo Auction held on  November 1,  2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45382",
                        "body": "",
                        "pubDate": "Thu, 01 Nov 2018 15:20:00 GMT",
                        "permaLink": "",
                        "id": "0000409"
                    },
                    {
                        "title": "Results of Underwriting Auctions Conducted on November 1, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45380",
                        "body": "",
                        "pubDate": "Thu, 01 Nov 2018 13:20:00 GMT",
                        "permaLink": "",
                        "id": "0000408"
                    },
                    {
                        "title": "RBI launches Quarterly Order Books, Inventories and Capacity Utilisation Survey: July-September 2018 (Round 43)",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45379",
                        "body": "The Reserve Bank of India has launched the 43rd round of its Order Books, Inventories and Capacity Utilisation Survey (OBICUS). The survey is for the reference period July- September 2018 (Q2:2018-19).     The Reserve Bank has been conducting the Order Books, Inventories and ...",
                        "pubDate": "Thu, 01 Nov 2018 12:05:00 GMT",
                        "permaLink": "",
                        "id": "0000407"
                    },
                    {
                        "title": "Money Market Operations as on October 31, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45378",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Thu, 01 Nov 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000405"
                    },
                    {
                        "title": "Sectoral Deployment of Bank Credit – September 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45374",
                        "body": "Data on sectoral deployment of bank credit collected from select 41 scheduled commercial banks, accounting for about 90 per cent of the total non-food credit deployed by all scheduled commercial banks, for the month of September 2018 are set out in Statements I and II.    ...",
                        "pubDate": "Wed, 31 Oct 2018 17:30:00 GMT",
                        "permaLink": "",
                        "id": "0000394"
                    },
                    {
                        "title": "Reserve Money for the week ended October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45375",
                        "body": "The Reserve Bank of India has today released data on Reserve Money for the week ended October 26, 2018.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1017",
                        "pubDate": "Wed, 31 Oct 2018 17:35:00 GMT",
                        "permaLink": "",
                        "id": "0000393"
                    },
                    {
                        "title": "RBI to conduct 4-day Variable Rate Reverse Repo auction under LAF on November 1, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45376",
                        "body": "The Reserve Bank of India will conduct the following Variable rate Reverse Repo auction on November 1, 2018, Thursday, as per the revised guidelines on Term Repo Auctions issued on February 13, 2014.                        Sl. No.          Notified Amount                      ...",
                        "pubDate": "Wed, 31 Oct 2018 18:40:00 GMT",
                        "permaLink": "",
                        "id": "0000392"
                    },
                    {
                        "title": "Underwriting Auction for sale of Government Securities for &#8377; 11,000 cr on November 1, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45365",
                        "body": "The Government of India has announced the sale (issue/re-issue) of Government Stock detailed below through auctions to be held on November 2, 2018.    As per revised scheme of underwriting notified on November 14, 2007, the amounts of Minimum Underwriting Commitment (MUC) and ...",
                        "pubDate": "Wed, 31 Oct 2018 11:55:00 GMT",
                        "permaLink": "",
                        "id": "0000391"
                    },
                    {
                        "title": "RBI imposes monetary penalty on Dr. Ambedkar Nagrik Sahakari Bank Maryadit, Gwalior, Madhya Pradesh",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45366",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377;50,000/- (Rupees Fifty Thousand only) on the Dr. Ambedkar Nagrik Sahakari Bank Maryadit, Gwalior, Madhya Pradesh in exercise of the powers vested in it under the provisions of Section 47A(1)(c) read with Section ...",
                        "pubDate": "Wed, 31 Oct 2018 14:05:00 GMT",
                        "permaLink": "",
                        "id": "0000390"
                    },
                    {
                        "title": "Money Market Operations as on October 30, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45364",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Wed, 31 Oct 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000386"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on October 30, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45355",
                        "body": "Tenor        14-day                    Notified Amount (in &#8377; billion)         235.00                    Total amount of bids received (in &#8377; billion)        417.99                    Amount allotted (in &#8377; billion)        235.00                    Cut off Rate ...",
                        "pubDate": "Tue, 30 Oct 2018 12:05:00 GMT",
                        "permaLink": "",
                        "id": "0000380"
                    },
                    {
                        "title": "Results of Auctions of 3 to 14 years State Development Loans of Eight State Governments - Full Auction Result",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45359",
                        "body": "The Results of the auctions of 3 to 14 years State Development Loans for Eight State Governments held on October 30, 2018.                        Table                          (&#8377; in crore)                          &nbsp;          Andhra Pradesh          Chhattisgarh       ...",
                        "pubDate": "Tue, 30 Oct 2018 17:05:00 GMT",
                        "permaLink": "",
                        "id": "0000376"
                    },
                    {
                        "title": "Data on India’s Invisibles for First Quarter  (April- June) 2018-19",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45360",
                        "body": "The Reserve Bank today released data on India&rsquo;s invisibles as per the IMF&rsquo;s Balance of Payments and International Investment Position Manual (BPM6) format for April-June of 2018-19.    Ajit Prasad    Assistant Adviser    Press Release: 2018-2019/1002",
                        "pubDate": "Tue, 30 Oct 2018 17:05:00 GMT",
                        "permaLink": "",
                        "id": "0000375"
                    },
                    {
                        "title": "RBI releases data on ECB/FCCB/RDB for September 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45361",
                        "body": "The Reserve Bank of India has today released the data on External Commercial Borrowings (ECB), Foreign Currency Convertible Bonds (FCCB) and Rupee Denominated Bonds (RDB) both, through Automatic Route and Approval Route, for the month of September 2018.    Ajit Prasad    ...",
                        "pubDate": "Tue, 30 Oct 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000374"
                    },
                    {
                        "title": "Money Market Operations as on October 29, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45354",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Tue, 30 Oct 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000371"
                    },
                    {
                        "title": "RBI issues Directions to The Needs of Life Co-operative Bank Ltd. Mumbai, Maharashtra",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45352",
                        "body": "The Reserve Bank of India (vide directive DCBS.CO.BSD-I/D-3/12.22.163/2018-19 dated October 26, 2018) has placed the The Needs of Life Co-operative Bank Ltd. Mumbai, Maharashtra, under Directions. According to the Directions, depositors will be allowed to withdraw a sum not ...",
                        "pubDate": "Mon, 29 Oct 2018 19:25:00 GMT",
                        "permaLink": "",
                        "id": "0000357"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Mon, 29 Oct 2018 14:35:38 GMT",
                "item": [
                    {
                        "title": "Auction for Sale (Issue/Re-issue) of Government Stock (GS)",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11403&Mode=0",
                        "body": "Government of India      Ministry of Finance    Department of Economic Affairs    Budget Division    New Delhi, dated Oct 29, 2018    NOTIFICATION    Auction for Sale (Issue/Re-issue) of Government Stock (GS)    F.No.4(6)W&amp;M/2018: Government of India(GoI) hereby notifies ...",
                        "pubDate": "Mon, 29 Oct 2018 19:05:00 GMT",
                        "permaLink": "",
                        "id": "0000356"
                    },
                    {
                        "title": "Payments Bank and Small Finance Banks– access to Call/Notice/Term Money Market",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11405&Mode=0",
                        "body": "RBI/2018-19/68    FMRD.DIRD.09/14.01.001/2018-19    October 29, 2018    All Eligible Market Participants    Madam/Sir,    Payments Bank and Small Finance Banks&ndash; access to Call/Notice/Term Money Market    A reference is invited to Master Direction No.2/2016-17, dated July ...",
                        "pubDate": "Mon, 29 Oct 2018 19:10:00 GMT",
                        "permaLink": "",
                        "id": "0000354"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Mon, 29 Oct 2018 12:47:58 GMT",
                "item": [
                    {
                        "title": "RBI extends timing of fixed rate LAF Repo operation",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45351",
                        "body": "",
                        "pubDate": "Mon, 29 Oct 2018 18:10:00 GMT",
                        "permaLink": "",
                        "id": "0000353"
                    },
                    {
                        "title": "NSDL Payments Bank Limited commences operations",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45349",
                        "body": "NSDL Payments Bank Limited has commenced operations as a payments bank with effect from October 29, 2018. The Reserve Bank has issued a licence to the bank under Section 22 (1) of the Banking Regulation Act, 1949 to carry on the business of payments bank in India.    National ...",
                        "pubDate": "Mon, 29 Oct 2018 17:15:00 GMT",
                        "permaLink": "",
                        "id": "0000352"
                    },
                    {
                        "title": "Government of India announces the sale of five dated securities  for &#8377; 11,000 crore",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45350",
                        "body": "",
                        "pubDate": "Mon, 29 Oct 2018 17:55:00 GMT",
                        "permaLink": "",
                        "id": "0000351"
                    },
                    {
                        "title": "Quarterly Industrial Outlook Survey (IOS): October –  December 2018 (Round 84)",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45348",
                        "body": "The Reserve Bank of India has launched the 84th round of the quarterly Industrial Outlook Survey (IOS) of the Indian manufacturing sector for the reference period October-December 2018 (Q3:2018-19). The survey assesses business sentiment for the current quarter and expectations ...",
                        "pubDate": "Mon, 29 Oct 2018 15:15:00 GMT",
                        "permaLink": "",
                        "id": "0000348"
                    },
                    {
                        "title": "Money Market Operations as on October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45345",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Mon, 29 Oct 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000345"
                    }
                ]
            },
            {
                "feedTitle": "SPEECHES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/speeches_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Speeches.",
                "whenLastUpdate": "Fri, 26 Oct 2018 13:08:33 GMT",
                "item": [
                    {
                        "title": "On the Importance of Independent Regulatory Institutions – The Case of the Central Bank - Dr. Viral V Acharya, Deputy Governor, Reserve Bank of India - October 26, 2018 - Delivered as the A. D. Shroff Memorial Lecture, Mumbai",
                        "link": "http://www.rbi.org.in/scripts/BS_SpeechesView.aspx?id=1066",
                        "body": "No analogy is perfect; yet, analogies help convey things better. At times, a straw man has to be set up to make succinctly a practical or even an academic point. Occasionally, however, real life examples come along beautifully to make a communicator&rsquo;s work easier. Let me ...",
                        "pubDate": "Fri, 26 Oct 2018 18:30:00 GMT",
                        "permaLink": "",
                        "id": "0000316"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Fri, 26 Oct 2018 13:08:32 GMT",
                "item": [
                    {
                        "title": "Overseas Direct Investment for September 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45343",
                        "body": "The Reserve Bank of India has today released the data on Overseas Direct Investment, both under Automatic Route and the Approval Route, for the month of September 2018.    Ajit Prasad      Assistant Adviser    Press Release: 2018-2019/981",
                        "pubDate": "Fri, 26 Oct 2018 18:30:00 GMT",
                        "permaLink": "",
                        "id": "0000315"
                    },
                    {
                        "title": "RBI to inject durable liquidity through OMO purchase auctions  in November 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45338",
                        "body": "Based on an assessment of the durable liquidity needs going forward, RBI has decided to conduct purchase of Government securities under Open Market Operations (OMOs) for an aggregate amount of &#8377;400 billion in the month of November 2018.    The auction dates and the ...",
                        "pubDate": "Fri, 26 Oct 2018 17:20:00 GMT",
                        "permaLink": "",
                        "id": "0000309"
                    },
                    {
                        "title": "Result of the 14-day Variable Rate Repo Auction held on October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45332",
                        "body": "Tenor          14-day                          Notified Amount (in &#8377; billion)          235.00                          Total amount of bids received (in &#8377; billion)          615.40                          Amount allotted (in &#8377; billion)          235.04           ...",
                        "pubDate": "Fri, 26 Oct 2018 12:25:00 GMT",
                        "permaLink": "",
                        "id": "0000304"
                    },
                    {
                        "title": "Money Market Operations as on October 25, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45331",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Fri, 26 Oct 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000298"
                    },
                    {
                        "title": "Results of OMO purchase auction held on October 25, 2018 and Settlement on October 26, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45328",
                        "body": "I. SUMMARY RESULTS                        Aggregate Amount (Face value) notified by RBI          : &#8377; 120.00 billion                          Total amount offered (Face value) by participants          : &#8377; 201.74 billion                          Total amount accepted ...",
                        "pubDate": "Thu, 25 Oct 2018 15:45:00 GMT",
                        "permaLink": "",
                        "id": "0000295"
                    },
                    {
                        "title": "Mandya City Co-operative Bank Ltd., Mandya, Karnataka – Penalised",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45329",
                        "body": "The Reserve Bank of India has imposed a monetary penalty of &#8377; 50,000/- (Rupees fifty thousand only) on Mandya City Co-operative Bank Ltd., Mandya, in exercise of the powers vested in it under the provisions of Section 47A read with Section 46(4) of the Banking Regulation ...",
                        "pubDate": "Thu, 25 Oct 2018 17:15:00 GMT",
                        "permaLink": "",
                        "id": "0000294"
                    }
                ]
            },
            {
                "feedTitle": "NOTIFICATIONS FROM RBI",
                "feedUrl": "https://www.rbi.org.in/notifications_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for notifications.",
                "whenLastUpdate": "Thu, 25 Oct 2018 14:16:33 GMT",
                "item": [
                    {
                        "title": "Fire Audit of Currency Chests - Clarification",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11401&Mode=0",
                        "body": "RBI/2018-19/66    DCM (CC) No.1083/03.39.01/2018-19    October 25, 2018    The Chairman &amp; Managing Director Chief Executive Officer / Managing DirectorAll Banks having Currency Chests    Madam / Dear Sir,    Fire Audit of Currency Chests - Clarification    Please refer to ...",
                        "pubDate": "Thu, 25 Oct 2018 17:05:00 GMT",
                        "permaLink": "",
                        "id": "0000293"
                    },
                    {
                        "title": "Master Direction - Fit and Proper Criteria for Sponsors - Asset Reconstruction Companies (Reserve Bank) Directions, 2018",
                        "link": "http://www.rbi.org.in/scripts/NotificationUser.aspx?Id=11402&Mode=0",
                        "body": "RBI/DNBR/2018-19/66    Master Direction DNBR. PD (ARC) CC. No. 06/26.03.001/2018-19       October 25, 2018      The Chairman / Managing Director / Chief Executive Officer      All registered Asset Reconstruction Companies      Dear Sir/ Madam,      Master Direction - Fit and ...",
                        "pubDate": "Thu, 25 Oct 2018 17:00:00 GMT",
                        "permaLink": "",
                        "id": "0000292"
                    }
                ]
            },
            {
                "feedTitle": "PRESS RELEASES FROM RBI",
                "feedUrl": "https://www.rbi.org.in/pressreleases_rss.xml",
                "websiteUrl": "http://www.rbi.org.in",
                "feedDescription": "This is Feed from RBI for Press Releases.",
                "whenLastUpdate": "Thu, 25 Oct 2018 08:09:55 GMT",
                "item": [
                    {
                        "title": "Money Market Operations as on October 24, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45323",
                        "body": "(Amount in &#8377; billion, Rate in Per cent)                                        MONEY MARKETS @        &nbsp;        &nbsp;        &nbsp;                            Volume        Wtd.Avg.Rate         Range                            (One Leg)                                 ...",
                        "pubDate": "Thu, 25 Oct 2018 09:00:00 GMT",
                        "permaLink": "",
                        "id": "0000290"
                    },
                    {
                        "title": "Results of Underwriting Auctions Conducted on October 25, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45326",
                        "body": "",
                        "pubDate": "Thu, 25 Oct 2018 13:20:00 GMT",
                        "permaLink": "",
                        "id": "0000287"
                    },
                    {
                        "title": "Underwriting Auction for sale of Government Securities for &#8377; 11,000 cr on October 25, 2018",
                        "link": "http://www.rbi.org.in/scripts/BS_PressReleaseDisplay.aspx?prid=45312",
                        "body": "Government of India has announced the sale (issue/re-issue) of Government Stock detailed below through auctions to be held on October 26, 2018.    As per revised scheme of underwriting notified on November 14, 2007, the amounts of Minimum Underwriting Commitment (MUC) and the ...",
                        "pubDate": "Wed, 24 Oct 2018 13:20:00 GMT",
                        "permaLink": "",
                        "id": "0000269"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "RBI.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.002,
        "ctBuilds": 206,
        "ctDuplicatesSkipped": 130,
        "whenGMT": "Wed, 28 Nov 2018 11:14:00 GMT",
        "whenLocal": "11/28/2018, 11:14:00 AM",
        "aggregator": "River5 v0.4.14"
    }
})