onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "Twitter search feed for: #cashlessconsumer.",
                "feedUrl": "https://twitrss.me/twitter_search_to_rss/?term=%23CashlessConsumer",
                "websiteUrl": "https://twitter.com/search?f=tweets&src=typd&q=#cashlessconsumer",
                "feedDescription": "Twitter search feed for: #cashlessconsumer.",
                "whenLastUpdate": "Tue, 27 Nov 2018 01:03:44 GMT",
                "item": [
                    {
                        "title": "#CashlessConsumer #RTI CCA, @GoI_MeitY passes the buck to ESPs to *independently* comply eKYC requirements. \n\nIt is different matter that there are no alternative eKYC service provider other than @UIDAI, which is affected by #S57CollapseImpactpic.twitter.com/HgJh9lvOQB",
                        "link": "https://twitter.com/logic/status/1066911815773302784",
                        "body": "#CashlessConsumer #RTI CCA, @GoI_MeitY passes the buck to ESPs to *independently* comply eKYC requirements. \n\nIt is different matter that there are no alternative eKYC service provider other than @UIDAI, which is affected by #S57CollapseImpactpic.twitter.com/HgJh9lvOQB",
                        "pubDate": "Mon, 26 Nov 2018 04:29:33 GMT",
                        "permaLink": "https://twitter.com/logic/status/1066911815773302784",
                        "id": "0000690"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked @RBI about #PCR\n\nRole of 'Public' / Law in #PublicCreditRegistry - NIL #SaveOurPrivacy\n\n#RegulatoryTransparency @RBIpic.twitter.com/gDpXvEnJc0",
                        "link": "https://twitter.com/logic/status/1066966311538999296",
                        "body": "#CashlessConsumer #RTI We asked @RBI about #PCR\n\nRole of 'Public' / Law in #PublicCreditRegistry - NIL #SaveOurPrivacy\n\n#RegulatoryTransparency @RBIpic.twitter.com/gDpXvEnJc0",
                        "pubDate": "Mon, 26 Nov 2018 08:06:06 GMT",
                        "permaLink": "https://twitter.com/logic/status/1066966311538999296",
                        "id": "0000689"
                    },
                    {
                        "title": "I will spare you, but to correct you #CashlessConsumer started first with aim to fix payments for consumer before I was pulled into Aadhaar hijack. Improvements to everything on payments will be made. Have a good day.",
                        "link": "https://twitter.com/logic/status/1066231498230050816",
                        "body": "I will spare you, but to correct you #CashlessConsumer started first with aim to fix payments for consumer before I was pulled into Aadhaar hijack. Improvements to everything on payments will be made. Have a good day.",
                        "pubDate": "Sat, 24 Nov 2018 07:26:13 GMT",
                        "permaLink": "https://twitter.com/logic/status/1066231498230050816",
                        "id": "0000672"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked @SEBI_India about the public consultation held for changing the listing process using #UPI. Information denied, because \"public\" consultation discuss national security concerns. \n\nAppeal has been filed\n\n#RegulatoryTransparency #PublicConsultationpic.twitter.com/j1rVsXI1w1",
                        "link": "https://twitter.com/logic/status/1065906965312880641",
                        "body": "#CashlessConsumer #RTI We asked @SEBI_India about the public consultation held for changing the listing process using #UPI. Information denied, because \"public\" consultation discuss national security concerns. \n\nAppeal has been filed\n\n#RegulatoryTransparency ...",
                        "pubDate": "Fri, 23 Nov 2018 09:56:38 GMT",
                        "permaLink": "https://twitter.com/logic/status/1065906965312880641",
                        "id": "0000670"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked @RBI about price regulation on #AePS #MicroATM and got the following response. #MDR #CostOfCash\n\ntakeaway - While RBI indulges in price control for debit cards, ATM networks (which predominantly impact middle class), it leaves it to market for poorpic.twitter.com/nvXK3iyR5E",
                        "link": "https://twitter.com/logic/status/1065882573505736704",
                        "body": "#CashlessConsumer #RTI We asked @RBI about price regulation on #AePS #MicroATM and got the following response. #MDR #CostOfCash\n\ntakeaway - While RBI indulges in price control for debit cards, ATM networks (which predominantly impact middle class), it leaves it to market for ...",
                        "pubDate": "Fri, 23 Nov 2018 08:19:43 GMT",
                        "permaLink": "https://twitter.com/logic/status/1065882573505736704",
                        "id": "0000669"
                    },
                    {
                        "title": "As credit gets embedded into payments so thickly, payments regulation, policy, consumer protection is not limited to paytech alone & there is a convergence of fintech. Time for #CashlessConsumer to expand horizons to all forms of fintech, need more hands to protect consumers.  https://twitter.com/anivar/status/1064779545062993920 …",
                        "link": "https://twitter.com/logic/status/1064790897274089472",
                        "body": "As credit gets embedded into payments so thickly, payments regulation, policy, consumer protection is not limited to paytech alone &amp; there is a convergence of fintech. Time for #CashlessConsumer to expand horizons to all forms of fintech, need more hands to protect ...",
                        "pubDate": "Tue, 20 Nov 2018 08:01:47 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064790897274089472",
                        "id": "0000654"
                    },
                    {
                        "title": "#DisasterPayments #SaveDelta #GajaCycloneRelief @CMOTamiINadu Public Relief Fund Details \n\nPAN of TN Govt AAAGC 0038 F\nBANK :INDIAN OVERSEAS BANK \nAC NO : 117201000000070 \nIFSC CODE : IOBA0001172 \nSECRETARIAT BRANCH \nCHENNAI 600 009\n\n#CashlessConsumer #Verified",
                        "link": "https://twitter.com/logic/status/1064813358136942592",
                        "body": "#DisasterPayments #SaveDelta #GajaCycloneRelief @CMOTamiINadu Public Relief Fund Details \n\nPAN of TN Govt AAAGC 0038 F\nBANK :INDIAN OVERSEAS BANK \nAC NO : 117201000000070 \nIFSC CODE : IOBA0001172 \nSECRETARIAT BRANCH \nCHENNAI 600 009\n\n#CashlessConsumer #Verified",
                        "pubDate": "Tue, 20 Nov 2018 09:31:02 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064813358136942592",
                        "id": "0000653"
                    },
                    {
                        "title": "#SaveDelta #GajaCycloneRelief @KeralaCMDRF Please reach out to Tamil Nadu counterparts in sharing best practices.#CashlessConsumer wrote about payments for disaster relief \n https://medium.com/cashlessconsumer/payments-for-disaster-relief-6fb7083a1e7a …",
                        "link": "https://twitter.com/logic/status/1064813364575170561",
                        "body": "#SaveDelta #GajaCycloneRelief @KeralaCMDRF Please reach out to Tamil Nadu counterparts in sharing best practices.#CashlessConsumer wrote about payments for disaster relief \n https://medium.com/cashlessconsumer/payments-for-disaster-relief-6fb7083a1e7a&nbsp;&hellip;",
                        "pubDate": "Tue, 20 Nov 2018 09:31:04 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064813364575170561",
                        "id": "0000652"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked CRIS of Indian Railways about @NPCI_BHIM @UPI_NPCI cashback, ticket volumes both in counter as well as online. #Opendata #DigitalPayments #Subsidy Real Numbers from Merchant.pic.twitter.com/0d2Js0zfBb",
                        "link": "https://twitter.com/logic/status/1064851774668976129",
                        "body": "#CashlessConsumer #RTI We asked CRIS of Indian Railways about @NPCI_BHIM @UPI_NPCI cashback, ticket volumes both in counter as well as online. #Opendata #DigitalPayments #Subsidy Real Numbers from Merchant.pic.twitter.com/0d2Js0zfBb",
                        "pubDate": "Tue, 20 Nov 2018 12:03:41 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064851774668976129",
                        "id": "0000651"
                    },
                    {
                        "title": "Positive confirmation for #RTGS transaction to be implemented within 2 months - @RBI \n https://m.rbi.org.in//scripts/NotificationUser.aspx?Id=11414&Mode=0 …\n\n#SMS confirmation on RTGS credits. #CashlessConsumer #DigitalPayments",
                        "link": "https://twitter.com/logic/status/1065079082361151489",
                        "body": "Positive confirmation for #RTGS transaction to be implemented within 2 months - @RBI \n https://m.rbi.org.in//scripts/NotificationUser.aspx?Id=11414&amp;Mode=0&nbsp;&hellip;\n\n#SMS confirmation on RTGS credits. #CashlessConsumer #DigitalPayments",
                        "pubDate": "Wed, 21 Nov 2018 03:06:56 GMT",
                        "permaLink": "https://twitter.com/logic/status/1065079082361151489",
                        "id": "0000650"
                    },
                    {
                        "title": "Exclusive: @RBI issues in-principle licenses to 5 Account Aggregators -  https://www.medianama.com/2018/11/223-exclusive-rbi-issues-in-principle-licenses-to-5-account-aggregators/ … #CashlessConsumer",
                        "link": "https://twitter.com/logic/status/1064414241363542019",
                        "body": "Exclusive: @RBI issues in-principle licenses to 5 Account Aggregators -  https://www.medianama.com/2018/11/223-exclusive-rbi-issues-in-principle-licenses-to-5-account-aggregators/&nbsp;&hellip; #CashlessConsumer",
                        "pubDate": "Mon, 19 Nov 2018 07:05:05 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064414241363542019",
                        "id": "0000609"
                    },
                    {
                        "title": "#CashlessConsumer Attempted to map Indian Financial Sector Data flows for upcoming article on NBFC-AA.  https://github.com/srikanthlogic/drawio-drawings/blob/master/Indian%20Financial%20Sector%20Data%20Flows.xml … \n\nPlay with @drawio and send in your pull requests. #DataEconomy #SaveOurPrivacy #DataPayments #PaymentsData #DataLocalisationpic.twitter.com/7lkO4Jjiva",
                        "link": "https://twitter.com/logic/status/1064363431459868672",
                        "body": "#CashlessConsumer Attempted to map Indian Financial Sector Data flows for upcoming article on NBFC-AA.  https://github.com/srikanthlogic/drawio-drawings/blob/master/Indian%20Financial%20Sector%20Data%20Flows.xml&nbsp;&hellip; \n\nPlay with @drawio and send in your pull requests. ...",
                        "pubDate": "Mon, 19 Nov 2018 03:43:11 GMT",
                        "permaLink": "https://twitter.com/logic/status/1064363431459868672",
                        "id": "0000606"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked CIC about procedure that describes structure of CIC judgement differentiates operative part from arguments, as well as list of entities that were exempt from RTI through CIC judgements. \n\nInformation does not exist. cc @rtiindia @YouRTI_IN",
                        "link": "https://twitter.com/logic/status/1063665544321150976",
                        "body": "#CashlessConsumer #RTI We asked CIC about procedure that describes structure of CIC judgement differentiates operative part from arguments, as well as list of entities that were exempt from RTI through CIC judgements. \n\nInformation does not exist. cc @rtiindia @YouRTI_IN",
                        "pubDate": "Sat, 17 Nov 2018 05:30:02 GMT",
                        "permaLink": "https://twitter.com/logic/status/1063665544321150976",
                        "id": "0000600"
                    },
                    {
                        "title": "For the payments / Financial Inclusion folks on my TL, maps x payments help in furthering inclusion. #Opendata helps in providing information on touchpoints of all kinds.  https://twitter.com/logic/status/1044575463249711105 … #CashlessConsumer",
                        "link": "https://twitter.com/logic/status/1061903216940670976",
                        "body": "For the payments / Financial Inclusion folks on my TL, maps x payments help in furthering inclusion. #Opendata helps in providing information on touchpoints of all kinds.  https://twitter.com/logic/status/1044575463249711105&nbsp;&hellip; #CashlessConsumer",
                        "pubDate": "Mon, 12 Nov 2018 08:47:10 GMT",
                        "permaLink": "https://twitter.com/logic/status/1061903216940670976",
                        "id": "0000519"
                    },
                    {
                        "title": "“CashlessConsumer - RTI” -- Testing out moments\n\n https://twitter.com/i/moments/1061443828454907904 …",
                        "link": "https://twitter.com/logic/status/1061445263947153408",
                        "body": "&ldquo;CashlessConsumer - RTI&rdquo; -- Testing out moments\n\n https://twitter.com/i/moments/1061443828454907904&nbsp;&hellip;",
                        "pubDate": "Sun, 11 Nov 2018 02:27:26 GMT",
                        "permaLink": "https://twitter.com/logic/status/1061445263947153408",
                        "id": "0000513"
                    },
                    {
                        "title": "Shall be done :) #CashlessConsumer #PaymentsTransparency #RTI https://medium.com/cashlessconsumer/payments-transparency-regulatory-penalties-national-security-exceptions-aa2e647806b …",
                        "link": "https://twitter.com/logic/status/1060898689013727232",
                        "body": "Shall be done :) #CashlessConsumer #PaymentsTransparency #RTI https://medium.com/cashlessconsumer/payments-transparency-regulatory-penalties-national-security-exceptions-aa2e647806b&nbsp;&hellip;",
                        "pubDate": "Fri, 09 Nov 2018 14:15:32 GMT",
                        "permaLink": "https://twitter.com/logic/status/1060898689013727232",
                        "id": "0000511"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked @NABARDOnline about usage of Financial Inclusion Fund and its role in improving digital payment infrastructure (both card issuance as well as improving acceptance infrastructure). Response attached. #FinancialInclusion #DigitalPaymentspic.twitter.com/Omu6tewsQG",
                        "link": "https://twitter.com/logic/status/1060846459074023424",
                        "body": "#CashlessConsumer #RTI We asked @NABARDOnline about usage of Financial Inclusion Fund and its role in improving digital payment infrastructure (both card issuance as well as improving acceptance infrastructure). Response attached. #FinancialInclusion ...",
                        "pubDate": "Fri, 09 Nov 2018 10:48:00 GMT",
                        "permaLink": "https://twitter.com/logic/status/1060846459074023424",
                        "id": "0000500"
                    },
                    {
                        "title": "#CashlessConsumer #RTI @RBI has received 9 applications for NBFC-AA / account aggregator license and has in principle approved 5 of them. #DataEconomy #DataSharing #Consent #SaveOurPrivacy #SuperRegulatorySystempic.twitter.com/fBripoA7uU",
                        "link": "https://twitter.com/logic/status/1059746679945981952",
                        "body": "#CashlessConsumer #RTI @RBI has received 9 applications for NBFC-AA / account aggregator license and has in principle approved 5 of them. #DataEconomy #DataSharing #Consent #SaveOurPrivacy #SuperRegulatorySystempic.twitter.com/fBripoA7uU",
                        "pubDate": "Tue, 06 Nov 2018 09:57:52 GMT",
                        "permaLink": "https://twitter.com/logic/status/1059746679945981952",
                        "id": "0000489"
                    },
                    {
                        "title": "#CashlessConsumer #RTI @RBI has received 9 applications for NBFC-AA / account aggregator license and has in principle approved 5 of them. #DataEconomy #DataSharing #Consent #SaveOurPrivacy #SuperRegulatorySystempic.twitter.com/C7BebjQz2J",
                        "link": "https://twitter.com/logic/status/1059735308147679232",
                        "body": "#CashlessConsumer #RTI @RBI has received 9 applications for NBFC-AA / account aggregator license and has in principle approved 5 of them. #DataEconomy #DataSharing #Consent #SaveOurPrivacy #SuperRegulatorySystempic.twitter.com/C7BebjQz2J",
                        "pubDate": "Tue, 06 Nov 2018 09:12:41 GMT",
                        "permaLink": "https://twitter.com/logic/status/1059735308147679232",
                        "id": "0000471"
                    },
                    {
                        "title": ". @NPCI_NPCI @BharatBillPay responds complaint of #CashlessConsumer on user consent. 2 months time to make changes to apps to make consent explicit.\n\nFinancial players must take user consent to access utility bills data: NPCI  http://toi.in/L0wA-b/a24gj  via @timesofindia",
                        "link": "https://twitter.com/logic/status/1057858058871701504",
                        "body": ". @NPCI_NPCI @BharatBillPay responds complaint of #CashlessConsumer on user consent. 2 months time to make changes to apps to make consent explicit.\n\nFinancial players must take user consent to access utility bills data: NPCI  http://toi.in/L0wA-b/a24gj&nbsp; via @timesofindia",
                        "pubDate": "Thu, 01 Nov 2018 04:53:09 GMT",
                        "permaLink": "https://twitter.com/logic/status/1057858058871701504",
                        "id": "0000443"
                    },
                    {
                        "title": "#CashlessConsumer #RTI We asked @RBI for statistics on its awareness campaign through SMS, Missed Call, Email -- RBI Kehta Hai / RBISAY --  https://medium.com/cashlessconsumer/public-awareness-initiative-by-rbi-92be359b80b5 … Some interesting numbers here.pic.twitter.com/iCRvTCEV2P",
                        "link": "https://twitter.com/logic/status/1057957809806102528",
                        "body": "#CashlessConsumer #RTI We asked @RBI for statistics on its awareness campaign through SMS, Missed Call, Email -- RBI Kehta Hai / RBISAY --  https://medium.com/cashlessconsumer/public-awareness-initiative-by-rbi-92be359b80b5&nbsp;&hellip; Some interesting numbers ...",
                        "pubDate": "Thu, 01 Nov 2018 11:29:32 GMT",
                        "permaLink": "https://twitter.com/logic/status/1057957809806102528",
                        "id": "0000442"
                    },
                    {
                        "title": "The escalating of tension come after battles.\n\nProtectionist vs Free market forces\n\nRBI-NPCI-iSpirt ---- US Giants\n\n1. Payments data localisation (local only vs at least mirroring)\n2. Payments regulation (RBI centered vs independent)\n\nFor #CashlessConsumer there is no one side.",
                        "link": "https://twitter.com/logic/status/1058178910418923520",
                        "body": "The escalating of tension come after battles.\n\nProtectionist vs Free market forces\n\nRBI-NPCI-iSpirt ---- US Giants\n\n1. Payments data localisation (local only vs at least mirroring)\n2. Payments regulation (RBI centered vs independent)\n\nFor #CashlessConsumer there is no one side.",
                        "pubDate": "Fri, 02 Nov 2018 02:08:06 GMT",
                        "permaLink": "https://twitter.com/logic/status/1058178910418923520",
                        "id": "0000441"
                    },
                    {
                        "title": "++ #CashlessConsumer wants answerable regulator. \n\nPreliminary thoughts.  https://twitter.com/logic/status/1056379063223279617 …\n\nWe will put a consumer vision on regulatory governance, #PSS2018, spat between RBI and Government in context of payments (We believe PSS2018 is used as a additional weapon in battle)",
                        "link": "https://twitter.com/logic/status/1057562613121515521",
                        "body": "++ #CashlessConsumer wants answerable regulator. \n\nPreliminary thoughts.  https://twitter.com/logic/status/1056379063223279617&nbsp;&hellip;\n\nWe will put a consumer vision on regulatory governance, #PSS2018, spat between RBI and Government in context of payments (We believe ...",
                        "pubDate": "Wed, 31 Oct 2018 09:19:10 GMT",
                        "permaLink": "https://twitter.com/logic/status/1057562613121515521",
                        "id": "0000402"
                    },
                    {
                        "title": "#CashlessConsumer had some comments on @indicusfound policy brief - TOWARDS A MATURE DIGITAL PAYMENTS MARKET FOR INDIA\n\nOur comments  https://via.hypothes.is/https://us11.campaign-archive.com/?u=b1dc50f1953ee62e477bfb335&id=6e192ff342#annotations:query …: cc @sumitakale \n\n(Yes, @hypothes_is looks more exciting for #Policy commentary and we would love to use more)",
                        "link": "https://twitter.com/logic/status/1056857688729178112",
                        "body": "#CashlessConsumer had some comments on @indicusfound policy brief - TOWARDS A MATURE DIGITAL PAYMENTS MARKET FOR INDIA\n\nOur comments  https://via.hypothes.is/https://us11.campaign-archive.com/?u=b1dc50f1953ee62e477bfb335&amp;id=6e192ff342#annotations:query&nbsp;&hellip;: cc ...",
                        "pubDate": "Mon, 29 Oct 2018 10:38:03 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056857688729178112",
                        "id": "0000350"
                    },
                    {
                        "title": "https://cashlessconsumer-river5.glitch.me/  Payments River of Feed is now in beta. \n\nTrack everything related to #India #paytech - @PCIUpdates member blogs, Media feeds, notifications from @RBI @ReserveBankIT , blogposts from policy tanks @indicusfound @dvaratrust, #CashlessConsumer tweets & more.",
                        "link": "https://twitter.com/logic/status/1056439422483292160",
                        "body": "https://cashlessconsumer-river5.glitch.me/&nbsp; Payments River of Feed is now in beta. \n\nTrack everything related to #India #paytech - @PCIUpdates member blogs, Media feeds, notifications from @RBI @ReserveBankIT , blogposts from policy tanks @indicusfound @dvaratrust, ...",
                        "pubDate": "Sun, 28 Oct 2018 06:56:00 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056439422483292160",
                        "id": "0000344"
                    },
                    {
                        "title": "Thank you. More coming up on #PSS2018 exclusively. Also a line by line response to much publicised dissent on it and partial to response to speech by Dy Gov wrt payments. #CashlessConsumer",
                        "link": "https://twitter.com/logic/status/1056254799925043201",
                        "body": "Thank you. More coming up on #PSS2018 exclusively. Also a line by line response to much publicised dissent on it and partial to response to speech by Dy Gov wrt payments. #CashlessConsumer",
                        "pubDate": "Sat, 27 Oct 2018 18:42:23 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056254799925043201",
                        "id": "0000339"
                    },
                    {
                        "title": "#CashlessConsumer representation made to @MORTHIndia to review clause 16(1) of Motor Vehicle (Tourist Permit) Rules, 2018 mandating #FASTag on all tourist vehicles. \n\nFull Letter \n https://docs.google.com/document/d/e/2PACX-1vSQPlduk5v809FNylFZmWiAtI5gowYi4qZZV_OEeHhHJTQFnlLwgn6SMS8CuefTg7YGHUi919z3L-6D/pub …",
                        "link": "https://twitter.com/logic/status/1056212002568204288",
                        "body": "#CashlessConsumer representation made to @MORTHIndia to review clause 16(1) of Motor Vehicle (Tourist Permit) Rules, 2018 mandating #FASTag on all tourist vehicles. \n\nFull Letter \n ...",
                        "pubDate": "Sat, 27 Oct 2018 15:52:19 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056212002568204288",
                        "id": "0000338"
                    },
                    {
                        "title": "Reply to emails or back off @RBI. We need a functional regulator for payments. Not sleeping one. #CashlessConsumer.  https://twitter.com/dugalira/status/1055844415770673152 …",
                        "link": "https://twitter.com/logic/status/1056216901007757312",
                        "body": "Reply to emails or back off @RBI. We need a functional regulator for payments. Not sleeping one. #CashlessConsumer.  https://twitter.com/dugalira/status/1055844415770673152&nbsp;&hellip;",
                        "pubDate": "Sat, 27 Oct 2018 16:11:47 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056216901007757312",
                        "id": "0000337"
                    },
                    {
                        "title": "#CashlessConsumer #RTIDesk Payments Transparency — Regulatory Penalties, National Security exceptions https://medium.com/cashlessconsumer/payments-transparency-regulatory-penalties-national-security-exceptions-aa2e647806b …",
                        "link": "https://twitter.com/logic/status/1056071598447312896",
                        "body": "#CashlessConsumer #RTIDesk Payments Transparency&#x200A;&mdash;&#x200A;Regulatory Penalties, National Security exceptions https://medium.com/cashlessconsumer/payments-transparency-regulatory-penalties-national-security-exceptions-aa2e647806b&nbsp;&hellip;",
                        "pubDate": "Sat, 27 Oct 2018 06:34:24 GMT",
                        "permaLink": "https://twitter.com/logic/status/1056071598447312896",
                        "id": "0000336"
                    },
                    {
                        "title": "#CashlessConsumer has data and privacy in its mandate. Privacy can't be traded for interoperability. Build privacy features, have data minimization, hear consumers out and involve them in design. Interoperability through centralization poses greater risk to everyone.",
                        "link": "https://twitter.com/logic/status/1055789180167565312",
                        "body": "#CashlessConsumer has data and privacy in its mandate. Privacy can't be traded for interoperability. Build privacy features, have data minimization, hear consumers out and involve them in design. Interoperability through centralization poses greater risk to everyone.",
                        "pubDate": "Fri, 26 Oct 2018 11:52:10 GMT",
                        "permaLink": "https://twitter.com/logic/status/1055789180167565312",
                        "id": "0000308"
                    },
                    {
                        "title": "Multiple fault lines on ease of use, speed, privacy, cost make payments in daily transit untenable as it stands. There need to be  a clear user charter for transitpaytech #PayTech. #CashlessConsumer would love to document and put a study. :)",
                        "link": "https://twitter.com/logic/status/1055790852272996352",
                        "body": "Multiple fault lines on ease of use, speed, privacy, cost make payments in daily transit untenable as it stands. There need to be  a clear user charter for transitpaytech #PayTech. #CashlessConsumer would love to document and put a study. :)",
                        "pubDate": "Fri, 26 Oct 2018 11:58:49 GMT",
                        "permaLink": "https://twitter.com/logic/status/1055790852272996352",
                        "id": "0000307"
                    },
                    {
                        "title": "#CashlessConsumer - Stories from daily mailers in inbox. #meta via @ETPrime_compic.twitter.com/I2D7EmWUgQ",
                        "link": "https://twitter.com/logic/status/1055325895655866368",
                        "body": "#CashlessConsumer - Stories from daily mailers in inbox. #meta via @ETPrime_compic.twitter.com/I2D7EmWUgQ",
                        "pubDate": "Thu, 25 Oct 2018 05:11:15 GMT",
                        "permaLink": "https://twitter.com/logic/status/1055325895655866368",
                        "id": "0000291"
                    },
                    {
                        "title": "#CashlessConsumer #RTIDesk - We asked Financial Intelligence unit some questions on #DataLocalisation, #Aadhaar and Regulatory penalties to #PPI and were responded with blanket Sec 24(1) that excludes the organisation under the purview of #RTI.pic.twitter.com/gDsoqaRadB",
                        "link": "https://twitter.com/logic/status/1055278705952477184",
                        "body": "#CashlessConsumer #RTIDesk - We asked Financial Intelligence unit some questions on #DataLocalisation, #Aadhaar and Regulatory penalties to #PPI and were responded with blanket Sec 24(1) that excludes the organisation under the purview of #RTI.pic.twitter.com/gDsoqaRadB",
                        "pubDate": "Thu, 25 Oct 2018 02:03:44 GMT",
                        "permaLink": "https://twitter.com/logic/status/1055278705952477184",
                        "id": "0000282"
                    },
                    {
                        "title": "https://www.cashlessconsumer.in/post/rti-002/  #CashlessConsumer #RTIDesk\n\nThere is urgent need for #SurveillanceReform, particularly on transparency of functioning of the agencies. The FIU is special in #DataEconomy as it has its own data platform, compliance API and amasses large chunks of data.",
                        "link": "https://twitter.com/logic/status/1055281068142514176",
                        "body": "https://www.cashlessconsumer.in/post/rti-002/&nbsp; #CashlessConsumer #RTIDesk\n\nThere is urgent need for #SurveillanceReform, particularly on transparency of functioning of the agencies. The FIU is special in #DataEconomy as it has its own data platform, compliance API and ...",
                        "pubDate": "Thu, 25 Oct 2018 02:13:07 GMT",
                        "permaLink": "https://twitter.com/logic/status/1055281068142514176",
                        "id": "0000281"
                    },
                    {
                        "title": "#CashlessConsumer #Data now has a #RTI desk.  https://www.cashlessconsumer.in/post/rti/  We have filed 15+ RTIs related to payments and hope to update them as we get responses",
                        "link": "https://twitter.com/logic/status/1054930479722438656",
                        "body": "#CashlessConsumer #Data now has a #RTI desk.  https://www.cashlessconsumer.in/post/rti/&nbsp; We have filed 15+ RTIs related to payments and hope to update them as we get responses",
                        "pubDate": "Wed, 24 Oct 2018 03:00:00 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054930479722438656",
                        "id": "0000278"
                    },
                    {
                        "title": "//On privacy. On the focus being consumer rights or fundamental rights// Small side note - I met a portion of IFF on the sidelines of  https://twitter.com/logic/status/832832090685075457 …. I made a brief call for support for #CashlessConsumer, while still having my own reservations with affiliations & orgs.",
                        "link": "https://twitter.com/logic/status/1054975143750057985",
                        "body": "//On privacy. On the focus being consumer rights or fundamental rights// Small side note - I met a portion of IFF on the sidelines of  https://twitter.com/logic/status/832832090685075457&nbsp;&hellip;. I made a brief call for support for #CashlessConsumer, while still having my ...",
                        "pubDate": "Wed, 24 Oct 2018 05:57:29 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054975143750057985",
                        "id": "0000277"
                    },
                    {
                        "title": "#CashlessConsumer has as big a problem with #privacy, as #Aadhaar. Hence it was only natural to seek allies from IFF. I still interact with many, across the split of IFF, outside of it.I also get it completely why organizations must limit its mandate in proportion with bandwidth",
                        "link": "https://twitter.com/logic/status/1054975146983940102",
                        "body": "#CashlessConsumer has as big a problem with #privacy, as #Aadhaar. Hence it was only natural to seek allies from IFF. I still interact with many, across the split of IFF, outside of it.I also get it completely why organizations must limit its mandate in proportion with bandwidth",
                        "pubDate": "Wed, 24 Oct 2018 05:57:30 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054975146983940102",
                        "id": "0000276"
                    },
                    {
                        "title": "It is these problems and experiences, that has made me approach #CashlessConsumer - as a collective - One that does not want to get into murky waters of organizational structures. I know it has high chances of failing / not scaling up. But that is a choice. disagree? Fork it!",
                        "link": "https://twitter.com/logic/status/1054977690753740800",
                        "body": "It is these problems and experiences, that has made me approach #CashlessConsumer - as a collective - One that does not want to get into murky waters of organizational structures. I know it has high chances of failing / not scaling up. But that is a choice. disagree? Fork it!",
                        "pubDate": "Wed, 24 Oct 2018 06:07:36 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054977690753740800",
                        "id": "0000275"
                    },
                    {
                        "title": "As #CashlessConsumer I have also volunteered with @50pConf (writing articles, helping in editorial of #50p) though barely interacted with @jackerhack . Its mostly (99%) @zainabbawa and @booleanbalaji",
                        "link": "https://twitter.com/logic/status/1054979045505597441",
                        "body": "As #CashlessConsumer I have also volunteered with @50pConf (writing articles, helping in editorial of #50p) though barely interacted with @jackerhack . Its mostly (99%) @zainabbawa and @booleanbalaji",
                        "pubDate": "Wed, 24 Oct 2018 06:12:59 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054979045505597441",
                        "id": "0000274"
                    },
                    {
                        "title": "#CashlessConsumer We also help you file #RTI and if you want to remain anonymous, you can make a request and one of us will review and file. We encourage everyone to file RTIs, share responses  https://www.cashlessconsumer.in/post/filerti/ \n\nVolunteers for this activity also welcome :)",
                        "link": "https://twitter.com/logic/status/1054995235095351297",
                        "body": "#CashlessConsumer We also help you file #RTI and if you want to remain anonymous, you can make a request and one of us will review and file. We encourage everyone to file RTIs, share responses  https://www.cashlessconsumer.in/post/filerti/&nbsp;\n\nVolunteers for this activity ...",
                        "pubDate": "Wed, 24 Oct 2018 07:17:19 GMT",
                        "permaLink": "https://twitter.com/logic/status/1054995235095351297",
                        "id": "0000273"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "CCTwitter.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.001,
        "ctBuilds": 35,
        "ctDuplicatesSkipped": 11,
        "whenGMT": "Tue, 27 Nov 2018 01:04:00 GMT",
        "whenLocal": "11/27/2018, 1:04:00 AM",
        "aggregator": "River5 v0.4.14"
    }
})