onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "MobiKwik",
                "feedUrl": "https://blog.mobikwik.com/index.php/feed/",
                "websiteUrl": "https://blog.mobikwik.com/",
                "feedDescription": "#MobiKwikHaiNa",
                "whenLastUpdate": "Tue, 27 Nov 2018 05:15:13 GMT",
                "item": [
                    {
                        "title": "End the year on an artistic note – 7 must visit festivals in India in December!",
                        "link": "https://blog.mobikwik.com/index.php/7-must-visit-festivals-in-india-in-december/",
                        "body": "When it comes to December, there is this strong urge to plan a visit to a place that is celebrating a festival or has organized a fair, you know to get into that holiday season mood. And I guess, India is one of those places where you can actually make the most of this month ...",
                        "pubDate": "Tue, 27 Nov 2018 05:11:26 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/7-must-visit-festivals-in-india-in-december/#respond",
                        "id": "0000693"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Mon, 26 Nov 2018 14:44:53 GMT",
                "item": [
                    {
                        "title": "How Co-working Spaces Operate in India (INFOGRAPHIC)",
                        "link": "https://www.instamojo.com/blog/how-co-working-spaces-operate-in-india-infographic/",
                        "body": "Let&#8217;s talk office! As a new business or a startup, do you really need to invest in a dedicated office space? The debate is real (estate) but have you considered co-working spaces as an option? Here&#8217;s an infographic that will answer your questions on whether you ...",
                        "pubDate": "Mon, 26 Nov 2018 14:32:19 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/how-co-working-spaces-operate-in-india-infographic/#respond",
                        "id": "0000688"
                    }
                ]
            },
            {
                "feedTitle": "Zeta Blog - Medium",
                "feedUrl": "https://blog.zeta.in/feed",
                "websiteUrl": "https://blog.zeta.in/?source=rss----4c4ed56774e2---4",
                "feedDescription": "Zeta is making payments digital, fast, inclusive and safe. Keep reading to know more. - Medium",
                "whenLastUpdate": "Mon, 26 Nov 2018 07:06:50 GMT",
                "item": [
                    {
                        "title": "Zeta Vlogs Ep #4: The in-hand salary paradox",
                        "link": "https://blog.zeta.in/zeta-vlogs-ep-4-the-in-hand-salary-paradox-3665910f8977?source=rss----4c4ed56774e2---4",
                        "body": "Owing to a major misconception, employees shy away from tax-saving reimbursements. We call this the in-hand paradox. In this edition of Zeta Vlogs, we explain this paradox and talk about how HR managers can address it among their employees.You can also read more about the ...",
                        "pubDate": "Mon, 26 Nov 2018 05:50:13 GMT",
                        "permaLink": "",
                        "id": "0000679"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Thu, 22 Nov 2018 14:47:11 GMT",
                "item": [
                    {
                        "title": "Add-On Tools to Up your Email Marketing Game",
                        "link": "https://www.instamojo.com/blog/email-marketing-tools-add-on-mailchimp/",
                        "body": "We&#8217;ve already established how important email is. But every so often, you may hit a roadblock with your audience. You may run into problems with scale. Want to pump up your email marketing efforts to get more leads and skyrocket your business? Here is a list of tools that ...",
                        "pubDate": "Thu, 22 Nov 2018 14:37:59 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/email-marketing-tools-add-on-mailchimp/#respond",
                        "id": "0000665"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Wed, 21 Nov 2018 09:09:42 GMT",
                "item": [
                    {
                        "title": "We are now India’s largest online insurance premium payments platform",
                        "link": "https://blog.paytm.com/we-are-now-indias-largest-online-insurance-premium-payments-platform-24e9b6475ea8?source=rss----4d9006e6d2c6---4",
                        "body": "India’s largest insurer LIC is also live on Paytm!We are excited to announce that you can now make insurance premium payments for more than 30 top insurers on Paytm, including the country’s largest insurance firm — Life Insurance Corporation of India (LIC). This will help ...",
                        "pubDate": "Wed, 21 Nov 2018 08:59:15 GMT",
                        "permaLink": "",
                        "id": "0000643"
                    }
                ]
            },
            {
                "feedTitle": "The Freecharge Blog",
                "feedUrl": "https://blog.freecharge.in/feed/",
                "websiteUrl": "https://blog.freecharge.in/",
                "feedDescription": "#StayCharged",
                "whenLastUpdate": "Tue, 20 Nov 2018 12:27:08 GMT",
                "item": [
                    {
                        "title": "5 Key Emerging Trends in the Indian Digital Payments Industry",
                        "link": "https://blog.freecharge.in/5-key-emerging-trends-in-the-indian-digital-payments-industry/",
                        "body": "Article courtesy:\nAbhishek Gupta &#8211; Senior Director, Product  &amp; Payments at FreeCharge\nAt the recently held Financial Services Roundtable at Bengaluru on “Banking Transformation, Payment Convergence &amp; Financial Crime Management”, Abhishek Gupta, Senior Director ...",
                        "pubDate": "Tue, 20 Nov 2018 12:09:31 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/5-key-emerging-trends-in-the-indian-digital-payments-industry/#respond",
                        "id": "0000636"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Mon, 19 Nov 2018 13:22:07 GMT",
                "item": [
                    {
                        "title": "Registering a Private Limited Company in India (2018 Update)",
                        "link": "https://www.instamojo.com/blog/registering-a-private-limited-company-in-india-2018-update/",
                        "body": "A Private Limited Company registration in India is customary for starting a new business. This structure is developed under the organized business sector governed by specific Act and provisions. \nA company in India is incorporated and registered under the Indian Companies Act, ...",
                        "pubDate": "Mon, 19 Nov 2018 13:21:06 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/registering-a-private-limited-company-in-india-2018-update/#respond",
                        "id": "0000617"
                    }
                ]
            },
            {
                "feedTitle": "Razorpay Blog",
                "feedUrl": "https://rzpwp.blog/feed/",
                "websiteUrl": "https://rzpwp.blog/",
                "feedDescription": "",
                "whenLastUpdate": "Mon, 19 Nov 2018 08:15:35 GMT",
                "item": [
                    {
                        "title": "Razorpay 2.0 – A Year In Numbers",
                        "link": "https://rzpwp.blog/2018/11/19/infographic-razorpay-a-year-in-numbers/",
                        "body": "A little over a year ago, we launched our suite of converged payment solutions &#8211; what we lovingly called the &#8216;Razorpay 2.0&#8216; solution. The products came into being because of a single insight &#8211; that online payments are a key issue in e-commerce. And that ...",
                        "pubDate": "Mon, 19 Nov 2018 08:08:33 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/11/19/infographic-razorpay-a-year-in-numbers/#respond",
                        "id": "0000610"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Sun, 18 Nov 2018 14:26:04 GMT",
                "item": [
                    {
                        "title": "Happy Birthday Pratheek!",
                        "link": "https://blog.paytm.com/happy-birthday-pratheek-2dddddae8651?source=rss----4d9006e6d2c6---4",
                        "body": "Wish you lots of happiness &amp; joy from all of us @PaytmOver the last few years, Paytm has grown rapidly, expanding its services &amp; offerings across the country.But it is our valued Customers &amp; Merchant partners that have been our biggest ambassadors &amp; the real ...",
                        "pubDate": "Sun, 18 Nov 2018 14:11:12 GMT",
                        "permaLink": "",
                        "id": "0000601"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Thu, 15 Nov 2018 03:50:16 GMT",
                "item": [
                    {
                        "title": "Taking the Pain Out of Payments",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/taking-the-pain-out-of-payments/",
                        "body": "Strategic partnership between Mastercard and MYOB drives new wave in fintech innovation\nSydney, 15 November 2018 – MYOB and Mastercard today announced a strategic partnership to bring innovative new ways of managing cashflow to hundreds of thousands of small business owners ...",
                        "pubDate": "Thu, 15 Nov 2018 03:27:25 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/taking-the-pain-out-of-payments/#comments",
                        "id": "0000562"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Wed, 14 Nov 2018 14:10:38 GMT",
                "item": [
                    {
                        "title": "The Art of Writing Effective Meta Descriptions to Increase Traffic",
                        "link": "https://www.instamojo.com/blog/meta-descriptions-increasing-traffic/",
                        "body": "Are you an online business trying to understand why your store isn&#8217;t getting the traffic it deserves? Meta descriptions may just be your problem.\nYou may have a flawless product, a good customer base, and a good marketing plan. But, what matters is whether it is reaching ...",
                        "pubDate": "Wed, 14 Nov 2018 14:09:47 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/meta-descriptions-increasing-traffic/#respond",
                        "id": "0000559"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Tue, 13 Nov 2018 14:11:08 GMT",
                "item": [
                    {
                        "title": "Mastercard and RBL Bank Join Hands to Expand the Digital Payments Acceptance Network in India",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-rbl-bank-join-hands-to-expand-the-digital-payments-acceptance-network-in-india/",
                        "body": "Mastercard and RBL Bank add more than 500,000 merchant acceptance locations since demonetization, fastest in the industry\nNew Delhi, 13 November 2018 &#8211; RBL Bank today announced that through strategic support from Mastercard, the bank has added over 500,000 merchant ...",
                        "pubDate": "Tue, 13 Nov 2018 07:00:58 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-rbl-bank-join-hands-to-expand-the-digital-payments-acceptance-network-in-india/#comments",
                        "id": "0000537"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Mon, 12 Nov 2018 14:25:47 GMT",
                "item": [
                    {
                        "title": "5 Things You Need to Know about India’s RuPay Cards",
                        "link": "https://www.instamojo.com/blog/indias-rupay-biggest-market-player-facts/",
                        "body": "Mastercard and VISA cards, move over! Made in India &#8211; RuPay &#8211; is here to take the digital payments sector by a storm. According to the National Payments Corporation of India, RuPay cards and UPI have together gathered 60% of the market share of digital transactions ...",
                        "pubDate": "Mon, 12 Nov 2018 14:24:47 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/indias-rupay-biggest-market-player-facts/#respond",
                        "id": "0000524"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Mon, 12 Nov 2018 09:40:52 GMT",
                "item": [
                    {
                        "title": "Mastercard opens Global Tech Hub in Sydney",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-opens-global-tech-hub-in-sydney/",
                        "body": "Tech Hub set to accelerate global innovation\nSydney, 12 November 2018 – Mastercard today announced the opening of the Australian Mastercard Global Tech Hub in Sydney. The Hub is the first in the southern hemisphere, with Sydney selected for its advanced marketplace and strong ...",
                        "pubDate": "Sun, 11 Nov 2018 23:15:25 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-opens-global-tech-hub-in-sydney/#comments",
                        "id": "0000518"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Fri, 09 Nov 2018 13:22:52 GMT",
                "item": [
                    {
                        "title": "Instagram Marketing: Decoding the Instagram Algorithm",
                        "link": "https://www.instamojo.com/blog/instagram-marketing-decoding-algorithm-tips/",
                        "body": "Are you looking to decode the secret to Instagram marketing?\nThere are no second thoughts about how important social media marketing is for your business. Every platform works on a different algorithm that ensures popularity and virality of posts. With Instagram, a common myth ...",
                        "pubDate": "Fri, 09 Nov 2018 13:03:55 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/instagram-marketing-decoding-algorithm-tips/#respond",
                        "id": "0000509"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Wed, 07 Nov 2018 13:58:26 GMT",
                "item": [
                    {
                        "title": "Inbound Tourism Fuels Future-Ready Cities Across Southeast Asia",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/inbound-tourism-fuels-future-ready-cities-across-southeast-asia/",
                        "body": "Singapore, 5 November – Mastercard, a leader in financial technology, today unveiled a report that demonstrates the transformative power of tourism in fueling the development of smarter cities and accelerating economic growth.\n“The ASEAN Opportunity: How City Planners and ...",
                        "pubDate": "Mon, 05 Nov 2018 03:00:34 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/inbound-tourism-fuels-future-ready-cities-across-southeast-asia/#comments",
                        "id": "0000480"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Tue, 06 Nov 2018 13:37:28 GMT",
                "item": [
                    {
                        "title": "Have You Checked Out Your Diwali Business Horoscope Yet?",
                        "link": "https://www.instamojo.com/blog/have-you-checked-out-your-diwali-business-horoscope-yet/",
                        "body": "First of all, a very Happy Diwali to you and your business!\nIf you&#8217;re still working, you should just shut down, go home and spend time with family. But if you&#8217;re looking at this post, there&#8217;s no gyaan today. Just a lot of positivity and fun.\nHere&#8217;s Your ...",
                        "pubDate": "Tue, 06 Nov 2018 13:34:23 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/have-you-checked-out-your-diwali-business-horoscope-yet/#respond",
                        "id": "0000479"
                    },
                    {
                        "title": "Business Loans: How to Get Up to Rs 1 crore in 59 minutes!",
                        "link": "https://www.instamojo.com/blog/msme-business-loans-rs-1-crore-loan-59-minutes/",
                        "body": "This Diwali, MSME business loans just got easier!\nAs a business, getting loans to fast-track your venture can be a challenging task. Truckloads of paperwork, multiple visits to banks, and long queues are common and also tedious. But now the government is working towards helping ...",
                        "pubDate": "Mon, 05 Nov 2018 13:51:28 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/msme-business-loans-rs-1-crore-loan-59-minutes/#respond",
                        "id": "0000460"
                    }
                ]
            },
            {
                "feedTitle": "MobiKwik",
                "feedUrl": "https://blog.mobikwik.com/index.php/feed/",
                "websiteUrl": "https://blog.mobikwik.com/",
                "feedDescription": "#MobiKwikHaiNa",
                "whenLastUpdate": "Mon, 05 Nov 2018 11:23:20 GMT",
                "item": [
                    {
                        "title": "MobiKwik is listed as one of the most promising fintech start-up globally",
                        "link": "https://blog.mobikwik.com/index.php/mobikwik-is-listed-as-one-of-the-most-promising-fintech-start-up-globally/",
                        "body": "We are delighted to be a part of The Fintech 250 for the second consecutive year. The Fintech 250 is a yearly report on top fintech start-ups globally, based on product innovations and technological advancements that transformed the financial services industry.\nIn the 9 years ...",
                        "pubDate": "Mon, 05 Nov 2018 11:13:59 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/mobikwik-is-listed-as-one-of-the-most-promising-fintech-start-up-globally/#respond",
                        "id": "0000455"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Fri, 02 Nov 2018 06:05:59 GMT",
                "item": [
                    {
                        "title": "We are the largest contributor to UPI Payments",
                        "link": "https://blog.paytm.com/we-are-the-largest-contributor-to-upi-payments-4ad00266edcf?source=rss----4d9006e6d2c6---4",
                        "body": "Paytm BHIM UPI is fast becoming the preferred mode of payment among our usersWe are excited to announce that we are currently the leading contributor to UPI payments in the country, with over 179 Million UPI transactions in October’18, and the highest concentration of ...",
                        "pubDate": "Fri, 02 Nov 2018 05:51:22 GMT",
                        "permaLink": "",
                        "id": "0000425"
                    }
                ]
            },
            {
                "feedTitle": "truebalance - Medium",
                "feedUrl": "https://medium.com/feed/truebalance",
                "websiteUrl": "https://medium.com/truebalance?source=rss----1d74a0c225fb---4",
                "feedDescription": "True Balance is a mobile app for people in India to easily check and recharge their mobile balance. With one tap balance check and quick recharges it is one of the leading mobile recharge app in India. - Medium",
                "whenLastUpdate": "Thu, 01 Nov 2018 13:38:11 GMT",
                "item": [
                    {
                        "title": "What’s New with Recharge Membership in November?",
                        "link": "https://medium.com/truebalance/whats-new-with-recharge-membership-in-november-141fa49eded7?source=rss----1d74a0c225fb---4",
                        "body": "Gold Members, do more Recharges and Bill Payments this month as now you will get higher Cashback on transactions upto Rs. 85,000. Also, enjoy different Cashback percentage on all our services, including Gift Cards also.Let’s find out, what’s new in Recharge Membership ...",
                        "pubDate": "Thu, 01 Nov 2018 13:21:36 GMT",
                        "permaLink": "",
                        "id": "0000419"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Wed, 31 Oct 2018 13:46:08 GMT",
                "item": [
                    {
                        "title": "9 Customer Acquisition Hacks for Businesses with Zero Sales",
                        "link": "https://www.instamojo.com/blog/9-customer-acquisition-hacks-businesses-with-zero-sales/",
                        "body": "What if we told you, you can get your first customer without any proven record of sales? Customer acquisition can be a challenging task to overcome as a newbie entrepreneur.\nWhile most acquisition strategies focus on businesses which already have a customer base, starting off ...",
                        "pubDate": "Wed, 31 Oct 2018 13:39:42 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/9-customer-acquisition-hacks-businesses-with-zero-sales/#respond",
                        "id": "0000403"
                    }
                ]
            },
            {
                "feedTitle": "Eko India Financial Services",
                "feedUrl": "http://eko.co.in/feed/",
                "websiteUrl": "https://eko.co.in/",
                "feedDescription": "",
                "whenLastUpdate": "Wed, 31 Oct 2018 04:13:15 GMT",
                "item": [
                    {
                        "title": "hey",
                        "link": "https://eko.co.in/hey/",
                        "body": "The post hey appeared first on Eko India Financial Services.",
                        "pubDate": "Wed, 31 Oct 2018 04:12:08 GMT",
                        "permaLink": "",
                        "comments": "https://eko.co.in/hey/#respond",
                        "id": "0000389"
                    }
                ]
            },
            {
                "feedTitle": "Zeta Blog - Medium",
                "feedUrl": "https://blog.zeta.in/feed",
                "websiteUrl": "https://blog.zeta.in/?source=rss----4c4ed56774e2---4",
                "feedDescription": "Zeta is making payments digital, fast, inclusive and safe. Keep reading to know more. - Medium",
                "whenLastUpdate": "Tue, 30 Oct 2018 15:18:24 GMT",
                "item": [
                    {
                        "title": "Need for digital ecosystems at educational institutes",
                        "link": "https://blog.zeta.in/need-for-digital-ecosystems-at-educational-institutes-e031b7060af5?source=rss----4c4ed56774e2---4",
                        "body": "By Bhavin TurakhiaIt has taken us some time to warm up to the idea of digital currencies. The initial lack of faith in the concept has given way to valuing the ease of one-tap payments, not being dependent on ATMs and not having to carry cash around. Although adoption is not ...",
                        "pubDate": "Tue, 30 Oct 2018 12:21:43 GMT",
                        "permaLink": "",
                        "id": "0000383"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Tue, 30 Oct 2018 15:18:20 GMT",
                "item": [
                    {
                        "title": "The Most Useful Way to add UPI Payments to your App/Website",
                        "link": "https://www.instamojo.com/blog/add-upi-payments-app-website-online/",
                        "body": "Are you looking to add the UPI payment option for your customers/users, in your mobile app or website?\nUPI(Unified Payment Interface) is a new digital payment option in India. It’s one of the payment options that allow you to pay anyone &#8211; online stores, your friends &amp; ...",
                        "pubDate": "Tue, 30 Oct 2018 13:54:54 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/add-upi-payments-app-website-online/#respond",
                        "id": "0000382"
                    }
                ]
            },
            {
                "feedTitle": "Zeta Blog - Medium",
                "feedUrl": "https://blog.zeta.in/feed",
                "websiteUrl": "https://blog.zeta.in/?source=rss----4c4ed56774e2---4",
                "feedDescription": "Zeta is making payments digital, fast, inclusive and safe. Keep reading to know more. - Medium",
                "whenLastUpdate": "Mon, 29 Oct 2018 17:05:44 GMT",
                "item": [
                    {
                        "title": "A cup and a conversation. Cafes across India",
                        "link": "https://blog.zeta.in/a-cup-and-a-conversation-cafes-across-india-6a796d1e061c?source=rss----4c4ed56774e2---4",
                        "body": "A warm cup of coffee or tea is to us, what fuel is to a car. It keeps us running, no matter what the occasion. Let Sodexo and Zeta help you get fueled up with our pick of the best cafes across the country.Use the merchant directory built into the Zeta app to find our picks, and ...",
                        "pubDate": "Wed, 18 Jul 2018 05:15:41 GMT",
                        "permaLink": "",
                        "id": "0000368"
                    },
                    {
                        "title": "Why employees shy away from tax benefits: Zeta Vlogs explores",
                        "link": "https://blog.zeta.in/why-employees-shy-away-from-tax-benefits-zeta-vlogs-explores-16f04d465685?source=rss----4c4ed56774e2---4",
                        "body": "According to a study on employee benefits in India, commissioned to Nielsen, 87% companies offer at least one type of employee tax benefit. However, 67% of employees preferred a higher in-hand salary over reimbursements.The reasons behind this all boil down to the way India Inc ...",
                        "pubDate": "Wed, 18 Jul 2018 05:28:10 GMT",
                        "permaLink": "",
                        "id": "0000367"
                    },
                    {
                        "title": "NASSCOM HR Summit 2018: A rewarding day for Zeta Spotlight®",
                        "link": "https://blog.zeta.in/nasscom-hr-summit-2018-a-rewarding-day-for-zeta-spotlight-a8ea4afbf330?source=rss----4c4ed56774e2---4",
                        "body": "Third time in a row, NASSCOM HR Summit 2018 has turned out to be a super successful event for Zeta and we’re happy to have received such an enthusiastic response.As always, this prestigious event gave us the opportunity to rub shoulders with top HR professionals, functional ...",
                        "pubDate": "Thu, 26 Jul 2018 08:58:17 GMT",
                        "permaLink": "",
                        "id": "0000366"
                    },
                    {
                        "title": "Breaking the in-hand salary paradox",
                        "link": "https://blog.zeta.in/breaking-the-in-hand-salary-paradox-11122c0ca62b?source=rss----4c4ed56774e2---4",
                        "body": "No matter how reimbursements are delivered and managed, the fundamental idea behind them is to decrease taxable income, and help employees save more every year.Image Courtesy Fortune IndiaBy BHAVIN TURAKHIA, Jun 7, 2018Anyone who has ever drawn a salary has, at one time or ...",
                        "pubDate": "Mon, 30 Jul 2018 11:00:58 GMT",
                        "permaLink": "",
                        "id": "0000365"
                    },
                    {
                        "title": "Zeta Vlogs Episode 2: Here’s how to retain millennials at the workplace #LikeABoss",
                        "link": "https://blog.zeta.in/zeta-vlogs-episode-2-heres-how-to-retain-millennials-at-the-workplace-likeaboss-69f07e804f5b?source=rss----4c4ed56774e2---4",
                        "body": "https://medium.com/media/43f629699470371215fdbab00a1a840b/hrefIf there’s something HR managers know about millennials it is the fact that they work for purpose, not paychecks. Despite this feat, millennials are labelled job-hoppers who are believed to be fickle and disloyal to ...",
                        "pubDate": "Wed, 01 Aug 2018 10:13:42 GMT",
                        "permaLink": "",
                        "id": "0000364"
                    },
                    {
                        "title": "How to file your income tax returns online — easily",
                        "link": "https://blog.zeta.in/how-to-file-your-income-tax-returns-online-easily-ea193f023d01?source=rss----4c4ed56774e2---4",
                        "body": "https://medium.com/media/18ed8fc2fc3385e1d4ef910f97bafbcf/hrefSo, you breathed a sigh of relief when the Income Tax Department extended the last date for filing your returns to August 31st, 2018. What next? A mad scramble to get the thing done? But if you’re like most people we ...",
                        "pubDate": "Thu, 16 Aug 2018 06:39:25 GMT",
                        "permaLink": "",
                        "id": "0000363"
                    },
                    {
                        "title": "It’s a Trifecta for Zeta",
                        "link": "https://blog.zeta.in/its-a-trifecta-for-zeta-b7923b5aaeab?source=rss----4c4ed56774e2---4",
                        "body": "2018, much like the year gone by, is brimming with awards and recognition for Zeta. We are delighted to share that we bagged three awards in a month!Payments &amp; Cards Summit 2018Best Payments App of the YearIt was double delight for us at the Payments &amp; Cards Summit 2018. ...",
                        "pubDate": "Tue, 28 Aug 2018 05:31:28 GMT",
                        "permaLink": "",
                        "id": "0000362"
                    },
                    {
                        "title": "Zeta Vlogs Ep #3: Tips to improve employee gifting",
                        "link": "https://blog.zeta.in/zeta-vlogs-ep-3-tips-to-improve-employee-gifting-f440227452fb?source=rss----4c4ed56774e2---4",
                        "body": "Gifting employees can be rather challenging for HR managers. Fortunately, we have a few tips that can help you get started on the right foot. From picking out a gift that suits employees best to things to keep in mind while picking out employee gift voucher or gift cards, here ...",
                        "pubDate": "Tue, 04 Sep 2018 12:02:26 GMT",
                        "permaLink": "",
                        "id": "0000361"
                    },
                    {
                        "title": "Burger and Cheese: An affair to remember",
                        "link": "https://blog.zeta.in/burger-and-cheese-an-affair-to-remember-9a1a9bdba141?source=rss----4c4ed56774e2---4",
                        "body": "The recipe is rather simple, in theory. Take a couple of buns, place a patty in between and round it off with some sauces and veggies. But making the best burger is more nuanced than you might think. If you find yourself asking Google to show you the ‘best burgers near me’ at ...",
                        "pubDate": "Wed, 12 Sep 2018 11:22:58 GMT",
                        "permaLink": "",
                        "id": "0000360"
                    },
                    {
                        "title": "What actually matters in employee gifting",
                        "link": "https://blog.zeta.in/what-actually-matters-in-employee-gifting-ccbcdd6a119d?source=rss----4c4ed56774e2---4",
                        "body": "A little appreciation makes a world of difference — especially at the workplace. To feel valued at work, employees need to know that their hard work is being acknowledged. That’s precisely why employee gifting and rewards and recognition are for.However, choosing the ideal gift ...",
                        "pubDate": "Fri, 14 Sep 2018 10:17:23 GMT",
                        "permaLink": "",
                        "id": "0000359"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Mon, 29 Oct 2018 17:05:40 GMT",
                "item": [
                    {
                        "title": "India is the 3rd Biggest Startup Hub in the World – NASSCOM",
                        "link": "https://www.instamojo.com/blog/india-ranks-3rd-biggest-hub-startup-bangalore/",
                        "body": "In the 4th edition of a new NASSCOM-Zinnov report titled “Indian Start-up Ecosystem 2018: Approaching Escape Velocity,&#8221; India has been ranked the 3rd biggest startup hub in the world!\n\n\nMultiple factors made India a leader of growth in the tech startup sector. Here are ...",
                        "pubDate": "Mon, 29 Oct 2018 15:15:48 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/india-ranks-3rd-biggest-hub-startup-bangalore/#respond",
                        "id": "0000358"
                    },
                    {
                        "title": "Strategic Ways of Finding your First Product Inspiration",
                        "link": "https://www.instamojo.com/blog/ways-finding-first-product-inspiration/",
                        "body": "On the look for the next big thing &#8211; your first product inspiration that can set you on track?\nBuilding a business is easy, but running a successful and sustainable business is hard. To run a sustainable business, it is essential for you as a business owner to understand ...",
                        "pubDate": "Fri, 26 Oct 2018 12:54:57 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/ways-finding-first-product-inspiration/#respond",
                        "id": "0000314"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Fri, 26 Oct 2018 01:24:50 GMT",
                "item": [
                    {
                        "title": "Mastercard Debuts Priceless Experiences for Fans at the League of Legends 2018 World Championship",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-debuts-priceless-experiences-for-fans-at-the-league-of-legends-2018-world-championship/",
                        "body": "“Mastercard Nexus” Pop-up Experience Announced; Music Performances Confirmed for the Finals Opening Ceremony Presented by Mastercard\nSeoul and Incheon, October 25, 2018 – Mastercard’s first activation of the World Championship comes to life through The Mastercard® Nexus, a ...",
                        "pubDate": "Fri, 26 Oct 2018 01:15:00 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-debuts-priceless-experiences-for-fans-at-the-league-of-legends-2018-world-championship/#comments",
                        "id": "0000297"
                    }
                ]
            },
            {
                "feedTitle": "MobiKwik",
                "feedUrl": "https://blog.mobikwik.com/index.php/feed/",
                "websiteUrl": "https://blog.mobikwik.com/",
                "feedDescription": "#MobiKwikHaiNa",
                "whenLastUpdate": "Wed, 24 Oct 2018 13:49:34 GMT",
                "item": [
                    {
                        "title": "To gift or not to gift? That’s not even a question on Karva Chauth!",
                        "link": "https://blog.mobikwik.com/index.php/to-gift-or-not-to-gift-thats-not-even-a-question-on-karwa-chauth/",
                        "body": "India could well be called the land of festivals. The amalgamation of difference cultures, languages and religions ensures that there is a festival being celebrated in some part of India almost every week. One such popular festival is Karva Chauth, which is a celebration of a ...",
                        "pubDate": "Wed, 24 Oct 2018 06:09:57 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/to-gift-or-not-to-gift-thats-not-even-a-question-on-karwa-chauth/#respond",
                        "id": "0000272"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Wed, 24 Oct 2018 13:49:32 GMT",
                "item": [
                    {
                        "title": "Download your Kerala Flood Donation Receipt from the official Kerala CMDRF Website",
                        "link": "https://blog.paytm.com/download-your-kerala-flood-donation-receipt-from-the-official-kerala-cmdrf-website-dc65de0d217c?source=rss----4d9006e6d2c6---4",
                        "body": "We have observed that some of our users may have received SMSs from Kerala CM Disaster Relief Fund (Kerala CMDRF) to download their donation receipts for contributions made on Paytm during the Kerala Floods.Due to a technical snag, the link in the SMS, in certain cases, may be ...",
                        "pubDate": "Wed, 24 Oct 2018 09:27:09 GMT",
                        "permaLink": "",
                        "id": "0000271"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Wed, 24 Oct 2018 13:49:30 GMT",
                "item": [
                    {
                        "title": "Email Marketing Hacks to Acquire and Retain Customers",
                        "link": "https://www.instamojo.com/blog/email-marketing-customer-acquisition-retention/",
                        "body": "Email marketing is not dead. While most things require your keen attention, did you know you could automate some of your work just through emails? These email marketing hacks may just be your answer.\nEmails are one of the most effective conventional ways of engaging your ...",
                        "pubDate": "Wed, 24 Oct 2018 13:12:50 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/email-marketing-customer-acquisition-retention/#respond",
                        "id": "0000270"
                    }
                ]
            },
            {
                "feedTitle": "PhonePe - Medium",
                "feedUrl": "https://blog.phonepe.com/feed",
                "websiteUrl": "https://blog.phonepe.com/?source=rss----725044d21903---4",
                "feedDescription": "India’s Payments App. Powered By UPI. - Medium",
                "whenLastUpdate": "Mon, 22 Oct 2018 17:33:32 GMT",
                "item": [
                    {
                        "title": "PhonePe sees a 150% growth in transactions during Flipkart’s Big Billion Days",
                        "link": "https://blog.phonepe.com/phonepe-sees-a-150-growth-in-transactions-during-flipkarts-big-billion-days-601af36d02d8?source=rss----725044d21903---4",
                        "body": "Big offers, massive discounts and exciting cashbacks. Flipkart’s Big Billion Days saw millions of shoppers from across India shop for their favorite products. The sale also saw PhonePe create history with a 150% growth in transactions over last year.Here are some interesting ...",
                        "pubDate": "Mon, 22 Oct 2018 15:59:26 GMT",
                        "permaLink": "",
                        "id": "0000246"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Mon, 22 Oct 2018 14:16:54 GMT",
                "item": [
                    {
                        "title": "17 Payment Gateway Terms Entrepreneurs Must Know",
                        "link": "https://www.instamojo.com/blog/payment-gateway-terminologies/",
                        "body": "Are you a newbie entrepreneur or an established business owner looking to pick a payment gateway for your business?\nWith the several options available in the market, it can be a difficult task to pick the best gateway for your business. It can get worse because payment gateways ...",
                        "pubDate": "Mon, 22 Oct 2018 12:48:34 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/payment-gateway-terminologies/#respond",
                        "id": "0000245"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Mon, 22 Oct 2018 07:55:45 GMT",
                "item": [
                    {
                        "title": "Our QR Tech now powers Japan’s QR-based PayPay",
                        "link": "https://blog.paytm.com/our-qr-tech-now-powers-japans-qr-based-paypay-1b39ee3884c6?source=rss----4d9006e6d2c6---4",
                        "body": "We are glad to announce that PayPay corporation, a joint venture between SoftBank Corp., Yahoo Japan and Paytm has launched PayPay, a smartphone-based settlement service in Japan. PayPay had teamed up us for the launch, and our technology and payments wisdom has been the ...",
                        "pubDate": "Mon, 22 Oct 2018 07:41:39 GMT",
                        "permaLink": "",
                        "id": "0000238"
                    }
                ]
            },
            {
                "feedTitle": "Official Blog on Payment Gateway - EBS",
                "feedUrl": "https://www.ebs.in/IPS/blog?format=feed&type=rss",
                "websiteUrl": "https://www.ebs.in/IPS/blog",
                "feedDescription": "",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:56 GMT",
                "item": [
                    {
                        "title": "Multilingualism to Break Barrier and Strengthen Rural Reach",
                        "link": "https://www.ebs.in/IPS/blog/multilingualism-to-break-barrier-and-strengthen-rural-reach",
                        "body": "India is the second most populous country of the world and it is believed that if it continues to grow at the same rate, it may surpass China in next 5 years i.e. till 2021.According to the World Bank’s Report (2014), almost 67.63% people of India’s total population are still ...",
                        "pubDate": "Thu, 14 Apr 2016 23:59:35 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/multilingualism-to-break-barrier-and-strengthen-rural-reach",
                        "id": "0000189"
                    },
                    {
                        "title": "3D Secure Payment Gateway: How It Has Turned Out To Be A Boon For The E-Commerce Industry in India ?",
                        "link": "https://www.ebs.in/IPS/blog/2-factor-authentication-or-3-d-secure-how-it-has-turned-out-to-be-a-boon-for-the-e-commerce-industry-in-india",
                        "body": "Transacting online was not very popular among cardholders in India because of the constant fear about the safety of such transactions. Online payments were mostly done using credit cards as banks were wary of opening up debit cards due to obvious security reasons and lack of ...",
                        "pubDate": "Thu, 05 May 2016 07:50:34 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/2-factor-authentication-or-3-d-secure-how-it-has-turned-out-to-be-a-boon-for-the-e-commerce-industry-in-india",
                        "id": "0000188"
                    },
                    {
                        "title": "The Fee Related With an Online Payment Gateway in India",
                        "link": "https://www.ebs.in/IPS/blog/the-fee-related-with-an-online-payment-gateway-in-india",
                        "body": "Payment gateway offers the desired and vital link between the online seller (merchant), its client, the client’s online payment instrument provider (credit/debit card or any other online payment mode as chosen by the client for making online payment), and the bank account of the ...",
                        "pubDate": "Tue, 26 Jul 2016 03:12:33 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/the-fee-related-with-an-online-payment-gateway-in-india",
                        "id": "0000187"
                    },
                    {
                        "title": "What Annoys and Confuses An Online Shopper",
                        "link": "https://www.ebs.in/IPS/blog/what-annoys-and-confuses-an-online-shopper",
                        "body": "When you are at a brick-and-mortar store, you do not need any technology to know when the buyers are getting annoyed and frustrated. Body language and voices/murmurs show that the credit card payment machine is not functioning well enough and the payment process is getting ...",
                        "pubDate": "Tue, 25 Oct 2016 07:14:19 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/what-annoys-and-confuses-an-online-shopper",
                        "id": "0000186"
                    },
                    {
                        "title": "India’s Premier Online Payment Service Provider",
                        "link": "https://www.ebs.in/IPS/blog/india%E2%80%99s-premier-online-payment-service-provider",
                        "body": "With the expanding reach of the internet in India we are witnessing a revolution of sorts. The regions and areas that were accessible with difficulties earlier are now being connected faster. The internet has enabled people from one part of the globe to communicate faster and ...",
                        "pubDate": "Wed, 04 Jan 2017 06:59:00 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/india%E2%80%99s-premier-online-payment-service-provider",
                        "id": "0000185"
                    },
                    {
                        "title": "Leading Provider of Innovative Payment Gateway Services for Android Smartphones",
                        "link": "https://www.ebs.in/IPS/blog/leading-provider-of-innovative-payment-gateway-services-for-android-smartphones",
                        "body": "With the fast pace at which technology is advancing today, India is not far behind in adapting to it. According to data from the 2014 MasterCard Online Shopping Survey, the number of Indian online shoppers using mobile smartphones to make purchases stood at around 63%, which has ...",
                        "pubDate": "Thu, 19 Jan 2017 07:30:48 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/leading-provider-of-innovative-payment-gateway-services-for-android-smartphones",
                        "id": "0000184"
                    },
                    {
                        "title": "Follow These 3 Tips While Adding A Payment Gateway To Your E-Commerce",
                        "link": "https://www.ebs.in/IPS/blog/follow-these-3-tips-while-adding-a-payment-gateway-to-your-e-commerce",
                        "body": "Any merchant, who does online selling and has an e-commerce or a business website, needs to have a good payment gateway seamlessly integrated into its business portal. While you may receive money through other channels like post-dated cheques among others, the process becomes ...",
                        "pubDate": "Mon, 30 Jan 2017 04:16:39 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/follow-these-3-tips-while-adding-a-payment-gateway-to-your-e-commerce",
                        "id": "0000183"
                    },
                    {
                        "title": "Easy and Safe Online Credit Card Processing Solutions From EBS",
                        "link": "https://www.ebs.in/IPS/blog/easy-and-safe-online-credit-card-processing-solutions-from-ebs",
                        "body": "India has one of the fastest growing and largest ecommerce industries in the world. Several factors are known to have contributed to the growth and expansion of this industry in India. This has also resulted in the increased use of alternate payment methods, especially credit ...",
                        "pubDate": "Mon, 14 Aug 2017 06:35:33 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/easy-and-safe-online-credit-card-processing-solutions-from-ebs",
                        "id": "0000182"
                    },
                    {
                        "title": "Set your business free from bondage of cash with EBS",
                        "link": "https://www.ebs.in/IPS/blog/set-your-business-free-from-bondage-of-cash-with-ebs",
                        "body": "Although cash has been the predominant choice of making payments and performing transactions in India since the beginning, today’s scenario is fast changing as merchants and consumers alike are turning to the more efficient and safer cashless transaction and payment ...",
                        "pubDate": "Thu, 05 Oct 2017 01:00:00 GMT",
                        "permaLink": "https://www.ebs.in/IPS/blog/set-your-business-free-from-bondage-of-cash-with-ebs",
                        "id": "0000181"
                    }
                ]
            },
            {
                "feedTitle": "Atom Technologies Ltd. | Blog",
                "feedUrl": "https://www.atomtech.in/blog/feed/",
                "websiteUrl": "https://www.atomtech.in/blog",
                "feedDescription": "Atom Technologies Blog",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:55 GMT",
                "item": [
                    {
                        "title": "10 points to consider before choosing a Digital Payment Partner",
                        "link": "https://www.atomtech.in/blog/10-points-consider-selecting-digital-payment-partner/",
                        "body": "Online businesses are gaining momentum at the moment as more and more merchants are looking forward to take their business a level ahead. The digitization has led to a wave where every merchant is participating in the race of integrating digital payments to ease and expand their ...",
                        "pubDate": "Fri, 15 Dec 2017 05:35:19 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/10-points-consider-selecting-digital-payment-partner/#respond",
                        "id": "0000170"
                    },
                    {
                        "title": "Here is Shyam’s story of how he started accepting Digital Payments to flourish his business",
                        "link": "https://www.atomtech.in/blog/shyam-started-accepting-digital-payments-to-flourish-his-business/",
                        "body": "Meet Shyam, the owner of Shyam Kirana Stores. He has been in the business for more than 20 years, constantly growing his business and increasing his sales moderately. But the advent of movements like Digital India, elimination of cash and other redundant form of payments led his ...",
                        "pubDate": "Wed, 07 Mar 2018 06:18:38 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/shyam-started-accepting-digital-payments-to-flourish-his-business/#respond",
                        "id": "0000169"
                    },
                    {
                        "title": "Here is Monica’s story of how mGalla helped in flourishing her home-made Bakery Business",
                        "link": "https://www.atomtech.in/blog/monica-started-accepting-digitalpayments-through-mgalla/",
                        "body": "Meet Monica, a 25 year old entrepreneur who has created a buzz with her home baked cookies, cupcakes and extravagant cakes. Monica has always loved baking and soon enough she is planning to establish her homemade baked goods into a professional cake shop.\nEveryday Monica takes ...",
                        "pubDate": "Wed, 14 Mar 2018 09:23:02 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/monica-started-accepting-digitalpayments-through-mgalla/#respond",
                        "id": "0000168"
                    },
                    {
                        "title": "Here is laxman’s story of how he started accepting Card Payments on Delivery through mPOS",
                        "link": "https://www.atomtech.in/blog/mpos-accept-card-payments-on-delivery/",
                        "body": "Laxman works as a delivery boy for a small café located in a very posh area of the city. His café is bustling with business and is growing considerably. It is a one of a kind, small and affordable café that serves the most scrumptious delicacies around.\nEvery day, Laxman has to ...",
                        "pubDate": "Tue, 17 Apr 2018 06:50:17 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/mpos-accept-card-payments-on-delivery/#respond",
                        "id": "0000167"
                    },
                    {
                        "title": "Now Accept UPI Payment Online through atom payment gateway",
                        "link": "https://www.atomtech.in/blog/accept-upi-payment-online/",
                        "body": "Accept payments via UPI without asking your customer to enter any bank details, OTPs and password. Yes, it’s that easy to accept UPI payment online through atom payment gateway. Your customers can use any UPI enabled app to make payments directly on your website.\n&nbsp;\nBut what ...",
                        "pubDate": "Wed, 18 Jul 2018 12:43:28 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/accept-upi-payment-online/#respond",
                        "id": "0000166"
                    },
                    {
                        "title": "7 Safety Tips to Follow While Doing Online Transactions",
                        "link": "https://www.atomtech.in/blog/7-tips-to-do-online-transactions-safely/",
                        "body": "The Internet era has given a boost to online shopping and banking which has ultimately led to online fraud and identity theft. Online transactions always carry some risk, but consumers can incorporate many things to increase their security on the web. If you take just the right ...",
                        "pubDate": "Wed, 25 Jul 2018 10:31:20 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/7-tips-to-do-online-transactions-safely/#respond",
                        "id": "0000165"
                    },
                    {
                        "title": "A Quick ‘Refund Guide’ Why Refund Takes Time to Reach Customers?",
                        "link": "https://www.atomtech.in/blog/quick-refund-guide-why-refund-takes-time-to-reach-customers/",
                        "body": "&nbsp;\nIn simple terms, refund is a process in which a consumer ask retailer to return the money previously paid.\nHas this ever happened to you while making an online purchase?\n\nYou returned an item and the money was promised to be credited to your account within 5-10 working ...",
                        "pubDate": "Tue, 07 Aug 2018 07:26:11 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/quick-refund-guide-why-refund-takes-time-to-reach-customers/#respond",
                        "id": "0000164"
                    },
                    {
                        "title": "UPI 2.0 launched. Close look at Key Features of UPI 2.0",
                        "link": "https://www.atomtech.in/blog/upi-2-0-launched-key-features-of-upi-2-0/",
                        "body": "Unified Payment Interface is a real time payment system that helps to transfer funds instantly between two banks with the help of mobile phone. Ever since the launch of UPI in August 2016, the face of digital payments has changed vigorously. Today, UPI has become the most ...",
                        "pubDate": "Mon, 20 Aug 2018 06:37:14 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/upi-2-0-launched-key-features-of-upi-2-0/#respond",
                        "id": "0000163"
                    },
                    {
                        "title": "What is India Post Payments Bank and how does it work? Here’s 10 things to know",
                        "link": "https://www.atomtech.in/blog/what-is-india-post-payments-bank-heres-10-things-to-know/",
                        "body": "The launch of India Post Payments Bank (IPPB) by Prime Minister Narendra Modi on 1st September 2018 has put the spotlight back on payments banks. The government has envisioned that the new bank model will take banking to the doorstep of every citizen through the unmatched ...",
                        "pubDate": "Mon, 10 Sep 2018 11:27:05 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/what-is-india-post-payments-bank-heres-10-things-to-know/#respond",
                        "id": "0000162"
                    },
                    {
                        "title": "Payment Settlement Guide- Understand the Payment Settlement Process",
                        "link": "https://www.atomtech.in/blog/payment-settlement-guide/",
                        "body": "Customer purchases a product from an e-commerce website, he then, selects the payment option and pays through various payment options available. Before the money reaches e-commerce (merchant) bank account, it passes through some simple steps. This process is known as ...",
                        "pubDate": "Mon, 08 Oct 2018 06:16:28 GMT",
                        "permaLink": "",
                        "comments": "https://www.atomtech.in/blog/payment-settlement-guide/#respond",
                        "id": "0000161"
                    }
                ]
            },
            {
                "feedTitle": null,
                "feedUrl": "https://corp.ezetap.com/feed/",
                "websiteUrl": "https://corp.ezetap.com/",
                "feedDescription": "Payments made Smart",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:54 GMT",
                "item": [
                    {
                        "title": "India needs more Earthbenders",
                        "link": "https://corp.ezetap.com/blogs/india-needs-earthbenders",
                        "body": "Shirish Andhare\nSVP – Product, Ezetap Mobile Solutions Pvt. Ltd.\n\nThe recent demonetization of Rs.500 and Rs.1000 bills to stem the tide of black money in the country was a remarkable fait accompli by our PM Mr. Narendra Modi. For those of us in Fintech who have come to accept ...",
                        "pubDate": "Fri, 25 Nov 2016 09:21:02 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/blogs/india-needs-earthbenders#respond",
                        "id": "0000160"
                    },
                    {
                        "title": "Heralding a New Green Revolution",
                        "link": "https://corp.ezetap.com/blogs/heralding-a-new-green-revolution",
                        "body": "Sanjeeb Kalita\nChief Revenue Officer, Ezetap Mobile Solutions Pvt. Ltd.\nBenefits of digital transformation on the agriculture economy\n\n&nbsp;\nOverview\nCash is the primary medium of monetary transactions in the Indian agricultural sector. It has been the foundation for moving ...",
                        "pubDate": "Mon, 06 Feb 2017 11:42:00 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/blogs/heralding-a-new-green-revolution#respond",
                        "id": "0000159"
                    },
                    {
                        "title": "BharatQR – Accelerating India’s Digital Transformation",
                        "link": "https://corp.ezetap.com/blogs/bharatqr-accelerating-indias-digital-transformation",
                        "body": "Bhaktha Keshavachar\nCo-Founder &amp; CTO, Ezetap Mobile Solutions Pvt. Ltd.\nA newly minted form of payments will be launched this month on 20th Feb, named BharatQR. What is QR? How will this help us in our transformation to a digital economy?\n\n\nQR stands for Quick Response; QR ...",
                        "pubDate": "Mon, 20 Feb 2017 12:17:45 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/blogs/bharatqr-accelerating-indias-digital-transformation#respond",
                        "id": "0000158"
                    },
                    {
                        "title": "Persistence pays, literally for the Life Insurance sector",
                        "link": "https://corp.ezetap.com/persistence-pays-literally-for-the-life-insurance-sector",
                        "body": "Prasanna Rao\nDirector of Marketing, Ezetap Mobile Solutions Pvt. Ltd.\nWays to tackle the industry challenge of customer retention and decrease renewal drop significantly\n\nOverview\nAt 3.67 lakh crore of premium collected in 2015-16, the life insurance industry has grown ...",
                        "pubDate": "Thu, 20 Apr 2017 17:16:38 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/persistence-pays-literally-for-the-life-insurance-sector#respond",
                        "id": "0000157"
                    },
                    {
                        "title": "Consumer lending growth – Boom or Bubble?",
                        "link": "https://corp.ezetap.com/Consumer-lending-growth-Boom-or-Bubble",
                        "body": "Prasanna Rao\nDirector of Marketing, Ezetap Mobile Solutions Pvt. Ltd.\nEnabling NBFCs to sustain the recent high growth in consumer lending by focusing on digitisation\n \nThis growth has been possible with NBFCs playing an immense role in driving this growth. NBFCs have grown at a ...",
                        "pubDate": "Thu, 18 May 2017 06:37:24 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/Consumer-lending-growth-Boom-or-Bubble#respond",
                        "id": "0000156"
                    },
                    {
                        "title": "The Design of Ezetap’s Duo 3",
                        "link": "https://corp.ezetap.com/the-design-of-ezetap-duo3",
                        "body": "T VenkataChalapathi\n\nPrincipal Architect, Ezetap Mobile Solutions Pvt. Ltd. \nBuilding a disruptive hardware product that meets global standards is a challenge, with respect to time and cost – especially for a start up like Ezetap with limited resources and lack of access to ...",
                        "pubDate": "Mon, 17 Jul 2017 06:08:35 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/the-design-of-ezetap-duo3#respond",
                        "id": "0000155"
                    },
                    {
                        "title": "Enabling Smoother, Smarter Citizen Services with Ezetap",
                        "link": "https://corp.ezetap.com/enabling-smoother-smarter-citizen-service",
                        "body": "In focus: Bangalore Traffic Police\n\n\n\n\nChaitali Bhatia\n\nSenior Manager &#8211; Marketing Ezetap Mobile Solutions Pvt. Ltd.\n\n\n\nOverview \nTraffic regulation and enforcement of road safety rules is a complex task in India, given the size of the population and increasing vehicular ...",
                        "pubDate": "Wed, 20 Dec 2017 10:47:51 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/enabling-smoother-smarter-citizen-service#respond",
                        "id": "0000154"
                    },
                    {
                        "title": "Solution Consultant",
                        "link": "https://corp.ezetap.com/solution-consultant/",
                        "body": "Experience required –  6 to 8 years\nLocation – Bengaluru and Mumbai\n\n\nOur Mission is to be the single solution through which businesses in India complete any\nfinancial transaction with their customers, supporting every instrument and method that their\ncustomers want to use\nOur ...",
                        "pubDate": "Wed, 11 Apr 2018 12:42:31 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/solution-consultant/#respond",
                        "id": "0000153"
                    },
                    {
                        "title": "QA Engineer- Automation",
                        "link": "https://corp.ezetap.com/qa-engineer-automation/",
                        "body": "Experience required – 4 to 6 years\nLocation – Bengaluru\n&nbsp;\nAbout The Company\n\nOur Vision is to be the single solution through which businesses in India complete any financial transaction with their customers, supporting every instrument and method that their customers want ...",
                        "pubDate": "Mon, 23 Apr 2018 09:59:09 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/qa-engineer-automation/#respond",
                        "id": "0000152"
                    },
                    {
                        "title": "Senior Software Engineer",
                        "link": "https://corp.ezetap.com/senior-software-engineer/",
                        "body": "Experience required – 5 to 7 years\nLocation – Bangalore\n\nJob responsibilities\n\n\n\nBuilding scalable, reliable, high-performing platforms\nDevelopment of complex product features focused on design, coding, testing, and end user experience\nMaintain a high service level in terms of ...",
                        "pubDate": "Thu, 14 Jun 2018 05:49:36 GMT",
                        "permaLink": "",
                        "comments": "https://corp.ezetap.com/senior-software-engineer/#respond",
                        "id": "0000151"
                    }
                ]
            },
            {
                "feedTitle": "truebalance - Medium",
                "feedUrl": "https://medium.com/feed/truebalance",
                "websiteUrl": "https://medium.com/truebalance?source=rss----1d74a0c225fb---4",
                "feedDescription": "True Balance is a mobile app for people in India to easily check and recharge their mobile balance. With one tap balance check and quick recharges it is one of the leading mobile recharge app in India. - Medium",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "GRAB THE BENEFITS OF INCREASED FOLLOWER’S LIMIT ASAP!",
                        "link": "https://medium.com/truebalance/grab-the-benefits-of-increased-followers-limit-asap-6805355c40fe?source=rss----1d74a0c225fb---4",
                        "body": "In April, our users earned Rs. 6000 and more with True Balance; by making more followers and follower’s follower in Recharge Membership Plan.This month, the bar is set even higher to give you more!Now, follower’s limit has been increased from 5 to 10 followers thus giving a ...",
                        "pubDate": "Mon, 14 May 2018 07:06:33 GMT",
                        "permaLink": "",
                        "id": "0000150"
                    },
                    {
                        "title": "What’s new with Recharge Membership in June?",
                        "link": "https://medium.com/truebalance/whats-new-with-recharge-membership-in-june-23a947349a19?source=rss----1d74a0c225fb---4",
                        "body": "In the month of June, rewards from Recharge Membership Programme are higher than ever. This time, you can earn upto Rs. 12,780 as against Rs. 10,270 that you could have earned last month.One of the most significant change is that the recharge limit for receiving auto cashback ...",
                        "pubDate": "Fri, 08 Jun 2018 13:48:53 GMT",
                        "permaLink": "",
                        "id": "0000149"
                    },
                    {
                        "title": "Now Pay Your Electricity Bill with Ease",
                        "link": "https://medium.com/truebalance/now-pay-your-electricity-bill-with-ease-a5f0a1dfad1d?source=rss----1d74a0c225fb---4",
                        "body": "No more long lines for Electricity Bill Payment! With True Balance giving you the convenience of paying your electricity bills in just a few taps, such harsh physical and mental sufferings are about to end! Now pay easily and quickly sitting at your home.Yes, keeping our promise ...",
                        "pubDate": "Fri, 15 Jun 2018 17:44:29 GMT",
                        "permaLink": "",
                        "id": "0000148"
                    },
                    {
                        "title": "Here’s the most convenient way to Pay Gas Bill",
                        "link": "https://medium.com/truebalance/easy-gas-bill-payment-with-true-balance-e48a1674c115?source=rss----1d74a0c225fb---4",
                        "body": "Now cook food without any interruptions! No calendar marking, or missing bill payment dates as True Balance is here with the easiest way to sort your gas bills.Yes, True Balance, with its quest to bring balance to lives, have launched the facility of Gas Bill Payment. Few taps ...",
                        "pubDate": "Fri, 15 Jun 2018 17:51:45 GMT",
                        "permaLink": "",
                        "id": "0000147"
                    },
                    {
                        "title": "What’s new with Recharge Membership in July?",
                        "link": "https://medium.com/truebalance/whats-new-with-recharge-membership-in-july-86b3ec302855?source=rss----1d74a0c225fb---4",
                        "body": "Good News, True Balance users! On popular demand, we are back with 7% Cashback, this month.Yes, now, you will be able to enjoy 7% Cashback on every Recharge and Bill Payment you do and could eventually get upto Rs. 9,120 as your total monthly earnings.What’s more?There are so ...",
                        "pubDate": "Mon, 02 Jul 2018 12:49:03 GMT",
                        "permaLink": "",
                        "id": "0000146"
                    },
                    {
                        "title": "Beware of Frauds!",
                        "link": "https://medium.com/truebalance/beware-of-frauds-15331b165b17?source=rss----1d74a0c225fb---4",
                        "body": "‘Sharing is Caring’ holds no meaning when it comes to financial transactions. Just to make your experience with True Balance more secure, we are here with things you must not share with anyone to avoid frauds.OTP : One-time password (OTP) is something you will receive on your ...",
                        "pubDate": "Thu, 12 Jul 2018 12:49:46 GMT",
                        "permaLink": "",
                        "id": "0000145"
                    },
                    {
                        "title": "New Improved Way to Check Your Balance",
                        "link": "https://medium.com/truebalance/a-new-improved-way-to-check-your-balance-9783a7fc9188?source=rss----1d74a0c225fb---4",
                        "body": "True Balance launches the coolest balance check interface. Gone are the ‘pull down’ days as from now a single tap will do the magic of instant balance check for you.This works differently for JIO users &amp; other users like Airtel, Vodafone, Idea etc. As per your registration ...",
                        "pubDate": "Fri, 27 Jul 2018 13:44:16 GMT",
                        "permaLink": "",
                        "id": "0000144"
                    },
                    {
                        "title": "What’s New with Recharge Membership in August?",
                        "link": "https://medium.com/truebalance/whats-new-with-recharge-membership-in-august-abba83587bd3?source=rss----1d74a0c225fb---4",
                        "body": "Enjoy flat 5% Auto Cashback, not only on the transactions you do but also on your follower’s &amp; follower’s followers Recharges/Bill Payments.Below are the benefits, one can gain from Recharge Membership in the month of AugustExciting Auto Cashback5% Auto Cashback on ...",
                        "pubDate": "Thu, 02 Aug 2018 04:48:14 GMT",
                        "permaLink": "",
                        "id": "0000143"
                    },
                    {
                        "title": "Gift Card: The Most Awaited Launch of True Balance",
                        "link": "https://medium.com/truebalance/gift-card-the-most-awaited-launch-of-true-balance-9381835c9b17?source=rss----1d74a0c225fb---4",
                        "body": "Gift Cards, the most awaited service in True Balance app has been launched. Now, no need to worry about not having a bank account and finishing your True Balance wallet limit. Gift Cards are meant to be an instant, safe and limitless mode of payment now. You can use a Gift Card ...",
                        "pubDate": "Sat, 15 Sep 2018 18:40:45 GMT",
                        "permaLink": "",
                        "id": "0000142"
                    },
                    {
                        "title": "What’s New with Recharge Membership in October?",
                        "link": "https://medium.com/truebalance/whats-new-with-recharge-membership-in-october-471730c90572?source=rss----1d74a0c225fb---4",
                        "body": "Gold Members, do more Recharges and Bill Payments this month. The maximum transaction limit has become Rs. 85,000 now. Also, enjoy different Cashback percentage on all our services, including Gift Cards as well.Let’s find out, what’s new in Recharge Membership ProgrammeFor you, ...",
                        "pubDate": "Mon, 01 Oct 2018 13:42:23 GMT",
                        "permaLink": "",
                        "id": "0000141"
                    }
                ]
            },
            {
                "feedTitle": "MobiKwik",
                "feedUrl": "https://blog.mobikwik.com/index.php/feed/",
                "websiteUrl": "https://blog.mobikwik.com/",
                "feedDescription": "#MobiKwikHaiNa",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "From Youth to Senior Citizens and everything in between",
                        "link": "https://blog.mobikwik.com/index.php/from-youth-to-senior-citizens-and-everything-in-between/",
                        "body": "To achieve anything in life, we need balance. Work and life balance, balance between caring for ourselves and caring for others, so on and so forth. Similar balance is required in a family and in a country, balance between energy and experience, experimentation and patience, ...",
                        "pubDate": "Tue, 21 Aug 2018 10:19:35 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/from-youth-to-senior-citizens-and-everything-in-between/#respond",
                        "id": "0000140"
                    },
                    {
                        "title": "Celebrating the perfect love-hate relationship – the bond between the siblings!",
                        "link": "https://blog.mobikwik.com/index.php/celebrating-the-perfect-love-hate-relationship/",
                        "body": "&nbsp;\n“Sibling are children of the same parents, each of whom is perfectly normal until they get together”\nIf there is one person you love like crazy and hate with equal passion, it is your sibling. You have the best of memories and worst of fights together. You keep feeding ...",
                        "pubDate": "Fri, 24 Aug 2018 08:48:23 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/celebrating-the-perfect-love-hate-relationship/#respond",
                        "id": "0000139"
                    },
                    {
                        "title": "Remembering the fun school stories, this Teacher’s Day!",
                        "link": "https://blog.mobikwik.com/index.php/remembering-the-fun-school-stories-this-teachers-day/",
                        "body": "This day reminds us of those old school days when a teacher enters the class and all scream HAPPY TEACHERS’ DAY those were the best day of our life. We might be hating them, giving nicknames to them, troubling them, annoying them but at the end we value them. What we are today ...",
                        "pubDate": "Wed, 05 Sep 2018 10:06:28 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/remembering-the-fun-school-stories-this-teachers-day/#respond",
                        "id": "0000138"
                    },
                    {
                        "title": "Send Money Instantly, The IMPS Way",
                        "link": "https://blog.mobikwik.com/index.php/send-money-instantly-the-imps-way/",
                        "body": "Nothing is permanent in your life… not even transferring money from your account to your MobiKwik wallet! Any time you want to send money back to your savings account or any other account, just pull up your MobiKwik app. The ‘Wallet to Bank Transfer’ feature gives you the ...",
                        "pubDate": "Fri, 07 Sep 2018 13:05:36 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/send-money-instantly-the-imps-way/#respond",
                        "id": "0000137"
                    },
                    {
                        "title": "Instant Gratification… With Instant Loans From MobiKwik!",
                        "link": "https://blog.mobikwik.com/index.php/instant-gratification-with-instant-loans-from-mobikwik/",
                        "body": "Your gym just came out with a tempting membership offer, but the first of the month is still a couple of weeks away. You want to gift your mom the newest iPhone (or is it for you?) but there’s no way you can swing the entire amount on your own. You’re looking at the dream flight ...",
                        "pubDate": "Fri, 14 Sep 2018 08:26:52 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/instant-gratification-with-instant-loans-from-mobikwik/#respond",
                        "id": "0000136"
                    },
                    {
                        "title": "Life without Engineers",
                        "link": "https://blog.mobikwik.com/index.php/life-without-engineers/",
                        "body": "Imagine, if there were no Engineers, there would be no one to design the bridges you drive through. If there were no engineers, there would be no cars. If there were no engineers there would be no video game designers to support your lazy-ass games. If there were no engineers ...",
                        "pubDate": "Fri, 14 Sep 2018 12:00:18 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/life-without-engineers/#comments",
                        "id": "0000135"
                    },
                    {
                        "title": "Discover your city on two wheels with MobiKwik",
                        "link": "https://blog.mobikwik.com/index.php/discover-your-city-on-two-wheels-with-mobikwik/",
                        "body": "Introducing Bike rental services on MobiKwik. Now you can book a bike or scooty through MobiKwik and go on a ride that you’ve been waiting for a long time now.\nLove comes in all shapes and sizes. For some it’s a perfect 4&#215;4 drive, for others it’s a compact hatchback. And ...",
                        "pubDate": "Fri, 21 Sep 2018 12:02:58 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/discover-your-city-on-two-wheels-with-mobikwik/#respond",
                        "id": "0000134"
                    },
                    {
                        "title": "How to get an Instant Loan in 90 Seconds",
                        "link": "https://blog.mobikwik.com/index.php/how-to-get-an-instant-loan-in-90-seconds/",
                        "body": "When you’ve got to have that new phone, book those ‘perfect deal’ flight tickets or even rush to the hospital for an emergency, but the first of the month is still far away, who you gonna call? Rather than phone a friend or curse yourself for not having enough time to apply for ...",
                        "pubDate": "Mon, 24 Sep 2018 09:19:20 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/how-to-get-an-instant-loan-in-90-seconds/#comments",
                        "id": "0000133"
                    },
                    {
                        "title": "Shop till you drop and Go wicked this Dussehra",
                        "link": "https://blog.mobikwik.com/index.php/shop-till-you-drop-and-go-wicked-this-dussehra/",
                        "body": "There’s something about the month of October. The weather is just right, colours so bright! It’s that time of the year, when festivities are in the air. And with Dussehra just around the corner, what can be a better way to start the festive season than going crazy with the ...",
                        "pubDate": "Thu, 11 Oct 2018 04:07:27 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/shop-till-you-drop-and-go-wicked-this-dussehra/#respond",
                        "id": "0000132"
                    },
                    {
                        "title": "Let your savings shine – Introducing Gold on MobiKwik",
                        "link": "https://blog.mobikwik.com/index.php/let-your-savings-shine-introducing-gold-on-mobikwik/",
                        "body": "We Indians just love gold. Be it jewellery for wedding or gold coins for Diwali Puja or gifting on big occasions like anniversaries and birthdays, gold is an integral part of our culture and our belief system.  It is also an important asset for investment for long term financial ...",
                        "pubDate": "Thu, 18 Oct 2018 12:06:54 GMT",
                        "permaLink": "",
                        "comments": "https://blog.mobikwik.com/index.php/let-your-savings-shine-introducing-gold-on-mobikwik/#respond",
                        "id": "0000131"
                    }
                ]
            },
            {
                "feedTitle": "Asia Hub",
                "feedUrl": "https://newsroom.mastercard.com/asia-pacific/feed/",
                "websiteUrl": "https://newsroom.mastercard.com/asia-pacific",
                "feedDescription": "News from MasterCard",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "Diebold Nixdorf And Mastercard Join Forces To Provide Industry-Defining, Managed Self-Service Solution For Banking And Retail Customers",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/diebold-nixdorf-and-mastercard-join-forces-to-provide-industry-defining-managed-self-service-solution-for-banking-and-retail-customers/",
                        "body": "Partnership between two world-leading companies will deliver end-to-end, secure offering designed to dramatically impact channel operations\nSingapore, July 12 2018 – Diebold Nixdorf (NYSE: DBD), a world leader in driving connected commerce, today announced a partnership with ...",
                        "pubDate": "Thu, 12 Jul 2018 13:00:01 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/diebold-nixdorf-and-mastercard-join-forces-to-provide-industry-defining-managed-self-service-solution-for-banking-and-retail-customers/#comments",
                        "id": "0000130"
                    },
                    {
                        "title": "Mastercard Offers Greater Value for Premium Cardholders in Sri Lanka",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-offers-greater-value-for-premium-cardholders-in-sri-lanka/",
                        "body": "Adds benefits and privileges to suit the evolving needs of affluent consumers\nColombo, 16 July 2018 – Mastercard today unveiled an enhanced suite of benefits and privileges exclusively for World and Platinum Mastercard consumer credit cardholders in Sri Lanka.\nBased on the ...",
                        "pubDate": "Mon, 16 Jul 2018 04:30:56 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-offers-greater-value-for-premium-cardholders-in-sri-lanka/#comments",
                        "id": "0000129"
                    },
                    {
                        "title": "Bank of China, Mastercard Launch New Travel Credit Card With Air-mile Rewards",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/bank-of-china-mastercard-launch-new-travel-credit-card-with-air-mile-rewards/",
                        "body": "With no earnings cap and accelerated rewards, the BOC Elite Miles World Mastercard is Singapore’s best miles-earning card for frequent travelers\nSingapore, 24 July 2018 – Traveling is now Singapore’s favorite past-time, with Singaporeans making an average of more than 5 trips a ...",
                        "pubDate": "Tue, 24 Jul 2018 07:00:28 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/bank-of-china-mastercard-launch-new-travel-credit-card-with-air-mile-rewards/#comments",
                        "id": "0000128"
                    },
                    {
                        "title": "Mastercard Reinforces its Commitment to Digital India with a Cumulative INR 6,500 crore (US$ 1 billion) Investment",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-reinforces-its-commitment-to-digital-india-with-a-cumulative-inr-6500-crore-us-1-billion-investment/",
                        "body": "Investment significantly higher than the revenue earned\nFurther drives India’s efforts to grow digital transactions by being the most cost effective network\nNew Delhi, 26 July 2018 &#8211; Mastercard has been an active contributor to the Indian economy for over 36 years and has ...",
                        "pubDate": "Thu, 26 Jul 2018 05:37:45 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-reinforces-its-commitment-to-digital-india-with-a-cumulative-inr-6500-crore-us-1-billion-investment/#comments",
                        "id": "0000127"
                    },
                    {
                        "title": "Girls embracing technology subjects through Mastercard Girls4Tech",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/girls-embracing-technology-subjects-through-mastercard-girls4tech/",
                        "body": "Auckland, 30 July 2018 – Term three is only one week down, but a number of year 8 girls at Rotorua’s John Paul College have already been immersed in science, technology, engineering and mathematics (STEM) subjects through Girls4Tech™, a global Mastercard initiative.\nIn its ...",
                        "pubDate": "Sun, 29 Jul 2018 22:00:15 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/girls-embracing-technology-subjects-through-mastercard-girls4tech/#comments",
                        "id": "0000126"
                    },
                    {
                        "title": "Mastercard Empowers SMEs to Thrive in Digital Marketplace with Simplify Commerce",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-empowers-smes-to-thrive-in-digital-marketplace-with-simplify-commerce-2/",
                        "body": "Feature-rich platform makes it easy for businesses to accept payments securely\nColombo, 30 July 2018 – Mastercard today announced the launch of Simplify Commerce, a simple and secure platform that makes it easier for businesses to accept electronic payments, regardless of a ...",
                        "pubDate": "Mon, 30 Jul 2018 04:30:25 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-empowers-smes-to-thrive-in-digital-marketplace-with-simplify-commerce-2/#comments",
                        "id": "0000125"
                    },
                    {
                        "title": "New Zealand A Winter Hotspot For International Tourists",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/new-zealand-a-winter-hotspot-for-international-tourists/",
                        "body": "To share this research, copy and paste https://mstr.cd/2PeKI9w to your Twitter handle\nAuckland, 21 August 2018 – Mastercard data about inbound tourist travel trends has revealed that New Zealand is most popular with Australia, China and USA tourists. 40% of our visitors are our ...",
                        "pubDate": "Mon, 20 Aug 2018 22:00:38 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/new-zealand-a-winter-hotspot-for-international-tourists/#comments",
                        "id": "0000124"
                    },
                    {
                        "title": "Mastercard and Shangri-La’s Hambantota Golf Resort & Spa Enter into a Strategic Alliance in Sri Lanka",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-shangri-las-hambantota-golf-resort-spa-enter-into-a-strategic-alliance-in-sri-lanka/",
                        "body": "Premium lifestyle benefits and privileges introduced for nation’s affluent consumers\nSri Lanka, 22 August 2018 – Mastercard and Shangri-La’s Hambantota Golf Resort &amp; Spa today announced a strategic partnership in Sri Lanka to offer greater value to the growing number of ...",
                        "pubDate": "Wed, 22 Aug 2018 04:30:03 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-shangri-las-hambantota-golf-resort-spa-enter-into-a-strategic-alliance-in-sri-lanka/#comments",
                        "id": "0000123"
                    },
                    {
                        "title": "Mastercard and Liverpool City Council announce City Possible partnership",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-liverpool-city-council-announce-city-possible-partnership/",
                        "body": "The two parties will collaborate on ways to serve residents and visitors\nSydney, 28 August 2018 – Building on its experience in working with cities across Australia and beyond, Mastercard has today signed an agreement with Liverpool City Council aiming to collaborate via ...",
                        "pubDate": "Tue, 28 Aug 2018 00:15:35 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-liverpool-city-council-announce-city-possible-partnership/#comments",
                        "id": "0000122"
                    },
                    {
                        "title": "Scan and Go: Mastercard Partners with Wing to Augment QR Payments Option in Cambodia",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/scan-and-go-mastercard-partners-with-wing-to-augment-qr-payments-option-in-cambodia/",
                        "body": "Phnom Penh, 28 August 2018 – As more Asia Pacific markets transition into cashless societies, Mastercard today announced plans to enhance existing QR-based mobile payment services in Cambodia, in partnership with Wing Limited Specialised Bank (Wing). Harnessing the power of ...",
                        "pubDate": "Tue, 28 Aug 2018 02:48:15 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/scan-and-go-mastercard-partners-with-wing-to-augment-qr-payments-option-in-cambodia/#comments",
                        "id": "0000121"
                    },
                    {
                        "title": "Tackling Misconceptions Around Contactless Technology",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/tackling-misconceptions-around-contactless-technology/",
                        "body": "To share this research, copy and paste https://mstr.cd/2LGpI8q to your Twitter handle\nAuckland, 4 September 2018 – Research shows approximately half (48%) of New Zealanders believe they will live in a cashless society in the next ten years, and almost the same amount of Kiwis ...",
                        "pubDate": "Mon, 03 Sep 2018 22:00:51 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/tackling-misconceptions-around-contactless-technology/#comments",
                        "id": "0000120"
                    },
                    {
                        "title": "Mastercard and South Indian Bank Launch ‘Merchant in a Box’ Bundled Payments Solution for Merchants",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-south-indian-bank-launch-merchant-in-a-box-bundled-payments-solution-for-merchants/",
                        "body": "New Delhi, 6 September 2018 – In a bid to reach out and empower 70 million merchants across India, Mastercard in association with South Indian Bank today announced the launch of ‘Merchant in a Box’ solution. A unique ready-to-use bundled tool kit, ‘Merchant in a Box’ is designed ...",
                        "pubDate": "Thu, 06 Sep 2018 12:03:36 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-south-indian-bank-launch-merchant-in-a-box-bundled-payments-solution-for-merchants/#comments",
                        "id": "0000119"
                    },
                    {
                        "title": "Ctrip Finance and Mastercard Mark New Partnership Milestone with Memorandum of Understanding",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/ctrip-finance-and-mastercard-mark-new-partnership-milestone-with-memorandum-of-understanding/",
                        "body": "Shanghai, 11 September 2018 – A Memorandum of Understanding (MoU) was signed at a launch event held today by Ctrip Finance and Mastercard, marking a new round of partnership between the two industry leaders. The MoU will see the two parties collaborate to enable partners ...",
                        "pubDate": "Tue, 11 Sep 2018 08:00:28 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/ctrip-finance-and-mastercard-mark-new-partnership-milestone-with-memorandum-of-understanding/#comments",
                        "id": "0000118"
                    },
                    {
                        "title": "Government Of Andhra Pradesh and Mastercard Introduce e-Rythu, A Digital Marketplace, To Create A Cashless Agricultural Ecosystem",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/government-of-andhra-pradesh-and-mastercard-introduce-e-rythu-a-digital-marketplace-to-create-a-cashless-agricultural-ecosystem/",
                        "body": "Honorable Chief Minister Sri Nara Chandrababu Naidu inaugurates the digital platform in Amaravati\nMakes payments much safer and simpler for all stakeholders in the agricultural supply chain–the farmer, the buyer, and the agent\nThe platform has a goal of reaching one million ...",
                        "pubDate": "Wed, 12 Sep 2018 06:30:09 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/government-of-andhra-pradesh-and-mastercard-introduce-e-rythu-a-digital-marketplace-to-create-a-cashless-agricultural-ecosystem/#comments",
                        "id": "0000117"
                    },
                    {
                        "title": "Mastercard Signs with League of Legends® as First Global Partner of the World’s Largest Esport",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-signs-with-league-of-legends-as-first-global-partner-of-the-worlds-largest-esport/",
                        "body": "Priceless® Experiences Rolling Out for League of Legends Esports Community\nPurchase, N.Y. and Singapore – 19 September 2018 – Mastercard today announced a multi-year partnership with Riot Games to become the first global sponsor for League of Legends esports, the largest esport ...",
                        "pubDate": "Wed, 19 Sep 2018 04:00:47 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-signs-with-league-of-legends-as-first-global-partner-of-the-worlds-largest-esport/#comments",
                        "id": "0000116"
                    },
                    {
                        "title": "Mastercard and Mahendra Singh Dhoni Team Up to Accelerate Digital Payments in India",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-mahendra-singh-dhoni-team-up-to-accelerate-digital-payments-in-india/",
                        "body": "Mastercard partners the iconic cricketer in addition to the existing ambassador Irrfan Khan. \nCash-to-Digital campaign to focus on driving debit card usage in tier 2 and 3 cities.\nNew Delhi, 19 September 2018 – Legendary cricketer Mahendra Singh Dhoni (MSD) has joined ...",
                        "pubDate": "Wed, 19 Sep 2018 10:30:13 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-mahendra-singh-dhoni-team-up-to-accelerate-digital-payments-in-india/#comments",
                        "id": "0000115"
                    },
                    {
                        "title": "Mastercard Global Destination Cities Index 2018: Half Of Top 10 Destinations In Index Are Asia Pacific Cities",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-global-destination-cities-index-2018-half-of-top-10-destinations-in-index-are-asia-pacific-cities/",
                        "body": "Mastercard brings together tourism and city partners to improve experiences for visitors\nSingapore, 25 September 2018 – In a world of rising nationalism, international travel takes on greater importance—breaking down barriers, broadening our horizons and driving economic impact ...",
                        "pubDate": "Tue, 25 Sep 2018 07:40:13 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-global-destination-cities-index-2018-half-of-top-10-destinations-in-index-are-asia-pacific-cities/#comments",
                        "id": "0000114"
                    },
                    {
                        "title": "Mastercard Integrates Mastercard Track™ in Singapore’s Networked Trade Platform to Simplify International Trade",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-integrates-mastercard-track-in-singapores-networked-trade-platform-to-simplify-international-trade/",
                        "body": "Facilitating secure and efficient electronic payments between buyers and suppliers\nSingapore, 1 October 2018 – Mastercard Track™, a unique global trade platform developed in collaboration with Microsoft, has been integrated with the Networked Trade Platform (NTP) in Singapore, ...",
                        "pubDate": "Mon, 01 Oct 2018 04:51:42 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-integrates-mastercard-track-in-singapores-networked-trade-platform-to-simplify-international-trade/#comments",
                        "id": "0000113"
                    },
                    {
                        "title": "Mastercard Takes Hong Kong’s Education Sector into New Milestone Through Integrated EduTech and FinTech Platform",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-takes-hong-kongs-education-sector-into-new-milestone-through-integrated-edutech-and-fintech-platform/",
                        "body": "Schools and parents enjoy hassle-free, fast, safe and convenient cashless payments experience\nHong Kong, 4 October 2018 – Mastercard is bringing cashless payments to Hong Kong’s education sector through a seamless FinTech and EduTech platform, which enables the city’s first-ever ...",
                        "pubDate": "Thu, 04 Oct 2018 10:24:03 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-takes-hong-kongs-education-sector-into-new-milestone-through-integrated-edutech-and-fintech-platform/#comments",
                        "id": "0000112"
                    },
                    {
                        "title": "Mastercard Welcomes BIA Palm Strip Lounge to LoungeKey Program",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-welcomes-bia-palm-strip-lounge-to-loungekey-program/",
                        "body": "COLOMBO, 10 October 2018 – Mastercard (NYSE: MA) announced the inclusion of the refurbished Palm Strip Lounge located in the International Departures area of the Colombo Bandaranaike International Airport to its globally acclaimed LoungeKey airport access program.\nThe move is ...",
                        "pubDate": "Wed, 10 Oct 2018 04:30:09 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-welcomes-bia-palm-strip-lounge-to-loungekey-program/#comments",
                        "id": "0000111"
                    },
                    {
                        "title": "Mastercard and Fitbit Make On-The-Go Payments a Breeze with Fitbit Pay",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-fitbit-make-on-the-go-payments-a-breeze-with-fitbit-pay/",
                        "body": "Fitbit Ionic and Fitbit Versa users now have the choice and peace of mind to make secure tap-and-go payments with Fitbit Pay and Mastercard\nBangkok, 10 October, 2018 – Today Mastercard announced that it is partnering with Fitbit (NYSE: FIT), Siam Commercial Bank (SCB) and ...",
                        "pubDate": "Wed, 10 Oct 2018 08:05:06 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-and-fitbit-make-on-the-go-payments-a-breeze-with-fitbit-pay/#comments",
                        "id": "0000110"
                    },
                    {
                        "title": "Mastercard Launches Staying Safe Online Guide for Small Business",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-launches-staying-safe-online-guide-for-small-business/",
                        "body": "New Zealand, 10 October 2018 – New Zealanders spent more than $4 billion online last year and with online spending now growing faster than traditional payments, it’s more important than ever for small businesses to pay attention to their cyber security. Mastercard has today ...",
                        "pubDate": "Wed, 10 Oct 2018 08:24:22 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-launches-staying-safe-online-guide-for-small-business/#comments",
                        "id": "0000109"
                    },
                    {
                        "title": "Mastercard Teams up with Zoho to Launch a Bundled Solution for SMEs",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-teams-up-with-zoho-to-launch-a-bundled-solution-for-smes/",
                        "body": "New Delhi, 15 October 2018 &#8211; Mastercard today announced its strategic partnership with Zoho, a leading cloud solution provider to launch a bundled &#8216;SME in a box&#8217; solution for business owners. The bundle will ease payments and help Small and Medium Enterprises ...",
                        "pubDate": "Mon, 15 Oct 2018 10:46:03 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/mastercard-teams-up-with-zoho-to-launch-a-bundled-solution-for-smes/#comments",
                        "id": "0000108"
                    },
                    {
                        "title": "Daraz Bangladesh and Southeast Bank Empower Online Purchases, through Mastercard Payment Gateway",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/daraz-bangladesh-and-southeast-bank-empower-online-purchases-through-mastercard-payment-gateway/",
                        "body": "Dhaka, 16 October 2018 – Daraz Bangladesh Limited, one of the largest e-commerce merchants in Bangladesh, has signed an agreement with Southeast Bank Limited (SEBL), allowing customers of the former to make online payments for purchases using Debit, Credit and Prepaid cards ...",
                        "pubDate": "Tue, 16 Oct 2018 07:00:17 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/daraz-bangladesh-and-southeast-bank-empower-online-purchases-through-mastercard-payment-gateway/#comments",
                        "id": "0000107"
                    },
                    {
                        "title": "Millennial and Gen Z Muslim Travelers driving a US$180 Billion Online Travel Market",
                        "link": "https://newsroom.mastercard.com/asia-pacific/press-releases/millennial-and-gen-z-muslim-travelers-driving-a-us180-billion-online-travel-market/",
                        "body": "Singapore, 18 October 2018 –The increasing reliance on the internet, social media and smartphones for place discovery and travel bookings by Muslim Millennials and Generation Z is driving a US$180 billion online market.\nThe Mastercard-CrescentRating Digital Muslim Travel Report ...",
                        "pubDate": "Thu, 18 Oct 2018 03:30:51 GMT",
                        "permaLink": "",
                        "comments": "https://newsroom.mastercard.com/asia-pacific/press-releases/millennial-and-gen-z-muslim-travelers-driving-a-us180-billion-online-travel-market/#comments",
                        "id": "0000106"
                    }
                ]
            },
            {
                "feedTitle": "Razorpay Blog",
                "feedUrl": "https://rzpwp.blog/feed/",
                "websiteUrl": "https://rzpwp.blog/",
                "feedDescription": "",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "Collect payments faster with the all new powerful Payment Links",
                        "link": "https://rzpwp.blog/2018/07/16/collect-payments-faster-with-payment-links/",
                        "body": "In the online world, payments are more than an exchange of money. It is a metric of conversion, a stamp of trust and an important part of the user experience. With the various payment options available to the customer, the digital payments industry needs to look at fine-tuning ...",
                        "pubDate": "Mon, 16 Jul 2018 06:02:17 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/07/16/collect-payments-faster-with-payment-links/#respond",
                        "id": "0000105"
                    },
                    {
                        "title": "Create and Send GST-compliant Invoices with Razorpay",
                        "link": "https://rzpwp.blog/2018/07/31/razorpay-gst-compliant-invoices/",
                        "body": "With GST coming into effect in 2017, all Indian businesses are now required by law to create GST-compliant invoices &#8211; both on paper and electronically. The number of e-invoices across the globe is on the rise (volume of e-invoices in 2016 was approx. $30 billion worldwide, ...",
                        "pubDate": "Tue, 31 Jul 2018 14:58:05 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/07/31/razorpay-gst-compliant-invoices/#respond",
                        "id": "0000104"
                    },
                    {
                        "title": "Aadhaar Virtual ID (VID) – Get Your VID in 3 Simple Steps",
                        "link": "https://rzpwp.blog/2018/08/02/get-your-aadhaar-virtual-id-vid-in-3-simple-steps/",
                        "body": "Guess what’s common in getting a mobile phone connection, investing in mutual funds and opening a bank account? Submitting Aadhaar details! Yes, Aadhaar has become the de facto authentication/verification tool for a gamut of services today. Using a single universal ID is great, ...",
                        "pubDate": "Thu, 02 Aug 2018 10:59:23 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/08/02/get-your-aadhaar-virtual-id-vid-in-3-simple-steps/#respond",
                        "id": "0000103"
                    },
                    {
                        "title": "UPI 2.0 – New Features, Missing Links, and the Effect on Indian Businesses",
                        "link": "https://rzpwp.blog/2018/08/16/upi-upgrade-affect-indian-business/",
                        "body": "When UPI was first launched in 2016, it was rightly heralded as a game changer. We couldn&#8217;t agree more, because the inherent structure of the NPCI’s flagship offering is designed to become the sole platform for seamless interoperability of PSPs (Payment Service Providers) ...",
                        "pubDate": "Thu, 16 Aug 2018 05:31:43 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/08/16/upi-upgrade-affect-indian-business/#respond",
                        "id": "0000102"
                    },
                    {
                        "title": "Lessons from the Frontline: How Razorpay Customers Drive Business Change Through Online Payments",
                        "link": "https://rzpwp.blog/2018/08/24/case-study-online-payments/",
                        "body": "One of the measures of innovation &#8211; whether technology or process-driven &#8211;  and its impact is the way it changes the way an organization does business. Some time back, we wrote to tell you about Razorpay 2.0; a suite of products that make Razorpay more than your ...",
                        "pubDate": "Fri, 24 Aug 2018 06:43:32 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/08/24/case-study-online-payments/#respond",
                        "id": "0000101"
                    },
                    {
                        "title": "Want to Make Your Company a Great Place to Work? Here Are 4 Powerful Secrets!",
                        "link": "https://rzpwp.blog/2018/09/04/razorpay-workplace-culture-secrets/",
                        "body": "There is a wonderful blog on the Harvard Business Review that begins by stating that workplace culture begins once the CEO walks out of the room. (The title is hilariously apt, and haven’t we all been in a similar situation?) At Razorpay though, we have always tried to subvert ...",
                        "pubDate": "Tue, 04 Sep 2018 10:24:41 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/09/04/razorpay-workplace-culture-secrets/#respond",
                        "id": "0000100"
                    },
                    {
                        "title": "How ATM PIN (Instead of OTP) Boosted Our Payment Success Rates",
                        "link": "https://rzpwp.blog/2018/09/07/how-atm-pin-instead-of-otp-boosted-our-payment-success-rates/",
                        "body": "If there is one thing the payment ecosystem obsesses about, it is ‘Success Rates’. This is because success rate has a direct impact on customer experience and revenue.\nWhat is success rate for payments? \nSuccess rate for payments refers to the number of successful transactions ...",
                        "pubDate": "Fri, 07 Sep 2018 11:37:19 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/09/07/how-atm-pin-instead-of-otp-boosted-our-payment-success-rates/#respond",
                        "id": "0000099"
                    },
                    {
                        "title": "How Secure Are Your Online Payments?",
                        "link": "https://rzpwp.blog/2018/09/27/online-payment-security/",
                        "body": "At Razorpay we strive to make every transaction done via our payment gateway a secure payment. We&#8217;re a technology-first online payments company and online payment security is in our DNA. We employ a ‘no stones unturned’ approach to safeguarding the interest of both the ...",
                        "pubDate": "Thu, 27 Sep 2018 08:56:28 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/09/27/online-payment-security/#respond",
                        "id": "0000098"
                    },
                    {
                        "title": "Nodal vs Escrow vs Current Account – What Does Your Business Need?",
                        "link": "https://rzpwp.blog/2018/10/11/nodal-escrow-current-account/",
                        "body": "Planning on starting an online business? Confused if you should open a Nodal, Escrow or Current account? We’ll help you understand what each of these terms means and which accounts best suits your business type.\nBefore that, let’s take a step back and understand how it all ...",
                        "pubDate": "Thu, 11 Oct 2018 11:34:48 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/10/11/nodal-escrow-current-account/#respond",
                        "id": "0000097"
                    },
                    {
                        "title": "Razorpay Checkout: How We Design for Faster Payments",
                        "link": "https://rzpwp.blog/2018/10/16/checkout-page-design/",
                        "body": "If there’s something every Indian e-commerce shopper hates, it is complications at the final step of the purchase. A great user experience at the checkout stage is a problem that many payment players in the industry have been trying to solve, for a very long time. \nWhy you ask? ...",
                        "pubDate": "Tue, 16 Oct 2018 09:07:25 GMT",
                        "permaLink": "",
                        "comments": "https://rzpwp.blog/2018/10/16/checkout-page-design/#respond",
                        "id": "0000096"
                    }
                ]
            },
            {
                "feedTitle": "Benow",
                "feedUrl": "https://www.benow.in/feed/",
                "websiteUrl": "https://www.benow.in/",
                "feedDescription": "Its Easy",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "How to Multiply your sales on Facebook this Festive season?",
                        "link": "https://www.benow.in/blog/multiply-sales-facebook-festive-season/",
                        "body": "The festive season is here- this is the time when many Indian festivals are scheduled in a row- from Eid to Diwali and Christmas- it’s a time of celebration, revelry and, of course, shopping!\nMost businesses see a spike in sales during festive season as many Indians see this ...",
                        "pubDate": "Thu, 06 Sep 2018 07:42:44 GMT",
                        "permaLink": "",
                        "comments": "https://www.benow.in/blog/multiply-sales-facebook-festive-season/#respond",
                        "id": "0000095"
                    }
                ]
            },
            {
                "feedTitle": "QWIKCILVER",
                "feedUrl": "https://qwikcilver.com/feed/",
                "websiteUrl": "https://qwikcilver.com/",
                "feedDescription": "",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:53 GMT",
                "item": [
                    {
                        "title": "May 2018",
                        "link": "https://qwikcilver.com/2018/05/20/may-2018/",
                        "body": "96\r\n    \r\n    \r\n    \r\n    \r\n    \r\n      body {width: 600px;margin: 0 auto;}\r\n      table {border-collapse: collapse;}\r\n      table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}\r\n      img {-ms-interpolation-mode: bicubic;}\r\n    \r\n    \r\n\r\n    \r\n      body, p, div {\r\n        ...",
                        "pubDate": "Sun, 20 May 2018 13:44:52 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/05/20/may-2018/#respond",
                        "id": "0000084"
                    },
                    {
                        "title": "June 2018",
                        "link": "https://qwikcilver.com/2018/06/18/june-2018/",
                        "body": "96\r\n    \r\n    \r\n    \r\n    \r\n    \r\n      body {width: 600px;margin: 0 auto;}\r\n      table {border-collapse: collapse;}\r\n      table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}\r\n      img {-ms-interpolation-mode: bicubic;}\r\n    \r\n    \r\n\r\n    \r\n      body, p, div {\r\n        ...",
                        "pubDate": "Mon, 18 Jun 2018 08:13:24 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/06/18/june-2018/#respond",
                        "id": "0000083"
                    },
                    {
                        "title": "TEST",
                        "link": "https://qwikcilver.com/2018/06/25/test/",
                        "body": "TEST",
                        "pubDate": "Mon, 25 Jun 2018 06:59:50 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/06/25/test/#respond",
                        "id": "0000082"
                    },
                    {
                        "title": "July 2018",
                        "link": "https://qwikcilver.com/2018/07/20/july-2018/",
                        "body": "body, p, div {\r\n        font-family: arial;\r\n        font-size: 14px;\r\n      }\r\n      body {\r\n        color: #9B9B9B;\r\n      }\r\n      body a {\r\n        color: #0070CD;\r\n        text-decoration: none;\r\n      }\r\n      p { margin: 0; padding: 0; }\r\n      table.wrapper {\r\n        ...",
                        "pubDate": "Fri, 20 Jul 2018 12:17:35 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/07/20/july-2018/#respond",
                        "id": "0000081"
                    },
                    {
                        "title": "Are your channel partners happy with their rewards?",
                        "link": "https://qwikcilver.com/2018/07/31/are-your-channel-partners-happy-with-their-rewards/",
                        "body": "Channel partners are the backbone of the retail sector and contribute 70-80 per cent to brands’ sales. But for the critical role played by them, are we appreciating them enough and giving them the due reward that they deserve? Distributors not being incentivized well is a common ...",
                        "pubDate": "Tue, 31 Jul 2018 11:41:41 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/07/31/are-your-channel-partners-happy-with-their-rewards/#respond",
                        "id": "0000080"
                    },
                    {
                        "title": "August 2018",
                        "link": "https://qwikcilver.com/2018/08/14/august-2018/",
                        "body": "",
                        "pubDate": "Tue, 14 Aug 2018 11:26:01 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/08/14/august-2018/#respond",
                        "id": "0000079"
                    },
                    {
                        "title": "Reward your Customers with Gift Cards & Ensure your Future Sales",
                        "link": "https://qwikcilver.com/2018/08/29/reward-your-customers-with-gift-cards-ensure-your-future-sales/",
                        "body": "",
                        "pubDate": "Wed, 29 Aug 2018 09:38:06 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/08/29/reward-your-customers-with-gift-cards-ensure-your-future-sales/#respond",
                        "id": "0000078"
                    },
                    {
                        "title": "September 2018",
                        "link": "https://qwikcilver.com/2018/09/07/september-2018/",
                        "body": "",
                        "pubDate": "Fri, 07 Sep 2018 12:14:08 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/09/07/september-2018/#respond",
                        "id": "0000077"
                    },
                    {
                        "title": "The Shopper-eye view of Retail",
                        "link": "https://qwikcilver.com/2018/09/25/24-september-2018/",
                        "body": "",
                        "pubDate": "Tue, 25 Sep 2018 09:30:57 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/09/25/24-september-2018/#respond",
                        "id": "0000076"
                    },
                    {
                        "title": "October 2018",
                        "link": "https://qwikcilver.com/2018/10/05/october-2018/",
                        "body": "",
                        "pubDate": "Fri, 05 Oct 2018 12:06:35 GMT",
                        "permaLink": "",
                        "comments": "https://qwikcilver.com/2018/10/05/october-2018/#respond",
                        "id": "0000075"
                    }
                ]
            },
            {
                "feedTitle": "Paytm Blog - Medium",
                "feedUrl": "https://blog.paytm.com/feed",
                "websiteUrl": "https://blog.paytm.com/?source=rss----4d9006e6d2c6---4",
                "feedDescription": "The official Paytm Blog - Medium",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:52 GMT",
                "item": [
                    {
                        "title": "We have acquired Smart-savings Management Startup ‘Balance.Tech’",
                        "link": "https://blog.paytm.com/we-have-acquired-smart-savings-management-startup-balance-tech-cbef918355cf?source=rss----4d9006e6d2c6---4",
                        "body": "We are excited to announce that we have acquired Bengaluru-based Smart-savings Management Startup named ‘Balance.Tech’. We have been building some amazing user experiences, and the ‘Balance.Tech’ team has great insights and track record in building such intelligent and elegantly ...",
                        "pubDate": "Thu, 09 Aug 2018 06:02:32 GMT",
                        "permaLink": "",
                        "id": "0000074"
                    },
                    {
                        "title": "Kerala Needs Our Help!",
                        "link": "https://blog.paytm.com/kerala-needs-our-help-70b34546c0be?source=rss----4d9006e6d2c6---4",
                        "body": "Blog Updated (21/08/2018):We have received confirmation from the Govt. of Kerala that every contribution that was made or will be made towards ‘Kerala CM’s Distress Relief Fund’ on Paytm is completely Tax exempt under Section 80G (2) of the Income Tax Act 1961. The Govt. of ...",
                        "pubDate": "Fri, 17 Aug 2018 08:21:02 GMT",
                        "permaLink": "",
                        "id": "0000073"
                    },
                    {
                        "title": "Thank you for your generous support towards the ‘Kerala CM’s Distress Relief Fund’",
                        "link": "https://blog.paytm.com/thank-you-for-your-generous-support-towards-the-kerala-cms-distress-relief-fund-bf0fcd790b7f?source=rss----4d9006e6d2c6---4",
                        "body": "Govt of Kerala will provide 100% Tax exemption under Section 80G (2) for all contributions(Updated on August 24, 2018. Earlier version — 30 crores from 12 lakh users)We are humbled to see how all of India has come together in helping the state of Kerala, which has been hit by ...",
                        "pubDate": "Mon, 20 Aug 2018 15:23:11 GMT",
                        "permaLink": "",
                        "id": "0000072"
                    },
                    {
                        "title": "We have launched an AI Cloud for India",
                        "link": "https://blog.paytm.com/we-have-launched-an-ai-cloud-for-india-816f85434281?source=rss----4d9006e6d2c6---4",
                        "body": "We are excited to announce the launch of our AI Cloud computing platform — ‘Paytm AI Cloud for India’ meant for Developers, Startups and Enterprises. This made-in-India AI-powered cloud offers a suite of business-centric apps for organisations that need high-quality solutions ...",
                        "pubDate": "Tue, 21 Aug 2018 05:24:50 GMT",
                        "permaLink": "",
                        "id": "0000071"
                    },
                    {
                        "title": "Announcing our latest funding round from Berkshire Hathaway. Here’s to a great partnership!",
                        "link": "https://blog.paytm.com/https-blog-paytm-com-paytm-gets-funding-from-berkshire-hathaway-63ebdeea8308?source=rss----4d9006e6d2c6---4",
                        "body": "Todd Combs from Berkshire Hathaway has joined our Board of DirectorsWe are excited to announce that US-based investment firm Berkshire Hathaway is now a part of our journey. This an endorsement from the world’s most respected investor.Berkshire joins Ant Financial, SoftBank, ...",
                        "pubDate": "Tue, 28 Aug 2018 08:44:43 GMT",
                        "permaLink": "",
                        "id": "0000070"
                    },
                    {
                        "title": "Paytm registers over 100 Cr Monthly Sessions",
                        "link": "https://blog.paytm.com/paytm-registers-over-100-cr-monthly-sessions-largest-contribution-from-money-transfers-a6489ddc38a7?source=rss----4d9006e6d2c6---4",
                        "body": "Over 9.2 Cr monthly users prefer Paytm for regular, frequent paymentsWe are currently witnessing upwards of 100 Cr sessions monthly on our platform. This increase is a result of the rapid growth in the adoption of Money Transfers along with a significant rise in the uptake of ...",
                        "pubDate": "Mon, 03 Sep 2018 05:40:53 GMT",
                        "permaLink": "",
                        "id": "0000069"
                    },
                    {
                        "title": "Paytm Money launches dedicated app for Investment at Zero Charges",
                        "link": "https://blog.paytm.com/paytm-money-launches-dedicated-app-for-investment-at-zero-charges-1fb092a364c?source=rss----4d9006e6d2c6---4",
                        "body": "Set to focus on making investments accessible for millions of usersEarlier this year, Paytm Money received SEBI’s approval to act as an Investment Adviser (IA) with the aim to provide both investment advisory &amp; execution services. Headquartered in and operating from ...",
                        "pubDate": "Tue, 04 Sep 2018 05:55:37 GMT",
                        "permaLink": "",
                        "id": "0000068"
                    },
                    {
                        "title": "Now pay your Visa Credit Card bill on Paytm; use UPI to make payments",
                        "link": "https://blog.paytm.com/now-pay-your-visa-credit-card-bill-on-paytm-580531cd5a68?source=rss----4d9006e6d2c6---4",
                        "body": "We are aiming to process over 2 Million Credit Card bill payments this financial yearWe are glad to announce our partnership with Visa to make Visa credit card payments across Banks on the Paytm app. VISA Credit Card users can now pay their monthly bills at any time, anywhere ...",
                        "pubDate": "Fri, 07 Sep 2018 04:45:39 GMT",
                        "permaLink": "",
                        "id": "0000067"
                    },
                    {
                        "title": "5 Mn of our Offline Merchants now accept UPI Payments",
                        "link": "https://blog.paytm.com/5-mn-of-our-offline-merchants-now-accept-upi-payments-c89ed84dd525?source=rss----4d9006e6d2c6---4",
                        "body": "Over 40% of all UPI transactions on our platform are merchant transactionsWe are happy to announce that over 5 million offline merchants out of our 9 million merchant base now accept UPI-based payments. This large-scale acceptance at merchant offline stores is now contributing ...",
                        "pubDate": "Tue, 25 Sep 2018 05:37:36 GMT",
                        "permaLink": "",
                        "id": "0000066"
                    },
                    {
                        "title": "Now automatically filter spam SMSes within the Paytm app",
                        "link": "https://blog.paytm.com/we-have-launched-spam-proof-sms-inbox-as-a-part-of-paytm-inbox-14191a556ae7?source=rss----4d9006e6d2c6---4",
                        "body": "We will make your lives simpler by classifying your SMSes into Personal, Transactional &amp; Promotional messagesWe are excited to announce the launch of our Spam-proof ‘SMS Inbox’ as a part of our widely popular Paytm Inbox.The Spam-proof SMS Inbox uses proprietary Machine ...",
                        "pubDate": "Mon, 01 Oct 2018 06:03:40 GMT",
                        "permaLink": "",
                        "id": "0000065"
                    }
                ]
            },
            {
                "feedTitle": "PhonePe - Medium",
                "feedUrl": "https://blog.phonepe.com/feed",
                "websiteUrl": "https://blog.phonepe.com/?source=rss----725044d21903---4",
                "feedDescription": "India’s Payments App. Powered By UPI. - Medium",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:52 GMT",
                "item": [
                    {
                        "title": "PhonePe Launches Gold — The First Step in Wealth Creation for Customers",
                        "link": "https://blog.phonepe.com/phonepe-launches-gold-the-first-step-in-wealth-creation-for-customers-dad74c53045a?source=rss----725044d21903---4",
                        "body": "Buy 24 Karat Gold on PhonePe nowPhonePe Launches Gold — The First Step in Wealth Creation for CustomersWe recently launched Gold as a category on the PhonePe app. Our customers can now buy and sell certified 24 Karat Gold, for as low as INR 1 at transparent prices. All they need ...",
                        "pubDate": "Thu, 25 Jan 2018 13:03:25 GMT",
                        "permaLink": "",
                        "id": "0000064"
                    },
                    {
                        "title": "Save Yourself From Merchant Frauds!",
                        "link": "https://blog.phonepe.com/save-yourself-from-merchant-frauds-55df9445fc95?source=rss----725044d21903---4",
                        "body": "Save Yourself From Merchant Frauds!Save yourself from merchant fraudsFraudsters are constantly looking at new ways to extract money. With more and more customers shopping online, Merchant Frauds are on the rise. In this blogpost we talk about staying safe from Merchant Frauds.We ...",
                        "pubDate": "Wed, 31 Jan 2018 10:43:46 GMT",
                        "permaLink": "",
                        "id": "0000063"
                    },
                    {
                        "title": "PhonePe strikes a strategic partnership with FreeCharge",
                        "link": "https://blog.phonepe.com/phonepe-strikes-a-strategic-partnership-with-freecharge-70262fbbd3a6?source=rss----725044d21903---4",
                        "body": "PhonePe strikes a strategic partnership with FreeChargePhonePe partners with FreeChargeWe recently entered into a strategic alliance with FreeCharge a leading wallet player. As a part of the partnership, we have now enabled over 45Mn users to link their existing FreeCharge ...",
                        "pubDate": "Wed, 31 Jan 2018 11:03:33 GMT",
                        "permaLink": "",
                        "id": "0000062"
                    },
                    {
                        "title": "PhonePe launches app-in-app platform with redBus as the first partner",
                        "link": "https://blog.phonepe.com/phonepe-launches-app-in-app-platform-with-redbus-as-the-first-partner-9d4b7d315ac0?source=rss----725044d21903---4",
                        "body": "PhonePe launches app-in-app platform with redBus as the first partnerFirst app to take an open ecosystem approachWe have entered into a strategic partnership with redBus, the world’s largest online bus ticket booking service. As a part of the partnership, Redbus has now gone ...",
                        "pubDate": "Mon, 26 Feb 2018 06:12:51 GMT",
                        "permaLink": "",
                        "id": "0000061"
                    },
                    {
                        "title": "All that Glitters is not Gold",
                        "link": "https://blog.phonepe.com/all-that-glitters-is-not-gold-a2c55eb8ed70?source=rss----725044d21903---4",
                        "body": "The February 2018 UPI numbers have been published by NPCI, and as always there is much to cheer about. As India’s first and largest non-banking UPI app, we are thrilled to see UPI grow from strength to strength. Its exponential growth in the past year has surpassed everyone’s ...",
                        "pubDate": "Thu, 08 Mar 2018 10:15:43 GMT",
                        "permaLink": "",
                        "id": "0000060"
                    },
                    {
                        "title": "PhonePe Partners with Ola to Launch the Industry- First AutoPay Feature",
                        "link": "https://blog.phonepe.com/phonepe-partners-with-ola-to-launch-the-industry-first-autopay-feature-80cd3f117c3c?source=rss----725044d21903---4",
                        "body": "PhonePe Partners with Ola to Launch the Industry- First AutoPay FeatureWe are excited to announce our partnership with Ola, India’s leading and one of the world’s largest ride-sharing companies, to offer users a hassle-free cab and auto booking experience.Users can now book an ...",
                        "pubDate": "Thu, 28 Jun 2018 06:33:07 GMT",
                        "permaLink": "",
                        "id": "0000059"
                    },
                    {
                        "title": "The ride to get OLA on PhonePe",
                        "link": "https://blog.phonepe.com/the-ride-to-get-ola-on-phonepe-847a41e7e6aa?source=rss----725044d21903---4",
                        "body": "PhonePe’s open ecosystem approach, enables partners to launch their stores/micro-apps on the app and reach out to over 100M users. This also provides partners another medium beyond their native apps and websites, where PhonePe ensures a seamless login/signup and payment ...",
                        "pubDate": "Wed, 11 Jul 2018 15:36:22 GMT",
                        "permaLink": "",
                        "id": "0000058"
                    },
                    {
                        "title": "Chapter 2: Ola Begins",
                        "link": "https://blog.phonepe.com/this-is-the-second-in-a-series-of-blog-posts-in-which-we-outline-our-experience-building-the-ola-6d109e665879?source=rss----725044d21903---4",
                        "body": "Chapter 2: Ola BeginsThis is the second in a series of blog posts in which we outline our experience of building the Ola micro-app using React Native.Read the first part of the blog here.Now that we had decided on the tech stack and had setup the base project with the utils ...",
                        "pubDate": "Sun, 29 Jul 2018 10:14:27 GMT",
                        "permaLink": "",
                        "id": "0000057"
                    },
                    {
                        "title": "Data Localization — Why this Kolaveri Di?",
                        "link": "https://blog.phonepe.com/data-localization-why-this-kolaveri-di-6d5680e3f012?source=rss----725044d21903---4",
                        "body": "Context: On April 6th 2018, the Reserve Bank of India issued a circular, which mandates all payment ecosystem players operating in India to store all data related to user transactions within national boundaries only. The deadline to comply with this circular is October ...",
                        "pubDate": "Tue, 11 Sep 2018 14:54:01 GMT",
                        "permaLink": "",
                        "id": "0000056"
                    },
                    {
                        "title": "Fraud alert: Preventing malicious apps from accessing your data",
                        "link": "https://blog.phonepe.com/fraud-alert-preventing-malicious-apps-from-accessing-your-data-3737b44ee70f?source=rss----725044d21903---4",
                        "body": "This is an authored post by Radhakrishna R and Anuj BhansaliMr. Kumar was puzzled when he got an OTP message in the morning and a few minutes later got a message “9999/- has been debited from your bank account XXX bank”. He was always careful not to share his OTP with anyone. On ...",
                        "pubDate": "Fri, 28 Sep 2018 07:38:38 GMT",
                        "permaLink": "",
                        "id": "0000055"
                    }
                ]
            },
            {
                "feedTitle": "Blog Instamojo",
                "feedUrl": "https://www.instamojo.com/blog/feed/",
                "websiteUrl": "https://www.instamojo.com/blog",
                "feedDescription": "For Small Businesses and Micro merchants in India. Sell online and collect payments for a seamless online business experience.",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:50 GMT",
                "item": [
                    {
                        "title": "5 Things You Need to Know about the Aadhaar Verdict",
                        "link": "https://www.instamojo.com/blog/10-things-about-aadhaar-verdict/",
                        "body": "Remember the times you were constantly reminded to update your Aadhaar card details by banks and telephone service providers? You will no longer have to, thanks to the Aadhaar Verdict by the Supreme Court on September 27, 2018.\nEarlier, banks denied accounts to people without an ...",
                        "pubDate": "Thu, 27 Sep 2018 15:13:00 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/10-things-about-aadhaar-verdict/#respond",
                        "id": "0000054"
                    },
                    {
                        "title": "IMPS ranks #1 in the Global Payment Systems Index",
                        "link": "https://www.instamojo.com/blog/imps-ranks-1-global-payment-systems-index/",
                        "body": "IMPS (Immediate Payment Service) has been ranked #1 on the Faster Payments Innovation Index, a platform that rates real-time programs based on how well they meet consumer needs and it&#8217;s comprehensiveness.\nThe NPCI recorded a whopping 135.74 million transactions worth over ...",
                        "pubDate": "Fri, 28 Sep 2018 12:12:07 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/imps-ranks-1-global-payment-systems-index/#respond",
                        "id": "0000053"
                    },
                    {
                        "title": "E-Commerce Companies to Register in All Operating States to Collect TCS",
                        "link": "https://www.instamojo.com/blog/e-commerce-register-operating-states-collect-tcs/",
                        "body": "Attention all online business owners! You will now need to register in all operating states in order to collect TCS.\nInitially, businesses didn&#8217;t have to be registered in every state they function, to collect TCS. A GST registration or a GSTN would suffice.  Now, it is ...",
                        "pubDate": "Mon, 01 Oct 2018 13:22:05 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/e-commerce-register-operating-states-collect-tcs/#respond",
                        "id": "0000052"
                    },
                    {
                        "title": "Collecting Payments via Instamojo: Everything You Need to Know",
                        "link": "https://www.instamojo.com/blog/collecting-payments-via-instamojo/",
                        "body": "You&#8217;ve done a great job setting up your online store and showcasing your products. What&#8217;s left? Finding an easy and secure tool for collecting payments via Instamojo.\nHere&#8217;s everything you need to know about how to activate your Instamojo account to collect ...",
                        "pubDate": "Mon, 08 Oct 2018 06:15:10 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/collecting-payments-via-instamojo/#respond",
                        "id": "0000051"
                    },
                    {
                        "title": "India Ranks 28th in GEAR Study for Online Payments Adoption",
                        "link": "https://www.instamojo.com/blog/india-ranks-28th-e-payments/",
                        "body": "India now ranks 28th in the Government E-Payments Adoption Ranking study, a survey conducted by Visa.\nThe Government E-Payments Adoption Ranking (GEAR) is a Global Index and benchmarking study commissioned by Visa, the card issuing company to check how governments around the ...",
                        "pubDate": "Mon, 08 Oct 2018 13:29:59 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/india-ranks-28th-e-payments/#respond",
                        "id": "0000050"
                    },
                    {
                        "title": "5 Reasons Entrepreneurs are Susceptible to Mental Health Challenges",
                        "link": "https://www.instamojo.com/blog/entrepreneurs-mental-health-challenges/",
                        "body": "Mental health is no joke.\nWe live in a world where running behind materialistic gains is the only thing we do. Do you remember the last time you went for a movie without constantly checking your emails? Do you recount a vacation where you met people and had a conversation, with ...",
                        "pubDate": "Wed, 10 Oct 2018 12:43:41 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/entrepreneurs-mental-health-challenges/#respond",
                        "id": "0000049"
                    },
                    {
                        "title": "Women Entrepreneurs Rocking the Instamojo Online Store",
                        "link": "https://www.instamojo.com/blog/women-entrepreneurs-online-store/",
                        "body": "This International Day of the Girl Child, let&#8217;s join hands to celebrate the women around us. Be it our mothers, friends, wives or daughters, their efforts often times, go unnoticed. So, let&#8217;s dedicate today to celebrating the doers, the dreamers, the makers and the ...",
                        "pubDate": "Thu, 11 Oct 2018 10:44:21 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/women-entrepreneurs-online-store/#respond",
                        "id": "0000048"
                    },
                    {
                        "title": "Last Date for Income Tax Return Filing for Businesses Extended",
                        "link": "https://www.instamojo.com/blog/last-day-file-it-returns-businesses/",
                        "body": "The last date to file income tax returns for business in India for the financial year 2017-18 has been extended to October 31, 2018. Previously, the last date for filing IT returns was pegged on October 15, 2018.\nThe deadline extension comes after several stakeholders approached ...",
                        "pubDate": "Mon, 15 Oct 2018 11:56:12 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/last-day-file-it-returns-businesses/#respond",
                        "id": "0000047"
                    },
                    {
                        "title": "4 Ways to Increase Sales and Revenue for your Online Business",
                        "link": "https://www.instamojo.com/blog/increase-sales-revenue-online-business/",
                        "body": "Being a sustainable online business is a daunting task. As a prerequisite to running a flourishing online business, one needs to ensure constant sales and revenue.\nTo be sustainable, you not only need regular sales but also need to generate high-value orders. This means that a ...",
                        "pubDate": "Wed, 17 Oct 2018 12:18:39 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/increase-sales-revenue-online-business/#respond",
                        "id": "0000046"
                    },
                    {
                        "title": "Influencer Marketing Hacks to Grow Your Customer Base",
                        "link": "https://www.instamojo.com/blog/influencer-marketing-grow-customer-base/",
                        "body": "Whether you are a new startup or an established business, increasing your customer base is one of the biggest challenges you face. Influencer marketing, may help you solve this problem.\nWith a wide variety of marketing strategies available in the market, it can be difficult for ...",
                        "pubDate": "Thu, 18 Oct 2018 12:33:27 GMT",
                        "permaLink": "",
                        "comments": "https://www.instamojo.com/blog/influencer-marketing-grow-customer-base/#respond",
                        "id": "0000045"
                    }
                ]
            },
            {
                "feedTitle": "Eko India Financial Services",
                "feedUrl": "http://eko.co.in/feed/",
                "websiteUrl": "https://eko.co.in/",
                "feedDescription": "",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:50 GMT",
                "item": [
                    {
                        "title": "Introducing Connect Doodles. A platform that celebrates :)",
                        "link": "http://eko.co.in/introducing-connect-doodles-a-platform-that-celebrates/",
                        "body": "The New Connect portal that our partners and merchants use, is a dynamic platform that has a life of its own! In India, we celebrate our diversity through festivals. Each festival has its own colors, sights, experiences and sounds. Beginning this month, Connect too will ...",
                        "pubDate": "Fri, 26 Aug 2016 12:39:00 GMT",
                        "permaLink": "",
                        "id": "0000044"
                    },
                    {
                        "title": "Most popular mobile devices in the Eko merchant network",
                        "link": "http://eko.co.in/most-popular-mobile-devices-in-the-eko-merchant-network/",
                        "body": "Eko works with a select network of almost 10,000 merchants across almost all the states in India who facilitate remittance services for our customers.\nA good portion of our merchants transact on smart-devices. The following is a glimpse of the popular device ...",
                        "pubDate": "Fri, 16 Sep 2016 12:40:25 GMT",
                        "permaLink": "",
                        "id": "0000043"
                    },
                    {
                        "title": "Eko @ 9",
                        "link": "http://eko.co.in/eko-9/",
                        "body": "Eko celebrated its 9th foundation day on 16th September 2016. 9 years of converting #CashToSmiles\n\nThe post Eko @ 9 appeared first on Eko India Financial Services.",
                        "pubDate": "Tue, 20 Sep 2016 10:29:33 GMT",
                        "permaLink": "",
                        "id": "0000042"
                    },
                    {
                        "title": "Connect Doodles. September/ October 2016",
                        "link": "http://eko.co.in/connect-doodles-september-october-2016/",
                        "body": "Connect. A platform that celebrates \nDurga Puja\n\nEid\n\nDeepavali/ Diwali\n\nConnect. Celebrate life. Celebrate diversity.\nCelebrate sending money home \n&nbsp;\n&nbsp;\nThe post Connect Doodles. September/ October 2016 appeared first on Eko India Financial Services.",
                        "pubDate": "Tue, 01 Nov 2016 13:33:46 GMT",
                        "permaLink": "",
                        "id": "0000041"
                    },
                    {
                        "title": "Eko ties up with Prabhu Money to Boost Money Remittance Business to Nepal",
                        "link": "http://eko.co.in/eko-ties-up-with-prabhu-money-to-boost-money-remittance-business-to-nepal/",
                        "body": "Eko has expanded its Indo-Nepal remittance (money transfer) services in partnership with Prabhu Money Transfer. Through these integrations, Eko enables it’s agents to remit money to a wider audience in Nepal. Eko’s first international tie-up for remittance services was with ...",
                        "pubDate": "Mon, 15 May 2017 18:43:15 GMT",
                        "permaLink": "",
                        "id": "0000040"
                    },
                    {
                        "title": "API + RESTful API. An introduction. Plain and simple",
                        "link": "http://eko.co.in/api-restful-api-an-introduction-plain-and-simple/",
                        "body": "Connect!\n\n\n\n\n\n\n\n\nFrom Appliance Plumbing Interface…\nImagine that you have just designed a water faucet/ tap which dispenses water only if your fingerprint matches (however strange this may sound, I believe creativity should know no bounds ;). Now, for this to be actually used in ...",
                        "pubDate": "Tue, 16 May 2017 10:42:20 GMT",
                        "permaLink": "",
                        "id": "0000039"
                    },
                    {
                        "title": "Now IMPS is always available for all transactions on Connect!",
                        "link": "http://eko.co.in/now-imps-always-available-transactions-connect/",
                        "body": "Scheduled Transaction\n&nbsp;\nA major pain point for our distributors, API partners and merchants was the unavailability of IMPS for certain recipient banks while remitting money. In such situations, we would compel our partners our partners to necessarily use NEFT. This severely ...",
                        "pubDate": "Fri, 29 Sep 2017 11:24:12 GMT",
                        "permaLink": "",
                        "comments": "http://eko.co.in/now-imps-always-available-transactions-connect/#comments",
                        "id": "0000038"
                    },
                    {
                        "title": "Why Security is Essential for our API Partners? Security 2.0 is all you need to know!",
                        "link": "http://eko.co.in/security-essential-api-partners-security-2-0-need-know/",
                        "body": "During current times of big cybercrimes and security hacks across the world, it is extremely important that your system is very secure. Especially, the industry that we work and the number of stakeholders like merchants, distributors, employees, etc involved, it is imperative ...",
                        "pubDate": "Wed, 25 Oct 2017 11:50:35 GMT",
                        "permaLink": "",
                        "comments": "http://eko.co.in/security-essential-api-partners-security-2-0-need-know/#respond",
                        "id": "0000037"
                    },
                    {
                        "title": "(Updated) Introducing API Widget!",
                        "link": "http://eko.co.in/introducing-api-widget/",
                        "body": "Eko was founded on the basic principle of enabling financial transactions for anybody from anywhere. In the evolution process, Eko pioneered domestic money transfer service where customers earning in cash could simply remit money to their family by visiting a nearby shop. This ...",
                        "pubDate": "Thu, 28 Jun 2018 05:20:21 GMT",
                        "permaLink": "",
                        "comments": "http://eko.co.in/introducing-api-widget/#comments",
                        "id": "0000036"
                    },
                    {
                        "title": "How To Choose The Best Money Transfer Platform For Your Business",
                        "link": "http://eko.co.in/how-to-choose-the-best-money-transfer-platform-for-your-business/",
                        "body": "Do you know which features to look at before choosing a money transfer platform for your business?\n&nbsp;\nThere are a number of factors that define your money transfer business like platform success rate, integration time, pricing, etc. \n&nbsp;\nSo, if your answer to the above ...",
                        "pubDate": "Sat, 01 Sep 2018 08:24:40 GMT",
                        "permaLink": "",
                        "comments": "http://eko.co.in/how-to-choose-the-best-money-transfer-platform-for-your-business/#respond",
                        "id": "0000035"
                    }
                ]
            },
            {
                "feedTitle": "The Freecharge Blog",
                "feedUrl": "https://blog.freecharge.in/feed/",
                "websiteUrl": "https://blog.freecharge.in/",
                "feedDescription": "#StayCharged",
                "whenLastUpdate": "Sat, 20 Oct 2018 18:23:50 GMT",
                "item": [
                    {
                        "title": "Bills and recharges due? We’ll remind you!",
                        "link": "https://blog.freecharge.in/bills-and-recharges-due-well-remind-you/",
                        "body": "The burden of remembering your dues can now be put to rest. With our latest &#8216;Reminders&#8217; feature on Android, you will not miss any of your recharge or bill dues ever again! What&#8217;s more? You can now see all your dues at one place. Take a look at this video to ...",
                        "pubDate": "Wed, 11 Apr 2018 09:28:27 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/bills-and-recharges-due-well-remind-you/#comments",
                        "id": "0000034"
                    },
                    {
                        "title": "Update your KYC details with FreeCharge!",
                        "link": "https://blog.freecharge.in/update-your-kyc-details-with-freecharge/",
                        "body": "As per RBI guidelines, we&#8217;ve been asked to inform our users to update KYC for their FreeCharge account. So we&#8217;re doing exactly that!\n\nA lengthy verification process, you think? Nuh uh! Visit http://frch.in/KYCL to update your KYC details in just a few seconds and ...",
                        "pubDate": "Thu, 26 Apr 2018 06:25:48 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/update-your-kyc-details-with-freecharge/#comments",
                        "id": "0000033"
                    },
                    {
                        "title": "FreeCharge looking to cash in on fintech boom",
                        "link": "https://blog.freecharge.in/freecharge-looking-to-cash-in-on-fintech-boom/",
                        "body": "BENGALURU: In a phase of rediscovery, Gurgaon based digital payments company Freecharge is looking at adding multiple financial services on its application beyond payments, across insurance, lending and even allowing customers to directly open bank accounts. The payments company ...",
                        "pubDate": "Mon, 21 May 2018 11:02:57 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/freecharge-looking-to-cash-in-on-fintech-boom/#respond",
                        "id": "0000032"
                    },
                    {
                        "title": "All you need to know about UPI on FreeCharge #UPayInstantly",
                        "link": "https://blog.freecharge.in/all-you-need-to-know-about-upi-on-freecharge-upayinstantly/",
                        "body": "Always thought net-banking was the best way to transfer money? Here&#8217;s a better way and a permanent fix to all your money transfer woes! Now you can transfer money from one bank account to another instantly, anytime, anywhere through BHIM UPI on FreeCharge. Want to know how ...",
                        "pubDate": "Thu, 24 May 2018 10:35:33 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/all-you-need-to-know-about-upi-on-freecharge-upayinstantly/#comments",
                        "id": "0000031"
                    },
                    {
                        "title": "12 reasons why UPI on FreeCharge is the most secure payments method!",
                        "link": "https://blog.freecharge.in/12-reasons-why-upi-on-freecharge-is-the-most-secure-payments-method/",
                        "body": "Article courtesy:\nAbhishek Gupta &#8211; Senior Director, Product  &amp; Payments at FreeCharge\nSince its launch in August 2016, BHIM UPI by NPCI (National Payments Corporation of India) has been a great success story in the Indian payments ecosystem. Currently, there are close ...",
                        "pubDate": "Tue, 05 Jun 2018 11:15:31 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/12-reasons-why-upi-on-freecharge-is-the-most-secure-payments-method/#comments",
                        "id": "0000030"
                    },
                    {
                        "title": "Hello Fraands! Chai Pilo. FreeCharge Karlo!",
                        "link": "https://blog.freecharge.in/hello-fraands-chai-pilo-freecharge-karlo/",
                        "body": "Have you heard about &#8216;Chai wali aunty&#8217; yet? If not, you should totally go check out her viral videos on social media! We played along on this high tide and our followers are loving it!\nTake a look:\n\n\nHello Fraands,Chai pilo. Recharge karlo.  Bill bharlo. Cashback ...",
                        "pubDate": "Thu, 14 Jun 2018 07:26:30 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/hello-fraands-chai-pilo-freecharge-karlo/#comments",
                        "id": "0000029"
                    },
                    {
                        "title": "It’s time to GET ON THE BUS with FreeCharge!",
                        "link": "https://blog.freecharge.in/its-time-to-get-on-the-bus-with-freecharge/",
                        "body": "Bus travel is always fun, isn&#8217;t it? The beautiful view the moment you exit the city and hit the hills, the bridges over the water rivers, the lush green farms, the cattle herds crossing by that become natural speed breakers, the village folk who stare at the bus when it ...",
                        "pubDate": "Fri, 20 Jul 2018 09:18:19 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/its-time-to-get-on-the-bus-with-freecharge/#comments",
                        "enclosure": [
                            {
                                "url": "https://blog.freecharge.in/wp-content/uploads/2018/07/GOT_Final_vdo.mp4",
                                "type": "video/mp4",
                                "length": "2306586"
                            }
                        ],
                        "id": "0000028"
                    },
                    {
                        "title": "UPI 2.0 now official: Here is everything you need to know!",
                        "link": "https://blog.freecharge.in/upi-2-0-now-official-here-is-everything-you-need-to-know/",
                        "body": "UPI 2.0, an upgraded version of National Payments Corporation of India (NPCI)’s Unified Payment Interface, has been announced with several new features. Reserve Bank of India (RBI) Governor Urjit Patel formally launched UPI 2.0 in the presence of select members from banks and ...",
                        "pubDate": "Tue, 21 Aug 2018 07:18:23 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/upi-2-0-now-official-here-is-everything-you-need-to-know/#respond",
                        "id": "0000027"
                    },
                    {
                        "title": "It’s your turn – Contribute and help Kerala",
                        "link": "https://blog.freecharge.in/its-your-turn-contribute-and-help-kerala/",
                        "body": "Remember your last holiday to Kerala? The stunning backwaters and golden beaches would have surely taken your breath away! The sturdy green mountains and the beautiful lakes get etched in your memory once you visit the state.\nBut now, God&#8217;s Own Country is undergoing ...",
                        "pubDate": "Tue, 21 Aug 2018 12:42:25 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/its-your-turn-contribute-and-help-kerala/#respond",
                        "id": "0000026"
                    },
                    {
                        "title": "FreeCharge – The power of *F*",
                        "link": "https://blog.freecharge.in/freecharge-the-power-of-f/",
                        "body": "Wondering what the F it&#8217;s all about? Watch our latest films and find out about the POWER OF F! \nFreeCharge for Electricity Bill Payments:\n\nFreeCharge for DTH Recharges:\n\nFreeCharge for Mobile Recharges:\n\nNow you know the POWER OF F! Which one did you like? Don&#8217;t ...",
                        "pubDate": "Mon, 03 Sep 2018 12:27:34 GMT",
                        "permaLink": "",
                        "comments": "https://blog.freecharge.in/freecharge-the-power-of-f/#respond",
                        "id": "0000025"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "PayTechBlogs.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.003,
        "ctBuilds": 32,
        "ctDuplicatesSkipped": 10,
        "whenGMT": "Tue, 27 Nov 2018 05:16:00 GMT",
        "whenLocal": "11/27/2018, 5:16:00 AM",
        "aggregator": "River5 v0.4.14"
    }
})