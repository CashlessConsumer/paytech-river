onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "ReBIT Newsletter Archive Feed",
                "feedUrl": "https://us16.campaign-archive.com/feed?u=ed93f5c78c48c6d9e64bf4479&id=8ac8ca2f6b",
                "websiteUrl": "https://us16.campaign-archive.com/feed?u=ed93f5c78c48c6d9e64bf4479&id=8ac8ca2f6b",
                "feedDescription": "",
                "whenLastUpdate": "Wed, 31 Oct 2018 17:10:42 GMT",
                "item": [
                    {
                        "title": "Cyber Pulse Vol I Issue #4",
                        "link": "https://mailchi.mp/b94ab07042c3/cyber-pulse-153627",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #4\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Tue, 07 Nov 2017 06:01:48 GMT",
                        "permaLink": "https://mailchi.mp/b94ab07042c3/cyber-pulse-153627",
                        "id": "0000206"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #5",
                        "link": "https://mailchi.mp/642130bc2807/cyber-pulse-182783",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #5\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Thu, 07 Dec 2017 11:00:52 GMT",
                        "permaLink": "https://mailchi.mp/642130bc2807/cyber-pulse-182783",
                        "id": "0000205"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #6",
                        "link": "https://mailchi.mp/e0dcf306c0e6/cyber-pulse-202587",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #6\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Sun, 07 Jan 2018 04:33:13 GMT",
                        "permaLink": "https://mailchi.mp/e0dcf306c0e6/cyber-pulse-202587",
                        "id": "0000204"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #7",
                        "link": "https://mailchi.mp/4c5d0f0de321/cyber-pulse-vol-i-issue-226939",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #7\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Thu, 08 Feb 2018 06:31:58 GMT",
                        "permaLink": "https://mailchi.mp/4c5d0f0de321/cyber-pulse-vol-i-issue-226939",
                        "id": "0000203"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #8",
                        "link": "https://mailchi.mp/ecba2e5289ba/cyber-pulse-vol-i-issue-244323",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #8\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Sat, 10 Mar 2018 13:23:27 GMT",
                        "permaLink": "https://mailchi.mp/ecba2e5289ba/cyber-pulse-vol-i-issue-244323",
                        "id": "0000202"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #9",
                        "link": "https://mailchi.mp/65d406c4b4a4/cyber-pulse-vol-i-issue-266559",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #9\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Tue, 10 Apr 2018 14:55:07 GMT",
                        "permaLink": "https://mailchi.mp/65d406c4b4a4/cyber-pulse-vol-i-issue-266559",
                        "id": "0000201"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #10",
                        "link": "https://mailchi.mp/df49e580f4cc/cyber-pulse-vol-i-issue-286955",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #10\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Sat, 12 May 2018 16:48:24 GMT",
                        "permaLink": "https://mailchi.mp/df49e580f4cc/cyber-pulse-vol-i-issue-286955",
                        "id": "0000200"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #11",
                        "link": "https://mailchi.mp/4f7007ed38a8/cyber-pulse-vol-i-issue-302435",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #11\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Tue, 12 Jun 2018 13:45:16 GMT",
                        "permaLink": "https://mailchi.mp/4f7007ed38a8/cyber-pulse-vol-i-issue-302435",
                        "id": "0000199"
                    },
                    {
                        "title": "Cyber Pulse Vol I Issue #12",
                        "link": "https://mailchi.mp/5638227ee1a3/cyber-pulse-vol-i-issue-316587",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol I Issue #12\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Wed, 11 Jul 2018 15:04:11 GMT",
                        "permaLink": "https://mailchi.mp/5638227ee1a3/cyber-pulse-vol-i-issue-316587",
                        "id": "0000198"
                    },
                    {
                        "title": "Cyber Pulse Vol II Issue #1",
                        "link": "https://mailchi.mp/195f160b7267/cyber-pulse-vol-i-issue-329943",
                        "body": "96\n\t\t\t\n\t\t\n\t\t\n\t\t\n        \n        \n\t\tCyber Pulse Vol II Issue #1\n        \n    \n\t\tp{\n\t\t\tmargin:10px 0;\n\t\t\tpadding:0;\n\t\t}\n\t\ttable{\n\t\t\tborder-collapse:collapse;\n\t\t}\n\t\th1,h2,h3,h4,h5,h6{\n\t\t\tdisplay:block;\n\t\t\tmargin:0;\n\t\t\tpadding:0;\n\t\t}\n\t\timg,a ...",
                        "pubDate": "Tue, 14 Aug 2018 08:29:54 GMT",
                        "permaLink": "https://mailchi.mp/195f160b7267/cyber-pulse-vol-i-issue-329943",
                        "id": "0000197"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "ReBIT.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0,
        "ctBuilds": 1,
        "ctDuplicatesSkipped": 0,
        "whenGMT": "Mon, 05 Nov 2018 05:15:00 GMT",
        "whenLocal": "2018-11-5 05:15:00",
        "aggregator": "River5 v0.4.14"
    }
})